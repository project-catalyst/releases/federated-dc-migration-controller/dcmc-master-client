from urllib.parse import urljoin

from communication_handler.api_clients.http_client.client import Client


class DCMCLiteClient(object):
    """DCMC Lite Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Lite
    (DCMCL) Client components of the H2020 CATALYST Migration Controller. It covers
    calls to the entire available API of the DCMC Lite Clients.

    Methods
    -------
    get_public_ssh_key()
        Retrieve the public SSH key of a DCMC Lite Client
    add_authorized_key(ssh_key)
        Add a public SSH key to the authorized keys of DCMC Lite Client
    vpn_connect()
        Command the DCMC Lite to connect to VPN
    vpn_disconnect()
        Command the DCMC Lite to disconnect from VPN

    """
    __API_PREFIX = 'catalyst/dcmcl/api/'
    __ADD_AUTHORIZED_KEY = 'ssh-keys/authorized-keys/add/'
    __RETRIEVE_PUBLIC_KEY = 'ssh-keys/public-key/retrieve/'
    __VPN_CONNECT = 'transaction/{transaction_id}/vpn/connect/'
    __VPN_DISCONNECT = 'transaction/{transaction_id}/vpn/disconnect/'
    __VXLAN_CONFIGURE = 'transaction/{transaction_id}/vxlan/configure/'

    def __init__(self, dcmcl_host_url):
        """DCMC Lite Client Class Constructor.

        Parameters
        ----------
        dcmcl_host_url : URL
            The DCMC Lite's Host URL

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin('http://{}'.format(dcmcl_host_url), DCMCLiteClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }

    def get_public_ssh_key(self):
        """Retrieves the public SSH Key from the DCMC Lite Client.

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler.api_clients.dcmcl_api import DCMCLiteClient
        >>> dcmcl_host_url = 'http://127.0.0.1/'
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url)
        >>> response = dcmcl.get_public_ssh_key()
        >>> response.status_code
        200
        >>> response.json()
        {'public-key': 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA5PMPeE7trRR2IgqompmVDO8Lri5uHK6QAuOq3adcbqip1e5Uwd+3vou'
                       'XX35qKAy1qXNvNd7L+cq5JIDMYHtSCmH6yQehIUI7oO8Th6bDREtkzl7TGa54JDNN68+riaNCuFESVH4Ols7Y8eR94f9wVU'
                       'W43CkOqHqRu6B8V8u6SfthSBbJAoBuFs6P78py5fhuWRLi3S9bav72WVJBjaeftSUYnZSvZWQo9c5UFOu+ijtVpySQlzkJJ'
                       'ABoOJPqAyk/v7oYVHj0nLzqH1u+LEex1m/ci2XL5Yu72LjJKdunwxtMzrTi6ECyJKqavKOlilngcX+vCp0IAxzHKfq++bji'
                       '9l6PxgDvjRjt5zybS3Gl+mokf6dYpAYeWNp7bOUX/M74WsxQGaL3Ml8rIbZ0YlTC7Rzg7RNzMoNoEoNK5RF080NbG8oA0ZX'
                       'b/3AnJ1VbQjETC5NkaskEoi5mr7aN4+tysWtktomiIH3SdDvQfp6ZYfNEhitIlnIvihK49Y20QQlpoLpN/I5M2cNrKoV2Xy'
                       'C+kG05KD/DIDqn1GFAheEvdQ3+blUTPHWk2XoghqWPyjkc3IesA9Ueb64Z0YVwmCTYTOoWCPh9KQn8AzW7ervuA/pCSbkat'
                       'gjT+gdyJzNW66dW4LvPe8Av/u8SOGUWuKzUb4kLdNgUtOph4egkUHAJRQ== user@ubuntu'}

        """
        url = urljoin(self.__url, DCMCLiteClient.__RETRIEVE_PUBLIC_KEY)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def add_authorized_key(self, payload):
        """Add a SSH Key to the authorized keys.

        Parameters
        ----------
        payload : dict
            A payload including the following fields:
                public-key : str
                    A public SSH key to authorize
                host_ip : str
                    The host ip to add to known_hosts
                hostname : str
                    The hostname of the SSH-key

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler.api_clients.dcmcl_api import DCMCLiteClient
        >>> dcmcl_host_url = 'http://127.0.0.1/'
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url)
        >>> ssh_key = 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA5PMPeE7trRR2IgqompmVDO8Lri5uHK6QAuOq3adcbqip1e5Uwd+3vou'
        ...           'XX35qKAy1qXNvNd7L+cq5JIDMYHtSCmH6yQehIUI7oO8Th6bDREtkzl7TGa54JDNN68+riaNCuFESVH4Ols7Y8eR94f9wVU'
        ...           'W43CkOqHqRu6B8V8u6SfthSBbJAoBuFs6P78py5fhuWRLi3S9bav72WVJBjaeftSUYnZSvZWQo9c5UFOu+ijtVpySQlzkJJ'
        ...           'ABoOJPqAyk/v7oYVHj0nLzqH1u+LEex1m/ci2XL5Yu72LjJKdunwxtMzrTi6ECyJKqavKOlilngcX+vCp0IAxzHKfq++bji'
        ...           '9l6PxgDvjRjt5zybS3Gl+mokf6dYpAYeWNp7bOUX/M74WsxQGaL3Ml8rIbZ0YlTC7Rzg7RNzMoNoEoNK5RF080NbG8oA0ZX'
        ...           'b/3AnJ1VbQjETC5NkaskEoi5mr7aN4+tysWtktomiIH3SdDvQfp6ZYfNEhitIlnIvihK49Y20QQlpoLpN/I5M2cNrKoV2Xy'
        ...           'C+kG05KD/DIDqn1GFAheEvdQ3+blUTPHWk2XoghqWPyjkc3IesA9Ueb64Z0YVwmCTYTOoWCPh9KQn8AzW7ervuA/pCSbkat'
        ...           'gjT+gdyJzNW66dW4LvPe8Av/u8SOGUWuKzUb4kLdNgUtOph4egkUHAJRQ== user@ubuntu'}
        >>> response = dcmcl.add_authorized_key(ssh_key)
        >>> response.status_code
        200

        """
        url = urljoin(self.__url, DCMCLiteClient.__ADD_AUTHORIZED_KEY)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def vpn_connect(self, transaction_id):
        """Command DCMCL to connect to VPN.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to connect to VPN for

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler.api_clients.dcmcl_api import DCMCLiteClient
        >>> dcmcl_host_url = 'http://127.0.0.1/'
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url)
        >>> transaction_id = 89080
        >>> response = dcmcl.vpn_connect(transaction_id)
        >>> response.status_code
        202

        """
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCLiteClient.__VPN_CONNECT).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def vpn_disconnect(self, transaction_id):
        """Command DCMCL to disconnect from the VPN.

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler.api_clients.dcmcl_api import DCMCLiteClient
        >>> dcmcl_host_url = 'http://127.0.0.1/'
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url)
        >>> transaction_id = 90887
        >>> response = dcmcl.vpn_disconnect(transaction_id)
        >>> response.status_code
        202

        """
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCLiteClient.__VPN_DISCONNECT).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def vxlan_configure(self, transaction_id, local_ip=None, remote_ip=None):
        """Configure VXLAN interface of migration.

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler.api_clients.dcmcl_api import DCMCLiteClient
        >>> dcmcl_host_url = 'http://127.0.0.1/'
        >>> dcmcl = DCMCLiteClient(dcmcl_host_url)
        >>> transaction_id = 90887
        >>> local_ip, remote_ip = '127.0.0.1', '192.168.255.78'
        >>> response = dcmcl.vxlan_configure(transaction_id, local_ip, remote_ip)
        >>> response.status_code
        202

        """
        params = {'transaction_id': transaction_id}
        payload = {'local_ip': local_ip, 'remote_ip': remote_ip}
        url = urljoin(self.__url, DCMCLiteClient.__VXLAN_CONFIGURE).format(**params)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response
