from urllib.parse import urljoin

from communication_handler.api_clients.http_client.client import Client


class ITLMConnectorClient(object):
    """IT Load Marketplace Connector Client Class.

    This class serves as a wrapper for the API endpoints offered by the Catalyst
    IT Load Marketplace Connector. It is utilized by the DCMC Master Clients of
    both the source and destination DCs.

    Methods
    -------
    migration_action(migration_id, payload)
        Register candidate migration actions
    register_migration_updates(action_id, status)
        Register migration status updates

    """
    MIGRATION_ACTION = 'migration/{}/actions/it_load/'
    MIGRATION_UPDATE = 'actions/{}/status/'

    def __init__(self, itlmc_host_url):
        """ITLMConnectorClient Class Constructor.

        Parameters
        ----------
        itlmc_host_url : str
            The host url of the IT Load Marketplace Connector

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = itlmc_host_url
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def migration_action(self, migration_id, payload):
        """Register candidate migration actions.

        This service is meant to be used by the DCMC Master Client to register information
        about candidate migrations, which will be used by the IT Load Marketplace Connector
        to place relevant market actions on the Catalyst IT Load Marketplace.

        Parameters
        ----------
        migration_id : int
            The ID of the migration as kept by the DCMC Master Client.
        payload : dict
            The payload to send to ITLMC to register migration actions. Includes the following keys:
                date : datetime
                    The time when the action is placed
                starttime : datetime
                    The time on which the candidate migration is planned to start
                endtime : datetime
                    The time on which the candidate migration is planned to end
                loadvalues : dict
                    Information about the load characteristics. Includes the following fields:
                        parameter : str
                            The name of the load characteristic
                        value : float
                              The value quantifying the load characteristics
                        uom : str
                            The unit of measurement of the value
                value : float
                    The value of the energy equivalent of the candidate migration
                uom : str
                    The unit of measurement for the energy value
                price : float
                    The price of the marketaction in the Catalyst IT Load Marketplace in euros
                action_type : {'bid', 'offer'}
                    The type of marketaction to be placed

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.itlmc_api import ITLMConnectorClient
        >>> migration_id = 87678
        >>> payload = {
        ...     "date": "2019-09-30T16:00:00Z",
        ...     "vc_tag": "0x53a218879d6d4d3ddf8a7c8af9c59966e7954847ee4eefdd0207a3ead9fc46f8",
        ...     "starttime": "2019-10-01T21:00:00Z",
        ...     "endtime": "2019-10-01T22:00:00Z",
        ...     "price": 0,
        ...     "value": 0,
        ...     "uom": 'kWh',
        ...     "action_type": "offer",
        ...     "load_values": [
        ...         {"parameter": "cpu", "value": 1, "uom": "cpu"},
        ...         {"parameter": "ram", "value": 2048, "uom": "MB"},
        ...         {"parameter": "disk", "value": 20, "uom": "GB"}
        ...     ]
        ... }
        >>> itlmc = ITLMConnectorClient(config.ITLMC_HOST_URL)
        >>> response = itlmc.migration_action(migration_id, payload)
        >>> response.status_code
        200

        """
        url = urljoin(self.__url, self.MIGRATION_ACTION).format(migration_id)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def register_migration_updates(self, action_id, status):
        """Register migration status updates.

        This service is meant to be used by the DCMC Master Client in order to inform
        about the migration result, which is associated with a certain market action.

        Parameters
        ----------
        action_id : int
            The ID of the marketaction in the Catalyst IT Load Marketplace
        status : {'delivered', 'not_delivered'}
            The status of the given marketaction

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.itlmc_api import ITLMConnectorClient
        >>> marketaction = 90009
        >>> status = 'not_delivered'
        >>> itlmc = ITLMConnectorClient(config.ITLMC_HOST_URL)
        >>> response = itlmc.register_migration_updates(marketaction, status)
        >>> response.status_code
        200

        """
        url = urljoin(self.__url, self.MIGRATION_UPDATE).format(action_id)
        response = self.__client.post(url=url, headers=self.__headers, payload={'status': status})
        return response
