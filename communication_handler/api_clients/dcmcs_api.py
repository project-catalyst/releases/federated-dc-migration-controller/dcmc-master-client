from urllib.parse import urljoin

import requests
from rest_framework import status

from communication_handler import config
from communication_handler.api_clients.http_client.client import Client


class DCMCServerClient(object):
    """DCMC Server Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Server
    (DCMCS) component of the H2020 CATALYST Migration Controller. It covers calls
    to the entire available API of the DCMC Server API.

    Methods
    -------
    migration(payload)
        Report accepted migration to DCMC Server
    token(payload)
        Request token from DCMC Server
    vcontainer(payload)
        Send vcontainer details for creation
    vcontainer_register(transaction_id)
        Inform DCMC Server of VContainer creation

    """
    __API_PREFIX = 'catalyst/dcmcs/api/'
    __REGISTER = 'datacenter/register/'
    __MIGRATION = 'migration/'
    __TOKEN = 'token/'
    __VCONTAINER = 'vcontainer/'
    __VCONTAINER_CREATED = 'transaction/{transaction_id}/vcontainer/status/created'
    __ROLLBACK_DESTINATION = 'transaction/{transaction_id}/rollback/destination/'

    def __init__(self, dcmcs_host_url, username, password):
        """DCMC Server Client Class Constructor.

        Parameters
        ----------
        dcmcs_host_url : str
            The host URL of the DCMC Server.
        username : str
            The username of the DCMC Master registered in the DCMC Server
        password : str
            The password of the DCMC Master registered in the DCMC Server

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin(dcmcs_host_url, DCMCServerClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.__token = self.token({'username': username, 'password': password})
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    def migration(self, dc_name, role, delivery_start, delivery_end, transaction):
        """Report accepted migration to DCMC Server.

        The migration endpoint is used by the DCMC Master Clients of both the source and
        the destination DCs to report an accepted migration bid or offer (according to
        the IT Load Marketplace) to the DCMC Server.

        Parameters
        ----------
        dc_name : str
            The username of the issuing DC as understood by the DCMC Server.
        role : {'source', 'destination'}
            The role of the issuing DC in the migration.
        delivery_start : str
            The time on which the migration is planned to start.
        delivery_end : str
            The time on which the migration is planned to end.
        transaction : int
            The ID of transaction as kept in the IT Load Marketplace

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(config.DCMCS_HOST_URL, config.DCMCS_USERNAME, config.DCMCS_PASSWORD)
        >>> response = dcmcs.migration(dc_name='DC1', role='destination', delivery_start='2019-10-01T21:00:00Z',
        ...                            delivery_end='2019-10-01T22:00:00Z', transaction=90008)
        >>> response.status_code
        201

        """
        payload = {
            'dc_name': dc_name,
            'role': role,
            'delivery_start': delivery_start,
            'delivery_end': delivery_end,
            'transaction': transaction
        }
        url = urljoin(self.__url, DCMCServerClient.__MIGRATION)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def token(self, payload):
        """Request token from DCMC Server.

        This service is meant to be used by the DCMC Master Client of the destination DC
        to request an access token on behalf of the new VContainer (which will host the
        migrated load) for accessing the VPN set by the DCMC Server.

        Parameters
        ----------
        payload : dict
            The payload to send to token endpoint. It includes the following keys:
                username : str
                    The username of the issuing DC for gaining authorization in the DCMC Server.
                password : str
                    The password of the issuing DC for gaining authorization in the DCMC Server.

        Returns
        -------
        token : str
            A Keycloak access token

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(config.DCMCS_HOST_URL, config.DCMCS_USERNAME, config.DCMCS_PASSWORD)
        >>> payload = {'username': config.KEYCLOAK_USERNAME, 'password': config.KEYCLOAK_PASSWORD}
        >>> token = dcmcs.token(payload)

        """
        url = urljoin(self.__url, DCMCServerClient.__TOKEN)
        response = requests.post(url=url, data=payload)
        return response.json()['access_token'] if response.status_code == status.HTTP_201_CREATED else None

    def vcontainer(self, cpu, cpu_uom, ram, ram_uom, disk, disk_uom, start, end, transaction):
        """Send VContainer details to the DCMC Server.

        This service is meant to be used by the DCMC Master Client of the source DC
        in order to sent the VContainer details to the DCMC Server. The DCMC Server
        will then forward these details to the DCMC Master Client of the destination
        DC and the creation of the VContainer will be triggered.

        Parameters
        ----------
        cpu : int
            The number of CPU cores of the VC described
        cpu_uom : str
            The unit of measurement for the CPU value
        ram : int
            The amount of RAM of the VC described
        ram_uom : str
            The unit of measurement of the Ram value
        disk: int
            The amount of disk of the VC described
        disk_uom : str
            The unit of measurement of the VC described
        start : str
            The time from which the VC is planned to be active
        end : str
            The time until which the VC is planned to be active
        transaction : int
            The transaction ID as kept in the IT Load Marketplace

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(config.DCMCS_HOST_URL, config.DCMCS_USERNAME, config.DCMCS_PASSWORD)
        >>> response = dcmcs.vcontainer(cpu=1, cpu_uom='cpu', ram=2048, ram_uom='MB', disk=20, disk_uom='GB',
        ...                             start='2019-10-01T21:00:00Z', end='2019-10-01T22:00:00Z', transaction=90099)
        >>> response.status_code
        201

        """
        payload = {
            'cpu': cpu,
            'cpu_uom': cpu_uom,
            'ram': ram,
            'ram_uom': ram_uom,
            'disk': disk,
            'disk_uom': disk_uom,
            'start': start,
            'end': end,
            'transaction': transaction
        }
        url = urljoin(self.__url, DCMCServerClient.__VCONTAINER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def vcontainer_created(self, transaction_id):
        """Notify DCMC Server of VContainer creation.

        This service is meant to be used by the DCMC Lite Client in order to inform the
        DCMC Server that is has been created for a certain transaction and is ready to
        accept a migrated load.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction in the Catalyst IT Load Marketplace

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(config.DCMCS_HOST_URL, config.DCMCS_USERNAME, config.DCMCS_PASSWORD)
        >>> transaction_id = 80009
        >>> response = dcmcs.vcontainer_created(transaction_id)
        >>> response.status_code
        200

        """
        url = urljoin(self.__url, DCMCServerClient.__VCONTAINER_CREATED).format(transaction_id)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def datacenter_register(self, payload=None):
        """Register Datacenter to the DCMC Server of the federation.

        Parameters
        ----------
        payload : dict
            The payload to send for datacenter registration. Must include the following fields:
                dc_name : str
                    The unique name of the DC to register
                ip_address : URL
                    The host URL of the DCMC Master
                username : str
                    The Keycloak Username of the DCMC Master
                password : str
                    The Keycloak password of the DCMC Master

        Returns
        -------
        Response
            The Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.dcmcs_api import DCMCServerClient
        >>> dcmcs = DCMCServerClient(config.DCMCS_HOST_URL, config.DCMCS_USERNAME, config.DCMCS_PASSWORD)
        >>> payload = {
        ...     "dc_name": "DCX",
        ...     "ip_address": "http://127.0.0.1:80",
        ...     "username": "dc_username",
        ...     "password": "dc_password"
        ... }
        >>> response = dcmcs.datacenter_register(payload)
        >>> response.status_code
        201

        """
        payload = payload if payload is not None else {
            'dc_name': config.OS_DC_NAME,
            'ip_address': config.DCMCM_HOST_URL,
            'username': config.KEYCLOAK_USERNAME,
            'password': config.KEYCLOAK_PASSWORD
        }
        url = urljoin(self.__url, DCMCServerClient.__REGISTER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        return response

    def rollback_destination(self, transaction_id):
        """Inform server for migration rollback at destination

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to rollback migration for

        Returns
        -------
        Response
            The Response object of the issued request

        """
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCServerClient.__ROLLBACK_DESTINATION).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response
