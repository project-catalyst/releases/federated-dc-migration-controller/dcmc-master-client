from datetime import datetime
from urllib.parse import urljoin

from communication_handler import config
from communication_handler.api_clients.http_client.client import Client


class VCG(object):
    """VCG Client Class.

    This class serves as a client to the API exposed by the Virtual Container Generator (VCG).
    It covers all the available endpoints, whether used by the DCMC Master or the DC Client
    only one endpoint is needed, the details of the container.

    Methods
    -------
    container_details(vc_tag)
        Retrieve container details by vc_tag

    """
    __REGISTER_CONTAINER = 'api/datacenter/{dc_name}/container/register/'
    __MIGRATION_PENDING_REGISTER = 'api/datacenter/{dc_name}/container/{vc_tag}/migrate/pending/'
    __MIGRATION_PENDING_CONFIRM = 'api/datacenter/{dc_name}/container/{vc_tag}/migrate/confirm/'
    __TX_VCTAG = 'api/transaction/{tx_hash}/vctag/'
    __AV_CHANGE = 'api/datacenter/{dc_name}/container/{vc_tag}/availability/change/'
    __CONTAINER_DETAILS = 'api/container/{vc_tag}/details/'
    __FLAVOR_DETAILS = 'api/container/{vc_tag}/flavor/'
    __CONTAINER_HISTORY = 'api/container/{vc_tag}/history/'
    __MIGRATION_PENDING_RETRIEVE = 'api/container/{vc_tag}/migration/pending/'

    def __init__(self, vcg_host_url):
        """VCG Client Class Constructor.

        Parameters
        ----------
        vcg_host_url : str
            The host URL of the VCG.

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = vcg_host_url
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def register_container(self, dc, vcpu, vram, vdisk, uuid, creation_date, ip, available=True):
        """Register a new container record and get transaction hash.

        Parameters
        ----------
        available : boolean
            Denotes if the container is available or not
        dc : str
            The name of the datacenter
        vcpu : int
            The vCPUs of the container
        vram : int
            The Container's RAM
        vdisk : int
            The disk of the container
        uuid : str
            The Container's OpenStack UUID
        ip : URL
            The container's URL
        creation_date : str
            The container's creation date

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        payload = {
            'vc_type': config.VC_TYPE,
            'available': available,
            'vcpu': vcpu,
            'vram': vram,
            'vdisk': vdisk,
            'id': uuid,
            'ip_address': ip,
            'controller': config.OS_CONTROLLER,
            'created': creation_date
        }
        params = {'dc_name': dc}
        url = urljoin(self.__url, VCG.__REGISTER_CONTAINER).format(**params)
        response = self.__client.post(url=url, payload=payload, headers=self.__headers)
        return response

    def get_transaction_vctag(self, tx_hash):
        """Retrieve the transaction VC tag.

        Parameters
        ----------
        tx_hash : str
            The transaction hash to fetch VC tag for

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        params = {'tx_hash': tx_hash}
        url = urljoin(self.__url, VCG.__TX_VCTAG).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def container_availability(self, dc_name, vc_tag, status):
        """Change the availability of the given container.

        Parameters
        ----------
        dc_name : str
            The name of the Datacenter
        vc_tag : str
            The VC Tag of the container
        status : boolean
            Is the container available?

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        payload = {'status': status, 'timestamp': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')}
        params = {'dc_name': dc_name, 'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__AV_CHANGE).format(**params)
        response = self.__client.post(url=url, payload=payload, headers=self.__headers)
        return response

    def flavor_details(self, vc_tag):
        """Retrieve the flavor details of the container from the block chain.

        Parameters
        ----------
        vc_tag : str
            The VC Tag of the container

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        params = {'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__FLAVOR_DETAILS).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def container_history(self, vc_tag):
        """Retrieves the history of operations performed on the container as they are persisted in the block chain.

        Parameters
        ----------
        vc_tag : str
            The VC Tag of the container

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        params = {'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__CONTAINER_HISTORY).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def container_pending_migration(self, vc_tag):
        """Retrieves the pending migrate information of the given container from the block chain (if any).

        Parameters
        ----------
        vc_tag : str
            The VC Tag of the container

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        params = {'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__MIGRATION_PENDING_RETRIEVE).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response

    def register_pending_migration(self, dc_name, vc_tag, target, timestamp, price=0, invoice=1):
        """Inform the VCG of a pending migration.

        Parameters
        ----------
        invoice : int
            The number of the invoice
        price : int
            The price of the migration operation
        timestamp : str
            The timestamp of migration start
        target : str
            The target host of the migration
        dc_name : str
            The name of the Datacenter
        vc_tag : str
            The VC tag of the container under migration

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        payload = {
            'target': target,
            'price': price,
            'invoice': invoice,
            'timestamp': timestamp
        }
        params = {'dc_name': dc_name, 'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__MIGRATION_PENDING_REGISTER).format(**params)
        response = self.__client.get(url=url, headers=self.__headers, payload=payload)
        return response

    def confirm_migration(self, dc_name, vc_tag, target, timestamp, price=0, invoice=1):
        """Inform the VCG of a confirmed migration.

        Parameters
        ----------
        invoice : int
            The number of the invoice
        price : int
            The price of the migration operation
        timestamp : str
            The timestamp of migration confirmation
        target : str
            The target host of the migration
        dc_name : str
            The name of the Datacenter
        vc_tag : str
            The VC tag of the container under migration

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)

        """
        payload = {
            'target': target,
            'price': price,
            'invoice': invoice,
            'timestamp': timestamp
        }
        params = {'dc_name': dc_name, 'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__MIGRATION_PENDING_CONFIRM).format(**params)
        response = self.__client.get(url=url, headers=self.__headers, payload=payload)
        return response

    def container_details(self, vc_tag):
        """Retrieve VContainer details by VC tag.

        Parameters
        ----------
        vc_tag : str
            The tag of the VContainer as assigned by the VCG

        Returns
        -------
        Response
            A Response object of the issued request

        Examples
        --------
        >>> from communication_handler import config
        >>> from communication_handler.api_clients.vcg_api import VCG
        >>> vcg = VCG(config.VCG_HOST_URL)
        >>> vc_tag = '0x57f4a457f846c530ad7c2e060df12034fdd4a3949880e2162805b5cd0bf1d2da'
        >>> response = vcg.container_details(vc_tag)
        >>> response.status_code
        200
        >>> response.json()
        {
            'controller': 'http://192.168.1.250',
            'updated': '2019-09-18T12:58:17Z',
            'id': '45743827-e81c-4004-a807-eba0daf282ef',
            'vc_type': 'vm:openstack',
            'owner': '0x10f683d9acc908cA6b7A34726271229B846b0292',
            'available': False,
            'created': '2019-09-18T12:55:26Z',
            'ip_address': 'http://192.168.1.218',
            'host': '0x10f683d9acc908cA6b7A34726271229B846b0292'
        }

        """
        params = {'vc_tag': vc_tag}
        url = urljoin(self.__url, VCG.__CONTAINER_DETAILS).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        return response
