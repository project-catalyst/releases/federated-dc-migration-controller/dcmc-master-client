from communication_handler import config
from communication_handler.api_clients.cloud_openvpn_api import CloudOpenVPNClient
from communication_handler.api_clients.dcmcl_api import DCMCLiteClient
from communication_handler.api_clients.dcmcm_api import DCMCMasterClient
from communication_handler.api_clients.dcmcs_api import DCMCServerClient
from communication_handler.api_clients.itlb_api import ITLBClient
from communication_handler.api_clients.itlmc_api import ITLMConnectorClient
from communication_handler.api_clients.keycloak_api import KeycloakAPIClient
from communication_handler.api_clients.vcg_api import VCG

communication_handler = None


def get_communication_handler():
    """Get an Instance of the CommunicationHandler Class

    Returns
    -------
    communication_handler : CommunicationHandler
        An instance of the CommunicationHandler Class

    Examples
    --------
    >>> from communication_handler.communication_handler import get_communication_handler
    >>> vcontainer_uuid = '0ddce9a8-6c32-4cdb-9b01-07a32d2e8c7f'
    >>> response = get_communication_handler().dcmcm.vcontainer_by_uuid(vcontainer_uuid)
    >>> response.status_code
    200
    >>> response.json()
    {
        "id": 78787,
        "cpu": 1,
        "cpu_uom": "cpu",
        "ram": 2048,
        "ram_uom": "MB",
        "disk": 20,
        "disk_uom": "GB",
        "start": "2019-10-01T21:00:00Z",
        "end": "2019-10-01T22:00:00Z",
        "uuid": "0ddce9a8-6c32-4cdb-9b01-07a32d2e8c7f",
        "transaction": 90099,
        "status": "registered"
    }

    """
    global communication_handler
    if communication_handler is None:
        communication_handler = CommunicationHandler()
    return communication_handler


class CommunicationHandler(object):
    """Communication Handler Class.

    This is a wrapper class for access to all API clients that are necessary for the
    operation of the DCMC Master component. The API clients can be accessed through
    the properties of this class.

    Attributes
    ----------
    covpn : CloudOpenVPNClient
        An instance of the CloudOpenVPNClient Class
    dcmcm : DCMCMasterClient
        An instance of the DCMCMasterClient Class
    dcmcs : DCMCServerClient
        An instance of the DCMCServerClient Class
    itlb : ITLBClient
        An instance of the ITLBClient Class
    itlmc : ITLMConnectorClient
        An instance of the ITLMConnectorClient Class
    keycloak : KeycloakAPIClient
        An instance of the KeycloakAPIClient Class
    vcg : VCG
        An instance of the VCG Class

    Methods
    -------
    dcmcl(dcmcl_host_url)
        Return a DCMC Lite Client for a specific host

    Examples
    --------
    >>> from communication_handler.communication_handler import CommunicationHandler
    >>> communication_handler = CommunicationHandler()
    >>> vcontainer_uuid = '0ddce9a8-6c32-4cdb-9b01-07a32d2e8c7f'
    >>> response = communication_handler.dcmcm.vcontainer_by_uuid(vcontainer_uuid)
    >>> response.status_code
    200
    >>> response.json()
    {
        "id": 78787,
        "cpu": 1,
        "cpu_uom": "cpu",
        "ram": 2048,
        "ram_uom": "MB",
        "disk": 20,
        "disk_uom": "GB",
        "start": "2019-10-01T21:00:00Z",
        "end": "2019-10-01T22:00:00Z",
        "uuid": "0ddce9a8-6c32-4cdb-9b01-07a32d2e8c7f",
        "transaction": 90099,
        "status": "registered"
    }

    """

    def __init__(self):
        """Communication Handler Constructor Class. """
        self.__covpn_client = None
        self.__dcmcm_client = None
        self.__dcmcs_client = None
        self.__dcmcl_clients = {}
        self.__itlb_client = None
        self.__itlmc_client = None
        self.__keycloak_client = None
        self.__vcg_client = None

    @property
    def covpn(self):
        """Cloud OpenVPN Client. """
        if self.__covpn_client is None:
            self.__covpn_client = CloudOpenVPNClient(cloud_openvpn_url=config.COVPN_HOST_URL,
                                                     username=config.KEYCLOAK_USERNAME,
                                                     password=config.KEYCLOAK_PASSWORD)
        return self.__covpn_client

    @property
    def dcmcm(self):
        """DCMC Master Client. """
        if self.__dcmcm_client is None:
            self.__dcmcm_client = DCMCMasterClient(dcmcm_host_url=config.DCMCM_HOST_URL,
                                                   username=config.KEYCLOAK_USERNAME,
                                                   password=config.KEYCLOAK_PASSWORD)
        return self.__dcmcm_client

    @property
    def dcmcs(self):
        """DCMC Server Client. """
        if self.__dcmcs_client is None:
            self.__dcmcs_client = DCMCServerClient(dcmcs_host_url=config.DCMCS_HOST_URL,
                                                   username=config.KEYCLOAK_USERNAME,
                                                   password=config.KEYCLOAK_PASSWORD)
        return self.__dcmcs_client

    @property
    def itlb(self):
        """Energy-aware IT Load Balancer Client. """
        if self.__itlb_client is None:
            self.__itlb_client = ITLBClient(config.ITLB_HOST_URL)
        return self.__itlb_client

    @property
    def itlmc(self):
        """IT Load Marketplace Connector Client. """
        if self.__itlmc_client is None:
            self.__itlmc_client = ITLMConnectorClient(config.ITLMC_HOST_URL)
        return self.__itlmc_client

    @property
    def keycloak(self):
        """Keycloak API Client. """
        if self.__keycloak_client is None:
            self.__keycloak_client = KeycloakAPIClient(keycloak_host_url=config.KEYCLOAK_HOST_URL,
                                                       realm=config.KEYCLOAK_REALM,
                                                       client_id=config.KEYCLOAK_CLIENT_ID,
                                                       client_secret=config.KEYCLOAK_CLIENT_SECRET)
        return self.__keycloak_client

    @property
    def vcg(self):
        """Virtual Container Generator Client. """
        if self.__vcg_client is None:
            self.__vcg_client = VCG(config.VCG_HOST_URL)
        return self.__vcg_client

    def dcmcl(self, dcmcl_host_url):
        """DCMC Lite Client."""
        if dcmcl_host_url not in self.__dcmcl_clients.keys():
            self.__dcmcl_clients[dcmcl_host_url] = DCMCLiteClient(dcmcl_host_url=dcmcl_host_url)
        return self.__dcmcl_clients[dcmcl_host_url]
