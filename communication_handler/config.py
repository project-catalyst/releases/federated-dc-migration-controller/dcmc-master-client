import os

# =================================
#     CLOUD_OPENVPN SETTINGS
# =================================
COVPN_HOST_URL = os.getenv('DCMCM_COVPN_HOST_URL')

# ==================================
#          DCMC MASTER HOST
# ==================================
DCMC_MASTER_HOST = {
    'PROTOCOL': 'http',
    'IP': os.getenv('DCMCM_HOST_IP'),
    'PORT': os.getenv('DCMCM_HOST_PORT')
}
DCMCM_HOST_URL = '{PROTOCOL}://{IP}:{PORT}/'.format(**DCMC_MASTER_HOST)

# =================================
#           DCMC SERVER
# =================================
DCMCS_HOST = {
    'IP': os.getenv('DCMCM_DCMCS_IP'),
    'PORT': os.getenv('DCMCM_DCMCS_PORT'),
    'PROTOCOL': os.getenv('DCMCM_DCMCS_PROTOCOL')
}
DCMCS_HOST_URL = '{PROTOCOL}://{IP}:{PORT}/'.format(**DCMCS_HOST)

# =================================
#       IT LOAD BALANCER
# =================================
ITLB_HOST = {
    'IP': os.getenv('DCMCM_ITLB_IP'),
    'PORT': os.getenv('DCMCM_ITLB_PORT'),
    'PROTOCOL': os.getenv('DCMCM_ITLB_PROTOCOL')
}
ITLB_HOST_URL = '{PROTOCOL}://{IP}:{PORT}'.format(**ITLB_HOST)

# =================================
#   IT LOAD MARKETPLACE CONNECTOR
# =================================
ITLMC_HOST = {
    'IP': os.getenv('DCMCM_ITLMC_IP'),
    'PORT': os.getenv('DCMCM_ITLMC_PORT'),
    'PROTOCOL': os.getenv('DCMCM_ITLMC_PROTOCOL')
}
ITLMC_HOST_URL = '{PROTOCOL}://{IP}:{PORT}/'.format(**ITLMC_HOST)

# =================================
#         KEYCLOAK SETTINGS
# =================================
KEYCLOAK_HOST_URL = os.getenv('DCMCM_KEYCLOAK_URL')
KEYCLOAK_REALM = os.getenv('DCMCM_KEYCLOAK_REALM')
KEYCLOAK_CLIENT_ID = os.getenv('DCMCM_KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_SECRET = os.getenv('DCMCM_KEYCLOAK_CLIENT_SECRET')
KEYCLOAK_USERNAME = os.getenv('DCMCM_KEYCLOAK_USERNAME')
KEYCLOAK_PASSWORD = os.getenv('DCMCM_KEYCLOAK_PASSWORD')

# ==================================
#              VCG HOST
# ==================================
VCG_HOST = {
    'IP': os.getenv('DCMCM_VCG_IP'),
    'PORT': os.getenv('DCMCM_VCG_PORT'),
    'PROTOCOL': os.getenv('DCMCM_VCG_PROTOCOL')
}
VCG_HOST_URL = '{PROTOCOL}://{IP}:{PORT}'.format(**VCG_HOST)

# ==================================
#             DC SETTINGS
# ==================================
OS_CONTROLLER = os.getenv('DCMCM_OS_CONTROLLER')
OS_DC_NAME = os.getenv('DCMCM_OS_DC_NAME')
VC_TYPE = 'vm:openstack'
