import string

import paramiko
import random
import docker
import docker.errors

from api import constants, config


def get_ram_in_mb(ram, uom):
    """Returns the RAM in MB.

    Parameters
    ----------
    ram : int
        The RAM in MB or GB
    uom : str
        The RAM's Unit of Measurement

    Returns
    -------
    ram_mb : int
        The RAM in MB

    """
    return ram if uom == 'MB' else ram * 1024


def run_command_over_ssh(server, port=22, username='user', password=None, command=None):
    """Runs a command over ssh.

    Parameters
    ----------
    server : str
        The IP/hostname of the server to run the command to
    port : int
        The SSH port
    username : str
        The host username
    password : str
        The host password
    command : str
        The command to run

    Returns
    -------
    tuple
        The tuple (stdin, stdout, stderr) of the ssh command

    """
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=server, port=port, username=username, password=password)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command=command)
    return ssh_stdin, ssh_stdout, ssh_stderr


def generate_password(length=16):
    """Returns a random password of lower & upper case letters and numbers.

    Parameters
    ----------
    length : int
        The length of the desired password

    Returns
    -------
    password : str
        The generated password

    """
    characters = list(string.ascii_letters + string.digits)
    random.shuffle(characters)
    password = "".join([random.choice(characters) for _ in range(length)])
    return password


def get_vpn_ip(transaction_id):
    """Get the VPN IP by transaction id.

    Parameters
    ----------
    transaction_id : int
        The id of the transaction to fetch VPN IP for

    Returns
    -------

    """
    try:
        docker_client = docker.from_env()
        ovpncli_container_name = constants.COVPN_CLIENT_CONTAINER_NAME.format(transaction_id)
        ovpncli_container = docker_client.containers.get(ovpncli_container_name)
        _, output = ovpncli_container.exec_run(cmd='get_vpn_ip {}'.format(config.TUN_DEVICE))
        return output.decode('utf-8').strip()
    except (docker.errors.NotFound, docker.errors.APIError):
        return None
