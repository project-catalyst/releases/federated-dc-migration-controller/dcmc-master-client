from api.constants import PENDING
from api.models import LoadValue, Load, Migration


def create_load_values(load, load_values):
    """Create LoadValues for recently created Load.

    Parameters
    ----------
    load : Load
        A Load object to create LoadValues for
    load_values : list
        A list of load_values. It should include the following parameters:
            parameter : str
                The type of the load parameter
            uom : str
                The unit of measurement of the parameter
            value : float
                The value of the parameter

    """
    for load_value_data in load_values:
        LoadValue.objects.create(load=load, parameter=load_value_data['parameter'],
                                 value=load_value_data['value'], uom=load_value_data['uom'])


def get_or_create_load(data):
    """Get existing Load or create new.

    Parameters
    ----------
    data : dict
        A dictionary of received data

    Returns
    -------
    load : Load
        A retrieved or created Load object

    """
    # Fetch or create Load
    load = Load.objects.prefetch_related('load_values', 'migrations').filter(vc_tag=data['vc_tag'])
    if not load.exists():
        load = Load.objects.create(date=data['date'], vc_tag=data['vc_tag'])
        # Create LoadValues
        create_load_values(load, data['load_values'])
    return Load.objects.prefetch_related('load_values', 'migrations').get(vc_tag=data['vc_tag'])


def process_migration_request(data):
    """Process data of a migration request.

    Parameters
    ----------
    data : dict
        A dictionary of received data

    Returns
    -------
    migration_id : int
        The ID of the recently created migration object

    """
    # Get or create Load and LoadValues
    load = get_or_create_load(data)
    # Create Migration
    migration = Migration.objects.create(load=load, action_type=data['action_type'],
                                         price=data['price'], status=PENDING)
    return migration.id
