from rest_framework import status

from communication_handler.communication_handler import get_communication_handler


def check_user_validity(user_token):
    """Checks Keycloak User Token Validity.

    Parameters
    ----------
    user_token : str
        A Keycloak User Authorization Token

    Returns
    -------
    The user if valid, else 'inactive'

    """
    response = get_communication_handler().keycloak.user_introspection(user_token)
    if response.status_code == status.HTTP_200_OK:
        introspected = response.json()
        if introspected != {'active': False}:
            return introspected['username']
        return 'inactive'
    return 'inactive'
