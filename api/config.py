import os

from django.conf import settings

# =================================
#       CLOUD INIT COMMAND
# =================================
CLOUD_INIT_ARGS = {
    'DCMCL_DC_HOST_TYPE': 'all_in_one:virtual',
    'DCMCL_PHYSICAL_NI': 'ens3',
    'DCMCL_KEYCLOAK_USERNAME': '',
    'DCMCL_KEYCLOAK_PASSWORD': '',
    'DCMCL_COVPN_HOST_URL': os.getenv('DCMCM_COVPN_HOST_URL'),
    'DCMCL_COVPN_OWNER': '',
    'DCMCL_COVPN_TENANT': os.getenv('DCMCM_COVPN_TENANT'),
    'DCMCL_DCMCS_HOST_URL': settings.DCMCS_HOST_URL,
    'DCMCL_TRANSACTION_ID': '',
}
CLOUD_INIT_COMMAND = '#!/bin/bash \n./home/ubuntu/dcmcl_setup.sh {DCMCL_DC_HOST_TYPE} {DCMCL_PHYSICAL_NI} ' \
                     '{DCMCL_KEYCLOAK_USERNAME} {DCMCL_KEYCLOAK_PASSWORD} {DCMCL_COVPN_HOST_URL} {DCMCL_COVPN_OWNER} ' \
                     '{DCMCL_COVPN_TENANT} {DCMCL_DCMCS_HOST_URL} {DCMCL_TRANSACTION_ID}'

# =================================
#   SSH COMMANDS FOR CONTROLLER
# =================================
VPN_CREATE_CMD = 'cd openvpn_client && docker run -dit --network host --device=/dev/net/tun --cap-add NET_ADMIN ' \
                 '--env-file .env_initiator --name catalyst-ovpncli-init catalyst-openvpn-client initiate'
VPN_SHARE_CMD = 'docker exec -i catalyst-ovpncli-init share_connection {}'
VPN_IP_CMD = 'ifconfig tun0 ' \
             '| grep -Eo \'inet (addr:)?([0-9]*\.){3}[0-9]*\' ' \
             '| grep -Eo \'([0-9]*\.){3}[0-9]*\' ' \
             '| grep -v \'127.0.0.1\''

# =================================
#   CLOUD_OPENVPN CLIENT SETTINGS
# =================================
COVPN_ENV_VARIABLES = {
    'OVPNCLI_COVPN_SERVER_IP': os.getenv('DCMCM_COVPN_HOST_URL'),
    'OVPNCLI_KEYCLOAK_URL': os.getenv('DCMCM_KEYCLOAK_URL'),
    'OVPNCLI_KEYCLOAK_REALM': os.getenv('DCMCM_KEYCLOAK_REALM'),
    'OVPNCLI_KEYCLOAK_CLIENT_ID': os.getenv('DCMCM_KEYCLOAK_CLIENT_ID'),
    'OVPNCLI_KEYCLOAK_CLIENT_SECRET': os.getenv('DCMCM_KEYCLOAK_CLIENT_SECRET'),
    'VPN_CONNECTION_USERNAME': os.getenv('DCMCM_KEYCLOAK_USERNAME'),
    'VPN_CONNECTION_PASSWORD': os.getenv('DCMCM_KEYCLOAK_PASSWORD'),
    'VPN_CONNECTION_TENANT': os.getenv('DCMCM_COVPN_TENANT'),
    'VPN_CONNECTION_PORT': os.getenv('DCMCM_COVPN_PORT')
}

# =================================
#       DCMCL DEFAULT PORT
# =================================
DCMCL_DEFAULT_PORT = os.getenv('DCMCM_DCMCL_DEFAULT_PORT')

# =================================
#            VPN DEVICE
# =================================
TUN_DEVICE = os.getenv('DCMCM_TUN_DEVICE')

# ==================================
#        OPENSTACK SETTINGS
# ==================================
OS_DC_NAME = os.getenv('DCMCM_OS_DC_NAME')
OS_CONTROLLER = os.getenv('DCMCM_OS_CONTROLLER')
OS_CONTROLLER_USER = os.getenv('DCMCM_OS_CONTROLLER_USER')
OS_CONTROLLER_PWD = os.getenv('DCMCM_OS_CONTROLLER_PWD')
