from drf_yasg.utils import swagger_auto_schema

from api.serializers import MigrationRequestSerializer, MigrationUpdateSerializer, HypervisorSerializer, \
    MigrationDetailsSerializer
from api.swagger.parameters import *
from api.swagger.responses import *

# ==================================
#        MIGRATION REQUEST
# ==================================
migration_request = swagger_auto_schema(
    operation_summary='Migration Request',
    operation_description='Place a migration request',
    request_body=MigrationRequestSerializer,
    responses=MigrationRequest_POST
)

# ==================================
#        MIGRATION DETAILS
# ==================================
migration_details_create = swagger_auto_schema(
    operation_summary='Migration Details',
    operation_description='Provide migration details',
    request_body=MigrationDetailsSerializer,
    responses=MigrationDetails_POST
)

# ==================================
#            MIGRATION
# ==================================
migration_retrieve = swagger_auto_schema(
    operation_summary='Retrieve Migration',
    operation_description='Retrieve migration associated with market action',
    manual_parameters=[MARKET_ACTION_ID],
    responses=Migration_GET
)
migration_update = swagger_auto_schema(
    operation_summary='Update Migration',
    operation_description='Associate migration with the CATALYST Marketplace Information',
    request_body=MigrationUpdateSerializer,
    responses=Migration_PUT
)

# ==================================
#            VCONTAINER
# ==================================
vcontainer_create = swagger_auto_schema(
    operation_summary='Create VContainer',
    operation_description='Trigger VC Creation',
    request_body=VContainerSerializer,
    responses=VContainer_POST
)
vcontainer_retrieve = swagger_auto_schema(
    operation_summary='Retrieve VContainer',
    operation_description='Retrieve transaction associated to VM',
    manual_parameters=[VCONTAINER_UUID],
    responses=VContainer_GET
)
vcontainer_update = swagger_auto_schema(
    operation_summary='Update VContainer',
    operation_description='Inform about vCompute\'s registration',
    request_body=VContainerSerializer,
    responses=VContainer_PATCH
)

# ==================================
#            CONTROLLER
# ==================================
controller_retrieve = swagger_auto_schema(
    operation_summary='Controller details',
    operation_description='Provide CTL Details',
    responses=Controller_GET,
    manual_parameters=[TRANSACTION_ID]
)

# ==================================
#            HYPERVISOR
# ==================================
hypervisor_register = swagger_auto_schema(
    operation_summary='Hypervisor Register',
    operation_description='Register a Hypervisor through DCMC Lite',
    request_body=HypervisorSerializer,
    responses=Hypervisor_POST
)

# ==================================
#           VPN CREATE
# ==================================
vpn_create = swagger_auto_schema(
    operation_summary='Create VPN',
    operation_description='Command DCMC Master to initiate VPN connection',
    responses=VpnCreate_POST,
    manual_parameters=[TRANSACTION_ID]
)

# ==================================
#           VPN CREATE
# ==================================
migration_rollback = swagger_auto_schema(
    operation_summary='Rollback Migration',
    operation_description='Migrate VC back to original CMP node',
    responses=MigrationRollback_GET,
    manual_parameters=[TRANSACTION_ID]
)

# ==================================
#    SERVICE ENDPOINTS RETRIEVE
# ==================================
service_endpoints_retrieve = swagger_auto_schema(
    operation_summary='Service Endpoints',
    operation_description='Retrieve service endpoints needed by Nova',
    responses=ServiceEndpoints_GET,
    manual_parameters=[TRANSACTION_ID]
)
