from drf_yasg import openapi

MARKET_ACTION_ID = openapi.Parameter(
    name='marketaction',
    in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description='The ID of the Marketaction to fetch migration for',
    required=True
)

VCONTAINER_UUID = openapi.Parameter(
    name='uuid',
    in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description='The UUID of the VContainer',
    required=True
)

TRANSACTION_ID = openapi.Parameter(
    name='transaction_id',
    in_=openapi.IN_PATH,
    type=openapi.TYPE_INTEGER,
    description='The ID of the transaction as kept in the CATALYST IT Load Marketplace',
    required=True
)
