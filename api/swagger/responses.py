from rest_framework import status

from api.serializers import MigrationSerializer, ControllerSerializer, VContainerSerializer, ServiceEndpointSerializer

# ==================================
#    MIGRATION DETAILS RESPONSES
# ==================================
MigrationDetails_POST = {
    status.HTTP_201_CREATED: 'Migration details were provided. Details object was created.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'No Load or Migration objects were found by this marketaction',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#        MIGRATION RESPONSES
# ==================================
Migration_GET = {
    status.HTTP_200_OK: MigrationSerializer,
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'Requested Migration not Found',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}
Migration_PUT = {
    status.HTTP_200_OK: 'Migration was associated with the CATALYST Marketplace Information',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'The migration under update was not found',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#     MIGRATION REQUEST RESPONSE
# ==================================
MigrationRequest_POST = {
    status.HTTP_201_CREATED: 'Migration request was placed. Migration object was created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#      VCONTAINER RESPONSES
# ==================================
VContainer_POST = {
    status.HTTP_201_CREATED: 'VC Creation was triggered. VC Object was created.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed access to this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}
VContainer_GET = {
    status.HTTP_200_OK: VContainerSerializer,
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'Requested resource not Found',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}
VContainer_PATCH = {
    status.HTTP_200_OK: 'Information about VM registration were received.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'The resource under update was not found',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#        CONTROLLER RESPONSE
# ==================================
Controller_GET = {
    status.HTTP_200_OK: ControllerSerializer,
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'Requested controller information were not found.',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#       HYPERVISOR RESPONSES
# ==================================
Hypervisor_POST = {
    status.HTTP_201_CREATED: 'Hypervisor was registered. Hypervisor object was created.',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed access to this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#       VPN CREATE RESPONSES
# ==================================
VpnCreate_POST = {
    status.HTTP_202_ACCEPTED: 'VPN creation request was accepted',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#    MIGRATION ROLLBACK RESPONSE
# ==================================
MigrationRollback_GET = {
    status.HTTP_202_ACCEPTED: 'VPN creation request was accepted',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}

# ==================================
#       SERVICE ENDPOINTS
# ==================================
ServiceEndpoints_GET = {
    status.HTTP_200_OK: ServiceEndpointSerializer,
    status.HTTP_401_UNAUTHORIZED: 'Unauthorized users are not allowed to view this resource',
    status.HTTP_403_FORBIDDEN: 'Operation forbidden for this type of user',
    status.HTTP_404_NOT_FOUND: 'No migration was found by this transaction ID',
    status.HTTP_405_METHOD_NOT_ALLOWED: 'This type of method is not allowed for this endpoint',
    status.HTTP_500_INTERNAL_SERVER_ERROR: 'Internal Server Error'
}
