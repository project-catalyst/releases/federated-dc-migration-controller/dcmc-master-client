from django.conf import settings
from django.conf.urls import url
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.urlpatterns import format_suffix_patterns

from api import views


# ==================================
#   SWAGGER ADD-ONS
# ==================================
API_DESCRIPTION = """The API exposed by the DCMC Master Client component (H2020 Catalyst Project).

The `swagger-ui` view can be found [here](/catalyst/dcmcm/api/docs).  
The `ReDoc` view can be found [here](/catalyst/dcmcm/api/redoc).  
The swagger `YAML` document can be found [here](/catalyst/dcmcm/api/swagger.yaml).  
"""

schema_view = get_schema_view(
    openapi.Info(
        title='[H2020 CATALYST] DCMC Master Client API',
        default_version='v1',
        description=API_DESCRIPTION,
        terms_of_service='',
        contact=openapi.Contact(email=''),
        license=openapi.License(name='LGPLv3')
    ),
    url='http://{}:{}'
        .format(settings.DCMC_MASTER_HOST['IP'], settings.DCMC_MASTER_HOST['PORT']),
    validators=[],
    public=True,
    permission_classes=(permissions.AllowAny,),
)

# ==================================
#   API ENDPOINTS
# ==================================
api_urlpatterns = [
    # Migration Request API Endpoint
    url(r'^migration/request/$',
        views.MigrationRequest.as_view(),
        name='migration-request-placement'),

    # Migration API Endpoints
    url(r'^migration/marketplace/update/$',
        views.MigrationUpdateView.as_view(),
        name='migration-marketplace-update'),
    url(r'^migration/marketaction/(?P<marketaction>[^/]+)/$',
        views.MigrationByMarketactionRetrieveView.as_view(),
        name='migration-by-marketaction'),
    url(r'^migration/details/$',
        views.MigrationDetailsCreateView.as_view(),
        name='migration-details-create'),

    # Controller API Endpoint
    url(r'^transaction/(?P<transaction_id>[^/]+)/controller/$',
        views.ControllerRetrieveView.as_view(),
        name='retrieve-ctl-details'),

    # VContainer API Endpoint
    url(r'^vcontainer/$',
        views.VContainerCreateView.as_view(),
        name='vcontainer-create'),

    # Transaction API Endpoint
    url(r'^transaction/vcontainer/(?P<uuid>[^/]+)/$',
        views.VContainerRetrieveView.as_view(),
        name='vcontainer-retrieve-ssid'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/vcontainer/status/registered/$',
        views.VContainerRegisteredView.as_view(),
        name='vcontainer-status-update'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/vpn/create/$',
        views.VpnCreateView.as_view(),
        name='create-transaction-vpn'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/migration/rollback/$',
        views.MigrationRollbackView.as_view(),
        name='rollback-migration'),
    url(r'^transaction/(?P<transaction_id>[^/]+)/service-endpoints/$',
        views.ServiceEndpointsView.as_view(),
        name='service-endpoints'),

    # Hypervisor API Endpoint
    url(r'^hypervisor/register/$',
        views.HypervisorRegisterView.as_view(),
        name='hypervisor-register'),

    # Documentation
    url(r'^swagger\.json/$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger\.yaml/$', schema_view.without_ui(cache_timeout=0), name='schema-yaml'),
    url(r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

api_urlpatterns = format_suffix_patterns(api_urlpatterns)
