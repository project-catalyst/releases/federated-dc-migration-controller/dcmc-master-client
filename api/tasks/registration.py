import logging

from celery.signals import worker_ready
from rest_framework import status

from api import constants
from api.models import Hypervisor
from communication_handler.communication_handler import get_communication_handler
from datacenter_handler.datacenter_handler import get_dc_handler

logger = logging.getLogger(constants.CELERY_TASKS)


@worker_ready.connect()
def register_to_dcmc_server(**kwargs):
    """Auto-register self to DCMC Server upon startup.

    This task is responsible for the auto-registration of a DC, through its DCMC Master Client,
    to the federation's DCMC Server. It is executed upon the initialization of DCMC Master.

    """
    task_tag = 'task:register_to_dcmc_server'
    logger.info('[{}] Attempting to register to DCMC Server'.format(task_tag))

    # Send registration request to DCMC Server
    response = get_communication_handler().dcmcs.datacenter_register()
    if response.status_code != status.HTTP_201_CREATED:
        logger.warning('[{}] Registration has failed with status code [{}]. Response body: `{}`.'
                       .format(task_tag, response.status_code, response.text))
        return
    logger.info('[{}] Registration to DCMC Server was successful'.format(task_tag))


@worker_ready.connect()
def record_and_register_hypervisors(**kwargs):
    """Retrieve and register the hypervisors available upon deployment. """
    task_tag = 'task:record_and_register_hypervisors'
    logger.info('[{}] Retrieving and registering hypervisors'.format(task_tag))

    # Retrieve and record available hypervisors
    hypervisors = get_dc_handler().get_hypervisors()
    try:
        for hypervisor in hypervisors:
            h = Hypervisor.objects.create(host_ip=hypervisor.host_ip,
                                          host_name=hypervisor.hypervisor_hostname)
            logger.info('[{}] Registered hypervisor: {}'.format(task_tag, h))
    except Exception as e:
        logger.error('[{}] An exception has occurred: `{}`'.format(task_tag, e))
