import ipaddress
import logging
import time

import docker
import docker.errors
from rest_framework import status

from api import constants, config
from api.models import MigrationOperationalDetails
from api.utils.misc import run_command_over_ssh, get_vpn_ip
from communication_handler.communication_handler import get_communication_handler
from dcmc_master_client.celery import app

logger = logging.getLogger(constants.CELERY_TASKS)


@app.task
def create_and_share_vpn(transaction_id, share_with='remote_device'):
    """Initiate VPN in the Controller and share with destination DCMC.

    This task initiates the VPN connection in the controller. When the VPN connection
    is established it is shared with the DCMC Lite Client of the destination DC for the
    VCompute registration to begin.

    This task can be used if the DCMC Master component is deployed in a machine different
    from the controller.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction in the marketplace
    share_with : str
        The DC to share connection with

    """
    task_tag = 'task:create_and_share_vpn'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Initiating VPN connection ...'.format(task_tag, transaction_tag))

    # Do create VPN connection
    _, _, _ = run_command_over_ssh(server=config.OS_CONTROLLER, username=config.OS_CONTROLLER_USER,
                                   password=config.OS_CONTROLLER_PWD, command=config.VPN_CREATE_CMD)

    # Check if VPN IP can be retrieved
    while True:
        time.sleep(15)
        _, ssh_stdout, _ = run_command_over_ssh(server=config.OS_CONTROLLER, username=config.OS_CONTROLLER_USER,
                                                password=config.OS_CONTROLLER_PWD, command=config.VPN_IP_CMD)
        controller_vpn_ip = ssh_stdout.read().strip().decode('utf-8')
        try:
            ipaddress.IPv4Address(controller_vpn_ip)
            break
        except ipaddress.AddressValueError:
            logger.warning('[{}][{}] VPN Connection has not been established yet ...'
                           .format(task_tag, transaction_tag))
            continue
    logger.info('[{}][{}] VPN Connection is established ...'.format(task_tag, transaction_tag))

    # Sharing VPN Connection
    _, _, _ = run_command_over_ssh(server=config.OS_CONTROLLER, username=config.OS_CONTROLLER_USER,
                                   password=config.OS_CONTROLLER_PWD, command=config.VPN_SHARE_CMD.format(share_with))
    logger.info('[{}][{}] VPN Connection is shared ...'.format(task_tag, transaction_tag))


@app.task
def create_vpn(transaction_id):
    """Launches a Docker OpenVPN Client.

    This task is responsible for launching an OpenVPN client as a Docker container. It is used by the
    source DCMC Master when the DCMC Server requests the creation of the VPN connection for source DCMC
    and vCMP DCMC Lite client to communicate. When the connection is created, the vCMP, as well as the
    compute node where the VM under migration lies in, are authorized to connect.

    Parameters
    ----------
    transaction_id (int): The ID of the transaction associated with the VPN creation

    Returns
    -------
    ovpncli_init_container
        An Docker container serving as an OpenVPN Client

    """
    task_tag = 'task:create_vpn'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Creating VPN connection ...'.format(task_tag, transaction_tag))

    try:
        # Get Docker Client and Set container name according to Marketplace Transaction ID
        docker_client = docker.from_env()
        ovpncli_container_name = constants.COVPN_CLIENT_CONTAINER_NAME.format(transaction_id)

        # Start OpenVPN Client container to initiate VPN connection. It should be noticed that the
        # network mode is set to host, the NET_ADMIN capability is added and access to /dev/net/tun
        # devices is granted for the container to share the VPN connection with its host.
        ovpncli_init_container = \
            docker_client.containers.run(name=ovpncli_container_name,
                                         image=constants.COVPN_CLIENT_IMAGE,
                                         network_mode='host',
                                         cap_add=['NET_ADMIN'],
                                         devices=['/dev/net/tun:/dev/net/tun:rwm'],
                                         environment=config.COVPN_ENV_VARIABLES,
                                         command='initiate',
                                         detach=True,
                                         auto_remove=True)
        logger.info('[{}][{}] Created VPN container ...'.format(task_tag, transaction_tag))

        # Iterate until VPN connection is created: This is accomplished by retrieving the IP of the tun
        # network interface that has been (or is going to be) created. The IP is retrieved from the tun
        # interface and validated. Until tun interface has a valid IP, this loop keeps executing.
        while True:
            time.sleep(10)
            try:
                ipaddress.IPv4Address(get_vpn_ip(transaction_id))
                break
            except ipaddress.AddressValueError:
                logger.debug('[{}][{}] The VPN connection creation is not completed yet. Waiting ...'
                             .format(task_tag, transaction_tag))
        logger.info('[{}][{}] Created VPN connection ...'.format(task_tag, transaction_tag))

        # Retrieve clients to share connection with from migration operational details. Share connection
        # with both source and destination CMPs: Both source CMP and destination CMP (vCMP) should be
        # authorized in order to be able to obtain the OpenVPN client config and connect to VPN.
        migrations_ops = MigrationOperationalDetails.objects.get(transaction=transaction_id)
        ovpncli_init_container.exec_run(cmd='share_connection {}'.format(migrations_ops.source_cmp))
        ovpncli_init_container.exec_run(cmd='share_connection {}'.format(constants.OS_VCMP_NAME.format(transaction_id)))
        logger.info('[{}][{}] Shared VPN connection with remote clients ...'.format(task_tag, transaction_tag))

        # Command source CMP to connect to VPN. Destination CMP is already connected to VPN. Source CMP
        # should also connect to VPN unless it lies in an all-in-one DC deployment where the controller
        # and compute nodes co-exist in the same machine. With this request, the source CMP is commanded
        # to connect to VPN. Whether this is actually needed is decided on the DCMC Lite Client side.
        response = get_communication_handler().dcmcl(migrations_ops.source_cmp_url).vpn_connect(transaction_id)
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.error('[{}][{}] Source CMP node could not connect to VPN'.format(task_tag, transaction_tag))
            return
        logger.info('[{}][{}] Request for VPN connection has been accepted by the Source CMP'
                    .format(task_tag, transaction_tag))
    except docker.errors.APIError:
        logger.error('[{}][{}] A Docker API Error occurred ...'.format(task_tag, transaction_tag))


@app.task
def remove_vpn(transaction_id):
    """Removes the containerized VPN connection.

    This task is responsible for closing the VPN connection and removing the relevant Docker
    container. Firstly, requests to source CMP and vCMP are issued for them to disconnect from
    VPN. Afterwards the VPN connection is removed and the container is stopped and removed as well.

    Parameters
    ----------
    transaction_id : int
        The transaction ID related to the VPN connection.

    """
    # Remove the requested VPN connection
    task_tag = 'task:remove_vpn'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    ovpncli_container_name = constants.COVPN_CLIENT_CONTAINER_NAME.format(transaction_id)
    logger.info('[{}][{}] Removing VPN connection ...'.format(task_tag, transaction_tag))
    try:
        # Retrieve operation details. Request source CMP and vCMP to disconnect from VPN for the connection
        # to be terminated gracefully. In case these requests fail, the VPN removal is continued in any case.
        migration_ops = MigrationOperationalDetails.objects.get(transaction=transaction_id)
        # Source CMP
        response = get_communication_handler().dcmcl(migration_ops.source_cmp_url).vpn_disconnect(transaction_id)
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.warning('[{}][{}] Source CMP failed to disconnect from VPN'.format(task_tag, transaction_tag))
        logger.info('[{}][{}] Source CMP disconnected from VPN successfully'.format(task_tag, transaction_tag))
        # Destination CMP
        response = get_communication_handler().dcmcl(migration_ops.destination_cmp_url).vpn_disconnect(transaction_id)
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.warning('[{}][{}] Destination CMP failed to disconnect from VPN'.format(task_tag, transaction_tag))
        logger.info('[{}][{}] Destination CMP disconnected from VPN successfully'.format(task_tag, transaction_tag))

        # Retrieve OpenVPN Client container related to transaction.
        docker_client = docker.from_env()
        ovpncli_init_container = docker_client.containers.get(ovpncli_container_name)
        # Remove connection and stop container.
        ovpncli_init_container.exec_run(cmd='remove_connection')
        ovpncli_init_container.stop()
        logger.info('[{}][{}] The VPN connection was removed. Container was cleaned ...'
                    .format(task_tag, transaction_tag))
    except docker.errors.NotFound:
        logger.error('[{}][{}] No container was found by the name [{}].'
                     .format(task_tag, transaction_tag, ovpncli_container_name))
    except docker.errors.APIError:
        logger.error('[{}][{}] A Docker API Error occurred ...'.format(task_tag, transaction_tag))
