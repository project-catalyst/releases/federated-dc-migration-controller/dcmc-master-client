import logging
import os
import time

from rest_framework import status

from api import constants, config
from api.models import MigrationDetails, Migration, VContainer, LoadValue, Hypervisor, MigrationOperationalDetails
from api.tasks.notify import post_migration_status_to_itlb, itlmc_register_migration_updates
from api.tasks.vpn import remove_vpn
from api.utils.misc import get_ram_in_mb, generate_password, get_vpn_ip
from communication_handler.communication_handler import get_communication_handler
from datacenter_handler.datacenter_handler import get_dc_handler
from dcmc_master_client.celery import app

logger = logging.getLogger(constants.CELERY_TASKS)


@app.task
def create_vcontainer(transaction_id):
    """Create VContainer in destination DC.

    This task is executed by the DCMC Master in the destination DC and it is responsible for
    the creation of the requested VContainer in the destination DC. The instantiation of a
    VM is triggered in OpenStack. If the instantiation fails the migration is aborted and the
    Energy Aware IT Load Balancer, along with the IT Load Marketplace connector are informed.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction related to the creation of the VContainer

    """
    task_tag = 'task:create_vcontainer'
    migration_details = MigrationDetails.objects.get(transaction=transaction_id)
    migration = Migration.objects.select_related('load').get(marketaction=migration_details.marketaction)
    transaction_tag = 'Transaction:{}'.format(transaction_id)

    try:
        # Get VContainer
        vcontainer = VContainer.objects.get(transaction=transaction_id)

        # Register the vCMP under creation as a new User: A new user is registered to Keycloak using the vCMP
        # name as username and a randomly generated password. After the creation of the vContainer, the password
        # will be known only to the vContainer itself.
        username, password = constants.OS_VCMP_NAME.format(transaction_id), generate_password()
        logger.info('[{}][{}] Registering new vCMP with username [{}] as a user ...'
                    .format(task_tag, transaction_tag, username))
        response = get_communication_handler().covpn.register_client(username, password)
        if response.status_code != status.HTTP_201_CREATED:
            logger.error('[{}][{}] Could not register vCMP as a user. Aborting.'.format(task_tag, transaction_tag))
            post_migration_status_to_itlb(migration.load.vc_tag, constants.FAILED)
            itlmc_register_migration_updates(migration.marketaction, constants.NOT_DELIVERED)
            return
        logger.info('[{}][{}] vCMP has been registered as user [{}].'.format(task_tag, transaction_tag, username))

        # Create VM
        # User data to be executed by the cloud-init service. Some elements of the cloud-init arguments are
        # pre-configured. Some of the elements that must be updated include the previously created username
        # and password, the transaction ID and the owner of the VPN connection to which the vContainer is
        # going to connect to.
        cloud_init_args = config.CLOUD_INIT_ARGS
        cloud_init_args['DCMCL_KEYCLOAK_USERNAME'] = username
        cloud_init_args['DCMCL_KEYCLOAK_PASSWORD'] = password
        cloud_init_args['DCMCL_TRANSACTION_ID'] = transaction_id
        cloud_init_args['DCMCL_COVPN_OWNER'] = vcontainer.covpn_owner
        user_data = config.CLOUD_INIT_COMMAND.format(**cloud_init_args)

        # Ensure the amount of RAM is expressed in MB.
        ram_in_mb = get_ram_in_mb(vcontainer.ram, vcontainer.ram_uom)

        # Do create VM: Proceed with the actual VM creation. Under normal circumstances where the vm is created
        # without errors, the UUID of the newly created VM is returned, otherwise None is returned. If a UUID is
        # is returned, the vContainer object is updated, otherwise it is deleted and the IT Load Balancer and IT
        # Marketplace Connector are notified of the consequent failure of the migration.
        logger.info('[{}][{}] Creating Vcontainer ...'.format(task_tag, transaction_tag))
        uuid = get_dc_handler().create_vm(cpu=vcontainer.cpu, memory=ram_in_mb, disk=vcontainer.disk,
                                          name=constants.OS_VCMP_NAME.format(transaction_id), user_data=user_data)
        if uuid is not None:
            logger.info('[{}][{}] A VContainer was created with UUID [{}]'.format(task_tag, transaction_tag, uuid))
            vcontainer.uuid = uuid
            vcontainer.save()
        else:
            logger.error('[{}][{}] VContainer creation has failed. Aborting...'.format(task_tag, transaction_tag, uuid))
            # TODO: Decide if vContainer should be deleted
            # Inform ITLB and ITLMC of failure
            post_migration_status_to_itlb(migration.load.vc_tag, constants.FAILED)
            itlmc_register_migration_updates(migration.marketaction, constants.NOT_DELIVERED)
    except VContainer.DoesNotExist:
        logger.error('[{}][{}] No vcontainer with transaction ID [{}] was found'
                     .format(task_tag, transaction_tag, transaction_id))
        post_migration_status_to_itlb(migration.load.vc_tag, constants.FAILED)
        itlmc_register_migration_updates(migration.marketaction, constants.NOT_DELIVERED)
    except Exception as e:
        logger.error('[{}][{}] An error has occurred: `{}`'.format(task_tag, transaction_tag, e))


@app.task
def is_vcmp_registered(transaction_id):
    """Wait until the new vCMP is registered as a Hypervisor.

    After the vCMP is connected to the VPN it should be registered as a hypervisor at the source DC.
    This function polls the hypervisors of the source DC until the new vCMP is actually registered.

    Parameters
    ----------
    transaction_id : int
        The ID of the marketaction corresponding to the migration

    Returns
    -------
    is_registered : bool
        True if the vCMP is registered, False otherwise

    """
    task_tag = 'task:is_vcmp_registered'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Waiting for vCMP registration in Hypervisors'.format(task_tag, transaction_tag))
    retries = 0
    vcmp_hostname = constants.OS_VCMP_NAME.format(transaction_id)
    while retries < 100:
        time.sleep(5)
        hypervisors = get_dc_handler().get_hypervisors()
        for hypervisor in hypervisors:
            if hypervisor.hypervisor_hostname == vcmp_hostname:
                logger.info('[{}][{}] vCMP [{}] has been registered in Hypervisors of the source DC'
                            .format(task_tag, transaction_tag, vcmp_hostname))
                return True
            else:
                logger.debug('[{}][{}] vCMP has not been registered yet'.format(task_tag, transaction_tag))
        retries += 1
    logger.error('[{}][{}] vCMP registration has failed. Aborting...'.format(task_tag, transaction_tag))
    return False


@app.task
def rollback_migration(transaction_id):
    """Rollback the migration back to initial CMP node.

    This tasks handles the live migration of a load from the vCMP node to its original CMP node.
    If it fails, the ITLB and ITLMC are informed accordingly. In the source DC, when the live
    migration is completed, the VPN connection is removed and, followed by the deletion of vCMP
    from compute and neutron agent services. In the destination DC, the vCMP instance is deleted.

    Parameters
    ----------
    transaction_id : int
        The ID of the marketaction corresponding to the migration

    """
    task_tag = 'task:rollback_migration'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Rolling back migration'.format(task_tag, transaction_tag))

    # TODO: Check if ITLMC or ITLB should be informed

    # Get vc_tag from transaction id
    migration_details = MigrationDetails.objects.get(transaction=transaction_id)
    if config.OS_DC_NAME == migration_details.source:
        # Get operational details
        migrations_ops = MigrationOperationalDetails.objects.get(transaction=transaction_id)
        # Migrate back to origin CMP
        logger.info('[{}][{}] DC is source. Migrating back to original CMP.'.format(task_tag, transaction_tag))
        migration_status = get_dc_handler().live_migrate_vm(migrations_ops.vcontainer_uuid, migrations_ops.source_cmp)
        if migration_status != constants.SUCCESS:
            logger.error('[{}][{}] Migration rollback failed. Retrying...'.format(task_tag, transaction_tag))
            return
        logger.info('[{}][{}] Instance was migrated to its original CMP node.'.format(task_tag, transaction_tag))
        # Remove VPN connection
        remove_vpn(transaction_id)
        # Proxy-inform destination DC through DCMC Server for migration rollback
        response = get_communication_handler().dcmcs.rollback_destination(transaction_id)
        if response.status_code != status.HTTP_202_ACCEPTED:
            logger.error('[{}][{}] Rollback destination request failed with status code [{}].'
                         .format(task_tag, transaction_tag, response.status_code))
            logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            return
        logger.info('[{}][{}] Rollback destination request was accepted by DCMC Server.'
                    .format(task_tag, transaction_tag))
        # Delete vCMP from source DC services
        time.sleep(5)
        logger.info('[{}][{}] Deleting vCMP from source DC services.'.format(task_tag, transaction_tag))
        get_dc_handler().handler.delete_service_by_host(migrations_ops.destination_cmp)
        logger.info('[{}][{}] Deleting vCMP Neutron agent from source DC agents.'.format(task_tag, transaction_tag))
        get_dc_handler().handler.delete_network_agent_by_host(migrations_ops.destination_cmp)
    elif config.OS_DC_NAME == migration_details.destination:
        logger.info('[{}][{}] DC is destination. Removing vCMP ...'.format(task_tag, transaction_tag))
        # Get UUID of VContainer and delete
        vcontainer = VContainer.objects.get(transaction=transaction_id)
        get_dc_handler().delete_vm(vcontainer.uuid)
        logger.info('[{}][{}] vCMP has been removed.'.format(task_tag, transaction_tag))


@app.task
def exchange_ssh_keys(transaction_id):
    """Exchange SSH Keys between two participating CMPs.

    For a live migration to be delivered, the participating CMP nodes should have each other's
    public SSH keys in their .ssh/authorized_keys. This task requests the public SSH keys from
    source CMP and vCMP and the exchange is completed by posting these SSH keys to the source
    CMP and vCMP. The SSH key payloads are not saved by the DCMC Master Client.

    Parameters
    ----------
    transaction_id : int
        The ID of the transaction as kept by the IT Load Marketplace

    Returns
    -------
    is_completed : bool
        True if exchange is successful, false otherwise

    """
    task_tag = 'task:exchange_ssh_keys'
    transaction_tag = 'Transaction:{}'.format(transaction_id)

    try:
        # Retrieve migration operational details
        migration_ops = MigrationOperationalDetails.objects.get(transaction=transaction_id)

        # Retrieve SSH Keys
        # Source CMP
        response = get_communication_handler().dcmcl(migration_ops.source_cmp_url).get_public_ssh_key()
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.error('[{}][{}] Could not retrieve SSH key from source CMP'.format(task_tag, transaction_tag))
            logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            return False
        logger.info('[{}][{}] SSH Key was retrieved from source CMP'.format(task_tag, transaction_tag))
        source_ssh_payload = response.json()
        # Destination CMP
        response = get_communication_handler().dcmcl(migration_ops.destination_cmp_url).get_public_ssh_key()
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.error('[{}][{}] Could not retrieve SSH key from destination CMP'.format(task_tag, transaction_tag))
            logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            return False
        logger.info('[{}][{}] SSH Key was retrieved from destination CMP'.format(task_tag, transaction_tag))
        destination_ssh_payload = response.json()

        # Exchange Keys
        # Source CMP
        response = get_communication_handler().dcmcl(migration_ops.source_cmp_url).add_authorized_key(
            destination_ssh_payload)
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.error('[{}][{}] Destination SSH Key could not be authorized in source CMP'
                         .format(task_tag, transaction_tag))
            logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            return False
        logger.info('[{}][{}] Destination SSH Key was authorized in source CMP'.format(task_tag, transaction_tag))
        # Destination CMP
        response = get_communication_handler().dcmcl(migration_ops.destination_cmp_url).add_authorized_key(
            source_ssh_payload)
        if response.status_code not in [status.HTTP_200_OK, status.HTTP_202_ACCEPTED]:
            logger.error('[{}][{}] Source SSH Key could not be authorized in destination CMP'
                         .format(task_tag, transaction_tag))
            logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            return False
        logger.info('[{}][{}] Source SSH Key was authorized in destination CMP'.format(task_tag, transaction_tag))
        return True
    except MigrationOperationalDetails.DoesNotExist:
        logger.error('[{}][{}] No migration with transaction ID [{}] was found'
                     .format(task_tag, transaction_tag, transaction_id))
        return False
    except Exception as e:
        logger.error('[{}][{}] An error has occurred: {}'.format(task_tag, transaction_tag, e))
        return False


@app.task
def live_migrate_instance(transaction_id):
    """Trigger the live migration of an OpenStack instance.

    This tasks handles the live migration of a load from its current compute node to a registered
    virtual compute node (vCMP). The UUID of the VM under migration has been already retrieved from
    the VCG and the live migration starts. If it fails, the ITLB and ITLMC are informed accordingly.

    Parameters
    ----------
    transaction_id : int
        The ID of the marketaction corresponding to the migration

    """
    task_tag = 'task:live_migrate_instance'
    transaction_tag = 'Transaction:{}'.format(transaction_id)

    # Get vc_tag from transaction id and operational details
    migration_details = MigrationDetails.objects.get(transaction=transaction_id)
    migration = Migration.objects.select_related('load').get(marketaction=migration_details.marketaction)
    migration_ops = MigrationOperationalDetails.objects.get(transaction=transaction_id)
    vc_tag = migration.load.vc_tag

    # Default statuses
    itlb_migration_status, itlmc_migration_status = constants.FAILED, constants.NOT_DELIVERED

    # Wait for registration of the new vCMP
    if is_vcmp_registered(transaction_id):
        # If vCMP is registered proceed to SSH-key exchange
        if exchange_ssh_keys(transaction_id):
            # Notify DCMC Lite of source DC for VXLAN configuration
            # TODO: Further review of VXLAN configuration in case of multiple CMPs
            logger.info('[{}][{}] Requesting VXLAN configuration from [source] DCMC Lite ...'
                        .format(task_tag, transaction_tag))
            response = get_communication_handler().dcmcl(migration_ops.source_cmp_url)\
                .vxlan_configure(transaction_id, local_ip=get_vpn_ip(transaction_id),
                                 remote_ip=migration_ops.destination_cmp_url.split(':')[0])
            if response.status_code != status.HTTP_202_ACCEPTED:
                logger.error('[{}][{}] VXLAN configuration request was not accepted by [source] CMP. Aborting'
                             .format(task_tag, transaction_tag))
                logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            logger.info('[{}][{}] VXLAN configuration has been accepted by [source] DCMC Lite.'
                        .format(task_tag, transaction_tag))
            # Do migrate
            logger.info('[{}][{}] Live migrating instance to vCMP ...'.format(task_tag, transaction_tag))
            migration_status = get_dc_handler().live_migrate_vm(migration_ops.vcontainer_uuid,
                                                                constants.OS_VCMP_NAME.format(transaction_id))
            if migration_status == constants.SUCCESS:
                logger.info('[{}][{}] Instance was migrated successfully to vCMP.'.format(task_tag, transaction_tag))
                itlb_migration_status, itlmc_migration_status = constants.SUCCESS, constants.DELIVERED
            else:
                logger.error('[{}][{}] Migration failed. Aborting.'.format(task_tag, transaction_tag))
    # Informing ITLB and ITLMC of migration status update
    post_migration_status_to_itlb(vc_tag, itlb_migration_status)
    itlmc_register_migration_updates(migration.marketaction, itlmc_migration_status)


@app.task
def init_migration_procedures(migration_id, transaction_id):
    """Prepare and start the migration.

    This task performs the prerequisite procedures for the migration. It initially sends a
    migration request to the DCMC Server to report migration bid or offer which has been
    accepted in the IT Load Marketplace.

    If this task is executed by the DCMC Master of the source DC then another request is sent
    to the DCMC Server including details about the VContainer that should be created in the
    destination DC.

    In case any of the aforementioned requests fails then the migration is aborted and both the
    Energy Aware IT Load Balancer and IT Load Marketplace Connector are informed of the abortion.

    Parameters
    ----------
    migration_id : int
        The ID of the migration to start.
    transaction_id : int
        The transaction ID as kept in the Marketplace

    """
    task_tag = 'task:init_migration_procedures'
    transaction_tag = 'Transaction:{}'.format(transaction_id)
    logger.info('[{}][{}] Initiating migration prerequisites.'.format(task_tag, transaction_tag))

    # Get migration details object
    migration = Migration.objects.select_related('load').get(id=migration_id)
    migration_details = MigrationDetails.objects.get(marketaction=migration.marketaction)
    dc_role = constants.SOURCE if config.OS_DC_NAME == migration_details.source else constants.DESTINATION
    vc_tag = migration.load.vc_tag

    # Send migration request to DCMC Server: Reporting accepted pending migration to DCMC Server.
    logger.info('[{}][{}] Sending migration request to DCMC Server'.format(task_tag, transaction_tag))
    response = get_communication_handler().dcmcs.migration(
        dc_name=config.OS_DC_NAME, role=dc_role, transaction=migration_details.transaction,
        delivery_start=migration_details.delivery_start.strftime('%Y-%m-%d %H:%M:%SZ'),
        delivery_end=migration_details.delivery_end.strftime('%Y-%m-%d %H:%M:%SZ')
    )
    if response.status_code != status.HTTP_201_CREATED:
        logger.error('[{}][{}] Migration request to DCMC Server failed with status code [{}]. Aborting...'
                     .format(task_tag, transaction_tag, response.status_code))
        logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
        # Inform ITLB and ITLMC of migration request failure
        post_migration_status_to_itlb(migration.load.vc_tag, constants.FAILED)
        itlmc_register_migration_updates(migration.marketaction, constants.NOT_DELIVERED)
        return
    logger.info('[{}][{}] OK. Migration request was successful.'.format(task_tag, transaction_tag))

    # If the DC role is source, the DCMC Master should retrieve the details of the instance with pending
    # live migration and forward these details to the DCMC Server. Later, the DCMC Server is going to
    # forward these details to the destination DC for a vContainer hosting the vCMP to be created.
    if dc_role is constants.SOURCE:
        # Create migration Operational Details object
        # Firstly get UUID from VCG
        logger.info('[{}][{}] Retrieving container details for current VC tag.'.format(task_tag, transaction_tag))
        # TODO: Uncomment when VCG is really used
        # response = get_communication_handler().vcg.container_details(vc_tag)
        # if response.status_code != status.HTTP_200_OK:
        #     logger.error('[{}][{}] Could not retrieve container details from VCG. Failed with status code [{}]. '
        #                    'Aborting.'.format(task_tag, transaction_tag, response.status_code))
        #     logger.debug('[{}][{}] Response payload `{}`.'.format(task_tag, vc_tag, response.json()))
        #     post_migration_status_to_itlb(vc_tag, constants.FAILED)
        #     itlmc_register_migration_updates(migration.marketaction, constants.NOT_DELIVERED)
        #     return
        # uuid = response.json()['id']
        uuid = os.getenv('DCMCM_VC_UUID')
        logger.info('[{}][{}] OK. Request was successful. UUID is [{}].'.format(task_tag, transaction_tag, uuid))
        # Secondly get hypervisor of server and create MigrationOperationDetails object
        host_name = get_dc_handler().get_hypervisor_of_server(uuid)
        hypervisor = Hypervisor.objects.get(host_name=host_name)
        MigrationOperationalDetails.objects.create(
            source_cmp=host_name, source_cmp_url='{}:{}'.format(hypervisor.host_ip, config.DCMCL_DEFAULT_PORT),
            transaction=migration_details.transaction, vcontainer_uuid=uuid
        )

        # Fetch Load Values
        load_values = LoadValue.objects.select_related('load').filter(load_id=migration.load.id)
        load_value_cpu = load_values.get(parameter='cpu')
        load_value_ram = load_values.get(parameter='ram')
        load_value_disk = load_values.get(parameter='disk')

        # Post to vContainer details to DCMC Server
        logger.info('[{}][{}] Sending VContainer request to DCMC Server'.format(task_tag, transaction_tag))
        response = get_communication_handler().dcmcs.vcontainer(
            cpu=load_value_cpu.value, cpu_uom=load_value_cpu.uom, ram=load_value_ram.value,
            ram_uom=load_value_ram.uom, disk=load_value_disk.value, disk_uom=load_value_disk.uom,
            start=migration_details.delivery_start.strftime('%Y-%m-%d %H:%M:%SZ'),
            end=migration_details.delivery_end.strftime('%Y-%m-%d %H:%M:%SZ'),
            transaction=migration_details.transaction
        )
        if response.status_code != status.HTTP_201_CREATED:
            logger.error('[{}][{}] VContainer request to DCMC Server failed with status code [{}]. Aborting.'
                         .format(task_tag, transaction_tag, response.status_code))
            logger.debug('[{}][{}] Response payload: `{}`'.format(task_tag, transaction_tag, response.json()))
            # In case of failure report to ITLMC and ITLB
            post_migration_status_to_itlb(migration.load.vc_tag, constants.FAILED)
            itlmc_register_migration_updates(migration.marketaction, constants.NOT_DELIVERED)
            return
        logger.info('[{}][{}] OK. VContainer request was successful.'.format(task_tag, transaction_tag))
