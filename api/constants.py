# =================================
#        MIGRATION STATUSES
# =================================
POSTED = 'posted'
REJECTED = 'rejected'
PENDING = 'pending'
SUCCESS = 'success'
FAILED = 'failed'
DELIVERED = 'delivered'
NOT_DELIVERED = 'not_delivered'

# =================================
#       MAX REQUEST RETRIES
# =================================
MAX_RETRIES = 5

# =================================
#             DC ROLES
# =================================
SOURCE = 'source'
DESTINATION = 'destination'

# =================================
#             LOGGERS
# =================================
API_LOGGER = 'api_logger'
CELERY_TASKS = 'celery-tasks'
OPENSTACK = 'openstack'

# =================================
#       OPENSTACK STATUSES
# =================================
ACTIVE = 'ACTIVE'
MIGRATING = 'MIGRATING'
ERROR = 'ERROR'

# ==================================
#     VIRTUAL COMPUTE SETTINGS
# ==================================
OS_VCMP_NAME = 'vcompute-trans-{}'

# =================================
#   CLOUD_OPENVPN CLIENT SETTINGS
# =================================
COVPN_CLIENT_IMAGE = 'catalyst-openvpn-client'
COVPN_CLIENT_CONTAINER_NAME = 'catalyst-ovpncli-init-{}'
