import logging
import os
import subprocess

from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

import api.swagger.decorators as swagger_decorators
from api import config
from api.constants import POSTED, PENDING, REJECTED, API_LOGGER
from api.models import Migration, VContainer, MigrationOperationalDetails, MigrationDetails
from api.permissions import IsKeycloakAuthenticated
from api.serializers import MigrationDetailsSerializer, MigrationSerializer, VContainerSerializer, HypervisorSerializer
from api.tasks.migration import create_vcontainer, init_migration_procedures, live_migrate_instance, rollback_migration
from api.tasks.notify import itlmc_register_migration_action, post_migration_status_to_itlb
from api.tasks.vpn import create_vpn
from api.utils.migration import process_migration_request
from api.utils.misc import get_vpn_ip

logger = logging.getLogger(API_LOGGER)


class MigrationRequest(APIView):
    """Process Migration Request(s) from IT Load Marketplace Connector (ITLMC). """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.migration_request
    def post(self, request, *args, **kwargs):
        try:
            # Check if a single object or array has been issued
            is_many = isinstance(request.data, list)
            if not is_many:
                logger.info('[/migration/request/] Received single migration request.')
                logger.debug('[/migration/request/] Request payload: `{}`'.format(request.data))
                migration_id = process_migration_request(request.data)
                itlmc_register_migration_action.delay(migration_id, request.data)
            else:
                logger.info('[/migration/request/] Received multiple migration requests.')
                logger.debug('[/migration/request/] Request payload: `{}`'.format(request.data))
                for data in request.data:
                    migration_id = process_migration_request(data)
                    itlmc_register_migration_action.delay(migration_id, data)
            logger.info('[/migration/request/] OK. Migration request(s) were processed successfully.')
            return Response(status=status.HTTP_201_CREATED)
        except KeyError:
            logger.error('[/migration/request/] BAD_REQUEST. Missing or malformed data: `{}`'.format(request.data))
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/migration/request/] A server error occurred: {}'.format(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class MigrationUpdateView(APIView):
    """Update Migration with CATALYST Marketplace Info. """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.migration_update
    def post(self, request, *args, **kwargs):
        try:
            # Get request data
            migration_id = request.data.get('migration', None)
            marketaction = request.data['marketaction']
            migration_status = request.data['status']
            # Get migration and update
            if migration_id is not None:
                logger.info('[/migration/marketplace/update/] Updating marketaction and status for migration with ID {}'
                            .format(migration_id))
                migration = Migration.objects.get(id=migration_id)
                migration.marketaction = marketaction
                migration.status = migration_status
                migration.save()
                logger.debug('[/migration/marketplace/update/] Updated Migration: `{}`.'.format(migration))
            else:
                logger.info('[/migration/marketplace/update/] Updating status for migration with marketaction ID [{}]'
                            .format(marketaction))
                migration = Migration.objects.get(marketaction=marketaction)
                migration.status = migration_status
                migration.save()
                logger.debug('[/migration/marketplace/update/] Updated Migration: `{}`.'.format(migration))
            if migration_status in [POSTED, PENDING, REJECTED]:
                # Send migration status update to ITLB
                post_migration_status_to_itlb.delay(migration.load.vc_tag, migration_status)
            logger.info('[/migration/marketplace/update/] OK. Migration was updated successfully.')
            return Response(status=status.HTTP_200_OK)
        except Migration.DoesNotExist:
            logger.warning('[/migration/marketplace/update/] Migration under update does not exist.')
            return Response(status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            logger.error('[/migration/marketplace/update/] BAD_REQUEST. Missing or malformed parameters: `{}`'
                         .format(request.data))
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/migration/marketplace/update/] A server error occurred: {}'.format(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class MigrationByMarketactionRetrieveView(generics.RetrieveAPIView):
    """Retrieve Migration by Marketaction. """
    queryset = Migration.objects.select_related('load').all()
    serializer_class = MigrationSerializer
    permission_classes = (IsKeycloakAuthenticated,)
    lookup_field = 'marketaction'

    @swagger_decorators.migration_retrieve
    def get(self, request, *args, **kwargs):
        try:
            logger.info('[/migration/marketaction/{}/] Retrieving migration with marketaction ID [{}]'
                        .format(kwargs['marketaction'], kwargs['marketaction']))
            return super(MigrationByMarketactionRetrieveView, self).get(request, *args, **kwargs)
        except KeyError:
            logger.error('[/migration/marketaction/{}/] BAD_REQUEST. Malformed or invalid parameters: `{}`.'
                         .format(kwargs['marketaction'], request.data))
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/migration/marketaction/{}/] A server error occurred: {}'.format(kwargs['marketaction'], e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class MigrationDetailsCreateView(generics.CreateAPIView):
    """Provide the details of the migration. """
    serializer_class = MigrationDetailsSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.migration_details_create
    def post(self, request, *args, **kwargs):
        try:
            marketaction = request.data['marketaction']
            logger.info('[/migration/details/] Migration details are created for migration with marketaction ID [{}]'
                        .format(marketaction))
            migration = Migration.objects.select_related('load').get(marketaction=marketaction)
            response = super(MigrationDetailsCreateView, self).post(request, *args, **kwargs)
            if response.status_code == status.HTTP_201_CREATED:
                logger.info('[/migration/details/] CREATED. Migration details were posted. Initiating procedure.')
                init_migration_procedures.delay(migration.id, request.data['transaction'])
            return response
        except Migration.DoesNotExist:
            logger.error('[/migration/details/] NOT_FOUND. No migration was found with marketaction ID [{}]'
                         .format(request.data['marketaction']))
            return Response(status=status.HTTP_404_NOT_FOUND)
        except KeyError:
            logger.error('[/migration/details/] BAD_REQUEST. Invalid or malformed parameters: `{}`'.format(request.data))
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/migration/details/] A server error occurred: {}'.format(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class VContainerCreateView(generics.CreateAPIView):
    """Trigger creation of vContainer. """
    serializer_class = VContainerSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.vcontainer_create
    def post(self, request, *args, **kwargs):
        logger.info('[/vcontainer/] Received request for VContainer Creation')
        response = super(VContainerCreateView, self).post(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            logger.info('[/vcontainer/] VContainer object was created. Creating VM in Openstack...')
            create_vcontainer.delay(request.data['transaction'])
            return response
        else:
            logger.error('[/vcontainer/] Request failed with response code [{}].'.format(response.status_code))
            logger.debug('[/vcontainer/] Payload: `{}`'.format(request.data))
            return response


class ControllerRetrieveView(APIView):
    """Provide Controller Details. """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.controller_retrieve
    def get(self, request, *args, **kwargs):
        try:
            transaction_id = kwargs['transaction_id']
            logger.info('[/transaction/{}/controller/] Received request for controller details.'.format(transaction_id))
            controller_vpn_ip = get_vpn_ip(transaction_id)
            logger.info('[/transaction/{}/controller/] The requested information were retrieved successfully.'
                        .format(transaction_id))
            return Response(
                {
                    'name': config.OS_DC_NAME,
                    'ip': controller_vpn_ip,
                    'port': settings.DCMC_MASTER_HOST['PORT']
                },
                status=status.HTTP_200_OK
            )
        except (ValueError, KeyError) as e:
            logger.error('[/transaction/_/controller/] The IP address retrieved is not valid: {}'.format(e))
            return Response({'error': 'The requested controller information were not found'},
                            status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.error('[/transaction/{}/controller/] A server error occurred: {}'.format(transaction_id, e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class VpnCreateView(APIView):
    """Create VPN in Controller. """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.vpn_create
    def post(self, request, *args, **kwargs):
        try:
            transaction_id = kwargs['transaction_id']
            logger.info('[/transaction/{}/vpn/create/] Received request for VPN creation.'.format(transaction_id))
            create_vpn.delay(transaction_id)
            return Response(status=status.HTTP_202_ACCEPTED)
        except KeyError:
            logger.error('[/transaction/_/vpn/create/] BAD_REQUEST. Missing or malformed data.')
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/transaction/{}/vpn/create/] A server error occurred: {}'.format(transaction_id, e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class VContainerRetrieveView(generics.RetrieveAPIView):
    """Retrieve transaction associated to VM"""
    queryset = VContainer.objects.all()
    serializer_class = VContainerSerializer
    permission_classes = (IsKeycloakAuthenticated,)
    lookup_field = 'uuid'

    @swagger_decorators.vcontainer_retrieve
    def get(self, request, *args, **kwargs):
        logger.info('[VC-Retrieve] Received request for VContainer by UUID')
        return super(VContainerRetrieveView, self).get(request, *args, **kwargs)


class VContainerRegisteredView(APIView):
    """Update Migration with CATALYST Marketplace Info. """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.vcontainer_update
    def post(self, request, *args, **kwargs):
        try:
            transaction_id = kwargs['transaction_id']
            logger.info('[/transaction/{}/vcontainer/status/registered/] Received request for VContainer registration'
                        .format(transaction_id))
            data = request.data
            migration_ops = MigrationOperationalDetails.objects.get(transaction=transaction_id)
            if data['host_name'] == migration_ops.source_cmp and not migration_ops.is_source_cmp_ready:
                logger.info('[/transaction/{}/vcontainer/status/registered/] Source CMP is ready for live migration'
                            .format(transaction_id))
                migration_ops.source_cmp_url = '{}:{}'.format(data['vpn_ip'], config.DCMCL_DEFAULT_PORT)
                migration_ops.is_source_cmp_ready = True
                migration_ops.save()
            else:
                logger.info('[/transaction/{}/vcontainer/status/registered/] Destination CMP is ready for live '
                            'migration'.format(transaction_id))
                migration_ops.destination_cmp = data['host_name']
                migration_ops.destination_cmp_url = '{}:{}'.format(data['vpn_ip'], config.DCMCL_DEFAULT_PORT)
                migration_ops.is_destination_cmp_ready = True
                migration_ops.save()
                # Add vCMP to hosts
                subprocess.Popen('echo "{} {}" >> /opt/dcmc_master_client/hosts'
                                 .format(data['vpn_ip'], migration_ops.destination_cmp), shell=True)
            logger.info('[/transaction/{}/vcontainer/status/registered/] Operational details have been updated for '
                        'migration: {}'.format(transaction_id, migration_ops))
            if migration_ops.is_source_cmp_ready and migration_ops.is_destination_cmp_ready:
                logger.info('[/transaction/{}/vcontainer/status/registered/] Source and Destination CMPs are ready for '
                            'live migration'.format(transaction_id))
                live_migrate_instance.delay(transaction_id)
            return Response(status=status.HTTP_200_OK)
        except KeyError:
            logger.error('[/transaction/_/vcontainer/status/registered/] BAD_REQUEST. Invalid or missing parameters.')
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/transaction/{}/vcontainer/status/registered/] A server error occurred: {}'
                         .format(transaction_id, e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class HypervisorRegisterView(generics.CreateAPIView):
    """Hypervisor Register View."""
    serializer_class = HypervisorSerializer
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.hypervisor_register
    def post(self, request, *args, **kwargs):
        return super(HypervisorRegisterView, self).post(request, *args, **kwargs)


class MigrationRollbackView(APIView):
    """Migration Rollback View."""
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.migration_rollback
    def get(self, request, *args, **kwargs):
        try:
            transaction_id = kwargs['transaction_id']
            logger.info('[/transaction/{}/migration/rollback] Received request to rollback migration'
                        .format(transaction_id))
            rollback_migration.delay(transaction_id)
            return Response(status=status.HTTP_202_ACCEPTED)
        except KeyError:
            logger.error('[/transaction/_/migration/rollback] BAD_REQUEST. Invalid or missing parameters.')
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            logger.error('[/transaction/{}/migration/rollback] A server error occurred: {}'.format(transaction_id, e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ServiceEndpointsView(APIView):
    """Provide Controller Details. """
    permission_classes = (IsKeycloakAuthenticated,)

    @swagger_decorators.service_endpoints_retrieve
    def get(self, request, *args, **kwargs):
        try:
            transaction_id = kwargs['transaction_id']
            logger.info('[/transaction/{}/service-endpoints/] Received request for service endpoints.'
                        .format(transaction_id))
            MigrationDetails.objects.get(transaction=transaction_id)
            logger.info('[/transaction/{}/service-endpoints/] The requested information were retrieved successfully.'
                        .format(transaction_id))
            return Response(
                {
                    'authenticate_uri': os.getenv('DCMCM_OS_AUTHENTICATE_URI', ''),
                    'authorization_url': os.getenv('DCMCM_OS_AUTHORIZATION_URL', ''),
                    'glance_service': os.getenv('DCMCM_OS_GLANCE_SERVICE', ''),
                    'memcached_service': os.getenv('DCMCM_OS_MEMCACHED_SERVICE', ''),
                    'memcached_secret_key': os.getenv('DCMCM_OS_MEMCACHED_SECRET_KEY', ''),
                    'neutron_service': os.getenv('DCMCM_OS_NEUTRON_SERVICE', ''),
                    'neutron_password': os.getenv('DCMCM_OS_NEUTRON_PASS', ''),
                    'nova_password': os.getenv('DCMCM_OS_NOVA_PASS', ''),
                    'placement_service': os.getenv('DCMCM_OS_PLACEMENT_SERVICE', ''),
                    'placement_password': os.getenv('DCMCM_OS_PLACEMENT_PASS', ''),
                    'transport_url': os.getenv('DCMCM_OS_TRANSPORT_URL', ''),
                    'nfs_server_host': os.getenv('DCMCM_OS_NFS_SERVER_HOST'),
                    'nfs_server_root': os.getenv('DCMCM_OS_NFS_SERVER_ROOT'),
                    'nova_uid': os.getenv('DCMCM_OS_NOVA_UID', ''),
                    'nova_gid': os.getenv('DCMCM_OS_NOVA_GID', ''),
                    'libvirt_uid': os.getenv('DCMCM_OS_LIBVIRT_UID', ''),
                    'libvirt_gid': os.getenv('DCMCM_OS_LIBVIRT_GID', ''),
                    'libvirt_group': os.getenv('DCMCM_OS_LIBVIRT_GROUP', ''),
                    'libvirt_user': os.getenv('DCMCM_OS_LIBVIRT_USER', '')
                },
                status=status.HTTP_200_OK
            )
        except KeyError:
            logger.error('[/transaction/_/service-endpoints/] BAD_REQUEST. Invalid or missing parameters.')
            return Response(status=status.HTTP_400_BAD_REQUEST)
        except MigrationDetails.DoesNotExist as e:
            logger.error('[/transaction/{}/service-endpoints/] No migration found for this transaction id: {}'
                         .format(transaction_id, e))
            return Response(status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.error('[/transaction/{}/service-endpoints/] A server error occurred: {}'.format(transaction_id, e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
