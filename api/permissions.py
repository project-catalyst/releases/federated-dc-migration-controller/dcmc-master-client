from rest_framework import permissions

from api.utils.keycloak import check_user_validity


class IsKeycloakAuthenticated(permissions.BasePermission):
    """Permission granted to Keycloak-authorized users. """

    message = 'You are not authorized to perform this action'

    def has_permission(self, request, view):
        authorization_header = request.META.get('HTTP_AUTHORIZATION', None)
        if authorization_header is None:
            return False
        user_token = authorization_header.replace('Bearer ', '')
        user_state = check_user_validity(user_token)
        return True if user_state != 'inactive' else False
