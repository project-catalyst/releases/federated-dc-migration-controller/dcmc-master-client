.. DCMC Master Client documentation master file, created by
   sphinx-quickstart on Mon Sep 30 15:26:28 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#########################
DCMC Master Documentation
#########################
This is the documentation for the code implementing the Master Client of the DC Migration Controller (DCMC) of the
H2020 Catalyst Project. The DCMC Master resides in every DC of the CATALYST federation and performs preparatory tasks
for the actual migration. Normally, this component is deployed in the Controller of the OpenStack installation,
but could run in other nodes as well.

For more details about the role of the DCMC Master in the overall DC Migration Controller, please refer to H2020 CATALYST
D3.3 *Federated DCs Migration Controller* deliverable report.


.. toctree::
   :maxdepth: 3
   :caption: Table of Contents:

*************************
Installation & Deployment
*************************
In the following, the essentials for the installation of a DCMC Master instance are described, including
prerequisites for the deployment, the configuration of the services and the actual deployment.

.. toctree::
   :maxdepth: 2

Prerequisites
=============
For the deployment of the DCMC Master component the Docker engine as well as docker-compose should be installed.
These actions can be performed following the instructions provided below. Firstly, an update should be performed
and essential packages should be installed::

   sudo apt-get update
   sudo apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common

Secondly the key and Docker repository should be added::

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg - sudo apt-key add -
   sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"

Then another update is performed, Docker is installed and the user is added to docker group::

   sudo apt-get update
   sudo apt-get install -y docker-ce
   sudo groupadd docker
   sudo usermod -aG docker $USER

Finally, docker-compose should be installed::

   sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   sudo chmod +x /usr/local/bin/docker-compose

For more information about Docker and docker-compose, please check at https://docs.docker.com/.

Environmental Parameters
========================
The services that are executed as containers, thus forming the DCMC Master receive their configurations
from a .env file located in the root of the repository. In the following tables, the environmental parameters
that are necessary for the configuration and deployment of the DCMC Master are recorded and described.

|

.. list-table:: Docker and Internal Services
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - PG_IMAGE_TAG
     - `Postgres`
     - Docker Image Tag
   * - PG_PORT
     - `Postgres`
     - Port
   * - PG_USER
     - `Postgres`
     - User
   * - PG_PASSWORD
     - `Postgres`
     - Password
   * - PG_DB
     - `Postgres`
     - Database
   * - REDIS_IMAGE_TAG
     - `Redis`
     - Docker Image Tag
   * - REDIS_PORT
     - `Redis`
     - Port

|

.. list-table:: OpenStack Services & Credentials
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - DCMCM_OS_DC_NAME
     - `OpenStack`
     - DC Name
   * - DCMCM_OS_AUTH_URL
     - `OpenStack`
     - Authorization
   * - DCMCM_OS_CONTROLLER
     - `OpenStack`
     - Controller IP
   * - DCMCM_OS_CONTROLLER_USER
     - `OpenStack`
     - Controller's Host Machine User
   * - DCMCM_OS_CONTROLLER_PWD
     - `OpenStack`
     - Controller's Host Machine Pwd
   * - DCMCM_OS_USERNAME
     - `OpenStack`
     - Username
   * - DCMCM_OS_USER_PWD
     - `OpenStack`
     - Password
   * - DCMCM_OS_PROJECT_NAME
     - `OpenStack`
     - Project Name
   * - DCMCM_OS_PROJECT_DOMAIN_ID
     - `OpenStack`
     - Project domain (default: default)
   * - DCMCM_OS_USER_DOMAIN_ID
     - `OpenStack`
     - User Domain (default: default)
   * - DCMCM_OS_TRANSPORT_USER
     - `RabbitMQ`
     - Transport User
   * - DCMCM_OS_TRANSPORT_PASSWORD
     - `RabbitMQ`
     - Transport Password
   * - DCMCM_OS_TRANSPORT_IP
     - `RabbitMQ`
     - Transport IP
   * - DCMCM_OS_TRANSPORT_PORT
     - `RabbitMQ`
     - Transport Port
   * - DCMCM_OS_AUTHENTICATE_URI
     - `Keystone`
     - Service Endpoint
   * - DCMCM_OS_AUTHORIZATION_URL
     - `Keystone`
     - Service Endpoint
   * - DCMCM_OS_GLANCE_SERVICE
     - `Glance`
     - Service Endpoint
   * - DCMCM_OS_MEMCACHED_SERVICE
     - `Memcached`
     - Service Host
   * - DCMCM_OS_MEMCACHED_SECRET_KEY
     - `Memcached`
     - Secret Key
   * - DCMCM_OS_NEUTRON_SERVICE
     - `Neutron`
     - Service Endpoint
   * - DCMCM_OS_NEUTRON_PASS
     - `Neutron`
     - Password
   * - DCMCM_OS_NOVA_PASS
     - `Nova`
     - Password
   * - DCMCM_OS_PLACEMENT_SERVICE
     - `Placement`
     - Service Endpoint
   * - DCMCM_OS_PLACEMENT_PASS
     - `Placement`
     - Password
   * - DCMCM_OS_TRANSPORT_URL
     - `RabbitMQ`
     - Transport URL
   * - DCMCM_OS_NFS_SERVER_HOST
     - `NFS`
     - Host IP
   * - DCMCM_OS_NFS_SERVER_ROOT
     - `NFS`
     - Exposed Directory
   * - DCMCM_OS_NOVA_UID
     - `Nova`
     - UID (nova)
   * - DCMCM_OS_NOVA_GID
     - `Nova`
     - GID (nova)
   * - DCMCM_OS_LIBVIRT_UID
     - `Libvirt` `QEMU`
     - UID (libvirt-qemu)
   * - DCMCM_OS_LIBVIRT_GID
     - `Libvirt` `QEMU`
     - GID (kvm)
   * - DCMCM_OS_LIBVIRT_GROUP
     - `Libvirt`
     - GID (libvirt)
   * - DCMCM_OS_LIBVIRT_USER
     - `Libvirt`
     - User for Libvirt
   * - DCMCM_OS_VCMP_IMAGE
     - `vCMP`
     - Image Name (default: vCompute-Train-v0.9.8-SNAPSHOT)
   * - DCMCM_OS_VCMP_PROJECT
     - `vCMP`
     - Project Name (default: Catalyst)|
   * - DCMCM_OS_VCMP_KEYPAIR
     - `vCMP`
     - Keypair (default: none)
   * - DCMCM_OS_VCMP_MIN_CPU
     - `vCMP`
     - Minimum numbers or CPUs (default: 1)
   * - DCMCM_OS_VCMP_MIN_RAM
     - `vCMP`
     - Minimum amount of RAM (MB) (default: 2048)
   * - DCMCM_OS_VCMP_MIN_DISK
     - `vCMP`
     - Minimum amount of disk (GB) (default: 20)

|

.. list-table:: External Services
   :widths: 35 30 35
   :header-rows: 1

   * - Parameter
     - Tag
     - Description
   * - DCMCM_VCG_IP
     - `VCG` 
     - Host IP
   * - DCMCM_VCG_PORT
     - `VCG`
     - Port
   * - DCMCM_VCG_PROTOCOL
     - `VCG`
     - Protocol
   * - DCMCM_ITLB_IP
     - `IT Load Balancer`
     - Host IP
   * - DCMCM_ITLB_PORT
     - `IT Load Balancer`
     - Port
   * - DCMCM_ITLB_PROTOCOL
     - `IT Load Balancer`
     - Protocol
   * - DCMCM_ITLMC_IP
     - `IT Load Markerplace Connector`
     - Host IP
   * - DCMCM_ITLMC_PORT
     - `IT Load Markerplace Connector`
     - Port
   * - DCMCM_ITLMC_PROTOCOL
     - `IT Load Markerplace Connector`
     - Protocol
   * - DCMCM_DCMCS_IP
     - `DCMC Server`
     - Host IP
   * - DCMCM_DCMCS_PORT
     - `DCMC Server`
     - Port
   * - DCMCM_DCMCS_PROTOCOL
     - `DCMC Server`
     - Protocol
   * - DCMCM_KEYCLOAK_URL
     - `Keycloak`
     - Host URL
   * - DCMCM_KEYCLOAK_REALM
     - `Keycloak`
     - Realm
   * - DCMCM_KEYCLOAK_CLIENT_ID
     - `Keycloak`
     - Client ID
   * - DCMCM_KEYCLOAK_CLIENT_SECRET
     - `Keycloak`
     - Client Secret
   * - DCMCM_KEYCLOAK_USERNAME
     - `Keycloak`
     - Username
   * - DCMCM_KEYCLOAK_PASSWORD
     - `Keycloak`
     - Password
   * - DCMCM_COVPN_HOST_URL
     - `Cloud OpenVPN`
     - Host URL
   * - DCMCM_COVPN_TENANT
     - `Cloud OpenVPN`
     - Tenant
   * - DCMCM_COVPN_PORT
     - `Cloud OpenVPN`
     - Connection Port
   * - DCMCM_DCMCL_DEFAULT_PORT
     - `DCMC Lite`
     - Default Port (default: 60004)


Deployment
==========
The DCMC Master is deployed as collection of Docker containers, utilizing docker-compose. Having cloned the
code of this repository, and having created the .env file the following commands should be executed::

   cp .env /dcmc-master-client
   cd dcmc-master-client
   docker-compose up --build -d

|

***************
DCMC Master API
***************
This is the documentation of the API offered by the DCMC Master component. This documentation is composed by:

  * The API Endpoints;
  * The Django models;
  * Utilities related to interfacing with OpenStack;
  * Asynchronous (Celery) Tasks;
  * Random essential utilities


For information about the interaction with other components and their respective implemented API clients please
refer to the next section of this document. For a detailed overview of the available requests offered byt the API
you may refer to the Swagger documentation available at http://<HOST_IP>:<HOST_PORT>/catalyst/dcmcm/api/docs/.

.. toctree::
   :maxdepth: 2

|

API Endpoints
=============
This section briefly documents the API offered by the DCMC Master Client.

.. openapi:: swagger.yml

|

DB & Django Models
==================
The Migration DB, lying on the DCMC Master of every DC in the Catalyst Federation, includes, essentially,
the following tables:

  * **Load**: The load to be migrated, characterized by its VC Tag
  * **LoadValues**: The values characterizing a load
  * **Migration**: Initial migration information before acceptance
  * **MigrationDetails**: Details of an accepted migration
  * **VContainer**: Representation of a VM created during the migration

The respective Django models for the aforementioned DB tables are documented in this chapter.

.. automodule:: api.models
   :members:

|

Asynchronous Tasks
==================
This section includes the documentation of the essential asynchronous tasks of the DCMC Master.

Migration
---------
This subsection includes documentation of the migration-related tasks.

.. automodule:: api.tasks.migration
   :members:

|

Notify
------
This subsection includes documentation of the tasks that notify other components of the migration procedures.

.. automodule:: api.tasks.notify
   :members:

|

Registration
------------
This subsection includes documentation of the registration tasks upon startup.

.. automodule:: api.tasks.registration
   :members:

|

VPN
---
This subsection includes documentation of the VPN-related tasks.

.. automodule:: api.tasks.vpn
   :members:

|

Essential Utils
===============

Keycloak
--------

.. automodule:: api.utils.keycloak
   :members:

|

Migration
---------

.. automodule:: api.utils.migration
   :members:

|

Miscellaneous
-------------

.. automodule:: api.utils.misc
   :members:

|


***********************
DCMC Master API Wrapper
***********************
In this section, the documentation of the DCMC Master API Wrapper is presented for reference purposes.

.. automodule:: dcmc_master_api_wrapper.dcmcm_api
   :members:

|


*********************
Communication Handler
*********************
In this section, the documentation of the API Clients that are utilized by the DCMC Master Clients is provided. The DCMCM
utilizes these clients through the Communication Handler Class. All the interactions of the DCMCM are made through this
class. The DCMCM interacts with:

  * The DCMC Server;
  * The DCMC Lite of the destination;
  * The Virtual Container Generator (VCG);
  * The Energy Aware IT Load Balancer (ITLB);
  * The IT Load Marketplace Connector (ITLMC);
  * The Cloud OpenVPN Server.

In the context of the DCMCM, the API clients include methods for performing operations as needed by the present component
only.

.. automodule:: communication_handler.communication_handler
   :members:

|

.. toctree::
   :maxdepth: 2

Cloud OpenVPN Client
====================

.. automodule:: communication_handler.api_clients.cloud_openvpn_api
   :members:

|


DCMC Server Client
==================

.. automodule:: communication_handler.api_clients.dcmcs_api
   :members:

|

DCMC Lite Client
================

.. automodule:: communication_handler.api_clients.dcmcl_api
   :members:

|

IT Load Balancer (ITLB) Client
==============================

.. automodule:: communication_handler.api_clients.itlb_api
   :members:

|

IT Load Marketplace Connector (ITLMC) Client
============================================

.. automodule:: communication_handler.api_clients.itlmc_api
   :members:

|

Virtual Container Generator (VCG) Client
========================================

.. automodule:: communication_handler.api_clients.vcg_api
   :members:

|

Keycloak API Client
===================

.. automodule:: communication_handler.api_clients.keycloak_api
   :members:

|

******************
Datacenter Handler
******************
The Datacenter Handler is the component of the DCMC Master responsible for the essential interactions with the datacenters
that take part in a migration. In the current version of the DCMC software, the Datacenter Handler class offers interfaces
that are able to:
  * get an authorization session;
  * retrieve the hypervisors of a datacenter;
  * create a new VM instance;
  * delete a VM instance;
  * live migrate a VM instance.
The above interactions are the minimal requirements for the current version of the DCMC software, in order for an inter-DC
migration (and the subsequent migration rollback) of an IT Load to take place.

The current version of the Datacenter Handler readily supports interactions with OpenStack-based datacenters. For other
types of datacenters to be supported, plugins can be developed, implementing the abstract DC handler class. Of course,
possible prerequisite operations, according to the type of datacenter should be included.

.. automodule:: datacenter_handler.datacenter_handler

|

Abstract DC Handler Class
=========================

.. automodule:: datacenter_handler.abstract_dc_handler.basehandler
   :members:

|

OpenStack Handler
=================
A collection of functions have been implemented, tailored to the needs of DCMCM application, interacting with the
necessary OpenStack components (e.g. Nova, Neutron) through the OpenStack Python clients of these services.

1. **Glance** : The OpenStack Image Service
  * Glance Docs: https://docs.openstack.org/glance/latest/
  * python-glanceclient: https://github.com/openstack/python-glanceclient
2. **Keystone** : the OpenStack Identity Service
  * Keystone Docs: https://docs.openstack.org/keystone/latest/
  * python-keystoneclient: https://github.com/openstack/python-keystoneclient
3. **Neutron** : The OpenStack Networking Service
  * Neutron Docs: https://docs.openstack.org/neutron/latest/
  * python-neutronclient: https://github.com/openstack/python-neutronclient/
4. **Nova** : Compute
  * Nova Docs: https://docs.openstack.org/nova/latest/
  * python-novaclient: https://github.com/openstack/python-novaclient

For more information about the essential OpenStack projects and their respective python clients please refer to the
provided resources.

.. automodule:: datacenter_handler.openstack.openstack
   :members:

|

********************
Notification Handler
********************

The Notification Handler is the component of the DCMC Master which subscribes to the available messaging system of
the Datacenter's installed VIM and receives notifications about a range of events, including creation, pausing, suspension,
deletion, association or disassociation of floating IPs and other events of a VM instance.

The Notification Handler component interacts with the Virtual Container Generator (VCG):
  * to register a newly created instance as a Virtual Container (VC);
  * to inform about availability changes of VCs (pauses, suspensions, power-offs);
  * to inform about a pending or confirmed migration;
  * to get essential details, such as the assigned VC Tag.
For more information and examples on the available calls of the VCG API client, please refer to the relevant section.

DB & Django Models
==================
The essential information for the notification handler component of the DCMC Master are stored in the Migration DB.
Essentially, the following tables are included for the operation of the notification handler:

  * **Instance**: A recorded VM Instance
  * **Transaction**: Details about the transaction with the block-chain
  * **Action**: Details about the actions executed on the instance

The respective Django models for the aforementioned DB tables are documented in this subsection.

.. automodule:: notification_handler.models
   :members:

|

OpenStack
=========

In this subsection the notifications for events occurring on the instances of an OpenStack installation are presented,
along with the documentation of their corresponding handlers.

Nova Notifications
------------------

The supported event types are listed in the following table.

.. list-table:: Handled notifications from the Nova Compute Service
   :widths: 35 65
   :header-rows: 1

   * - Event Type
     - Description
   * - compute.instance.create.start
     - Creating instance
   * - compute.instance.create.end
     - Instance has been created
   * - compute.instance.pause.start
     - Pausing instance
   * - compute.instance.pause.end
     - Instance has been paused
   * - compute.instance.unpause.start
     - Unpausing instance
   * - compute.instance.unpause.end
     - Instance has been unpaused
   * - compute.instance.suspend.start
     - Suspending instance
   * - compute.instance.suspend.end
     - Instance has been suspended
   * - compute.instance.resume.start
     - Resuming instance
   * - compute.instance.resume.end
     - Instance has been resumed
   * - compute.instance.reboot.start
     - Rebooting instance
   * - compute.instance.reboot.end
     - Instance has been rebooted
   * - compute.instance.power_off.start
     - Shutting off instance
   * - compute.instance.power_off.end
     - Instance has been shut off
   * - compute.instance.power_on.start
     - Powering on instance
   * - compute.instance.power_on.end
     - Instance has been powered on
   * - compute.instance.delete.start
     - Deleting instance
   * - compute.instance.delete.end
     - Instance has been deleted
   * - compute.instance.shutdown.start
     - Shutting down instance (for deletion)
   * - compute.instance.shutdown.end
     - Instance has been shut down (for deletion)
   * - compute.instance.resize.prep.start
     - Preparing to resize instance
   * - compute.instance.resize.prep.end
     - Preparation for resize has been completed
   * - compute.instance.resize.start
     - Resizing instance
   * - compute.instance.resize.end
     - Instance has been resized
   * - compute.instance.finish_resize.start
     - Finishing resize
   * - compute.instance.finish_resize.end
     - Instance resize has been finished
   * - compute.instance.resize.confirm.start
     - Confirming instance
   * - compute.instance.resize.confirm.end
     - Instance resizing has been confirmed
   * - compute.instance.resize.revert.start
     - Reverting instance resizing
   * - compute.instance.resize.revert.end
     - Instance resizing has been reverted
   * - compute.instance.live_migration.pre.start
     - Instance is being prepared for live-migration
   * - compute.instance.live_migration.pre.end
     - Instance has been prepared for live-migration
   * - compute.instance.live_migration._post.start
     - Instance is being live-migrated
   * - compute.instance.live_migration._post.end
     - Instance has been live-migrated
   * - compute.instance.live_migration.post.dest.start
     - Post live-migration procedures start
   * - compute.instance.live_migration.post.dest.end
     - Post live-migration procedures have been completed
   * - compute.instance.live_migration._rollback.end
     - Migration has been aborted and rolling bac


The documentation of the class that is responsible for handling notifications of the Nova Service follows.

.. automodule:: notification_handler.management.commands.nova_notifications
   :members:

|

Neutron Notifications
---------------------

Regarding the Neutron notifications, they are monitored in order to catch the events related to status updates of
floating IP addresses. The types of notifications that are monitored include the *sync_routers* and *update_floatingip_statuses*.

Upon the reception of an *update_floatingip_statuses* notification, the handler checks if a Floating IP has been associated or
disassociated with a recorded OpenStack instance. Upon association, the instance is registered with the VCG.

.. automodule:: notification_handler.management.commands.neutron_notifications
   :members:

|

Asynchronous Tasks
==================
.. .. automodule:: notification_handler.tasks
   :members:

.. autotask:: notification_handler.tasks.get_async_vctag

|

Random Essential Utils
======================

.. automodule:: notification_handler.utils
   :members:

|


******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
