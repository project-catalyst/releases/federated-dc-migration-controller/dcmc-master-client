import os

from celery import Celery, signals
from django.conf import settings


@signals.setup_logging.connect
def setup_celery_logging(**kwargs):
    pass


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dcmc_master_client.settings.{}'.format(os.getenv('DCMCM_ENV')))

app = Celery('dcmc_master_client')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.log.setup()
