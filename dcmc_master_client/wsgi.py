"""WSGI config for dcmc_master_client project. """

import os
import sys

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dcmc_master_client.settings.{}'.format(os.getenv('DCMCM_ENV')))
sys.path.append('/opt/dcmc_master_client')
application = get_wsgi_application()
