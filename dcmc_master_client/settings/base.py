"""Base settings for dcmc_master_client project. """

import os

# ==================================
#           PROJECT ROOT
# ==================================
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir))))

# ==================================
#      INSTALLED APPLICATIONS
# ==================================
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'api',
    'notification_handler',
    'docs',
    'drf_yasg',
]

# ==================================
#             MIDDLEWARE
# ==================================
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# ==================================
#      TEMPLATE CONFIGURATION
# ==================================
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# ==================================
#       RDBMS CONFIGURATION
# ==================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DCMCM_DB_NAME'),
        'USER': os.getenv('DCMCM_DB_USER'),
        'PASSWORD': os.getenv('DCMCM_DB_PASSWORD'),
        'HOST': os.getenv('DCMCM_DB_HOST'),
        'PORT': os.getenv('DCMCM_DB_PORT')
    }
}

# ==================================
#         BASIC SETTINGS
# ==================================
ROOT_URLCONF = 'dcmc_master_client.urls'
WSGI_APPLICATION = 'dcmc_master_client.wsgi.application'
SECRET_KEY = '8ggz^pk^p15bodvhe$0)or$5+5(!(%com1#t3iwa4s1*9)s##@'

# ==================================
#         TIMEZONE SETTINGS
# ==================================
TIME_ZONE = 'UTC'
USE_TZ = True

# ==================================
#       MULTILINGUAL SETTINGS
# ==================================
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True

# ==================================
#       STATIC FILES CONF
# ==================================
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static').replace('\\', '/')

# ==================================
# LOGGING SETTINGS
# ==================================
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] - [%(name)s:%(lineno)s] - [%(levelname)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'api': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/api.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'celery-tasks': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/celery-tasks.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'openstack': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/openstack.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'neutron_logger': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/neutron.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'nova_logger': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': str(PROJECT_ROOT) + "/logs/nova.log",
            'maxBytes': 2024 * 2024,
            'backupCount': 5,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'api_logger': {
            'handlers': ['api'],
            'level': 'DEBUG',
        },
        'celery-tasks': {
            'handlers': ['celery-tasks'],
            'level': 'DEBUG',
        },
        'openstack': {
            'handlers': ['openstack'],
            'level': 'DEBUG',
        },
        'nova_logger': {
            'handlers': ['nova_logger'],
            'level': 'INFO',
        },
        'neutron_logger': {
            'handlers': ['neutron_logger'],
            'level': 'DEBUG',
        },
    }
}

# ==================================
#          REDIS SETTINGS
# ==================================
REDIS_HOST = {
    'IP': os.getenv('DCMCM_REDIS_HOST'),
    'PORT': os.getenv('DCMCM_REDIS_PORT')
}

# =================================
#          CELERY SETTINGS
# =================================
BROKER_URL = 'redis://{IP}:{PORT}/0'.format(**REDIS_HOST)
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
CELERY_IMPORTS = ('api.tasks', 'notification_handler.tasks')

# =================================
#         SWAGGER SETTINGS
# =================================
SWAGGER_SETTINGS = {
    'enabled_methods': [
        'get',
        'post',
        'put',
        'patch',
        'delete',
    ],
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'description': "",
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        },
    },
    'JSON_EDITOR': False,
}

# =================================
#        SPHINX DOCS ROOT
# =================================
DOCS_ROOT = os.path.join(PROJECT_ROOT, 'docs/build/html')

# ==================================
#          DCMC MASTER HOST
# ==================================
DCMC_MASTER_HOST = {
    'PROTOCOL': 'http',
    'IP': os.getenv('DCMCM_HOST_IP'),
    'PORT': os.getenv('DCMCM_HOST_PORT')
}
DCMCM_HOST_URL = '{PROTOCOL}://{IP}:{PORT}/'.format(**DCMC_MASTER_HOST)

# =================================
#           DCMC SERVER
# =================================
DCMCS_HOST = {
    'IP': os.getenv('DCMCM_DCMCS_IP'),
    'PORT': os.getenv('DCMCM_DCMCS_PORT'),
    'PROTOCOL': os.getenv('DCMCM_DCMCS_PROTOCOL')
}
DCMCS_HOST_URL = '{PROTOCOL}://{IP}:{PORT}/'.format(**DCMCS_HOST)
