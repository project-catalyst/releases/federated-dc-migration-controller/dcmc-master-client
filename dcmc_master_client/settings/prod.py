"""Production settings for dcmc_master_client project."""

from .base import *

# ==================================
#             DEBUGGING
# ==================================
DEBUG = False

# ==================================
#           ALLOWED HOSTS
# ==================================
ALLOWED_HOSTS = ['*']

# ==================================
#          DCMC MASTER HOST
# ==================================
DCMC_MASTER_HOST = {
    'PROTOCOL': os.getenv('DCMCM_HOST_PROTOCOL'),
    'IP': os.getenv('DCMCM_HOST_IP'),
    'PORT': os.getenv('DCMCM_HOST_PORT')
}
