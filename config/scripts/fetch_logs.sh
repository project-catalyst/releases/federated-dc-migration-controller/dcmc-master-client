#!/usr/bin/env bash

DCMC_API='docker exec -i catalyst_dcmcm_1 tail -fn +1 /opt/dcmc_master_client/logs/api.log'
DCMC_TASKS='docker exec -i catalyst_dcmcm_1 tail -fn +1 /opt/dcmc_master_client/logs/celery-tasks.log'
DCMC_LITE_API='docker exec -i catalyst_dcmcl_1 tail -fn +1 /opt/dcmc_lite_client/logs/api.log'
DCMC_LITE_TASKS='docker exec -i catalyst_dcmcl_1 tail -fn +1 /opt/dcmc_lite_client/logs/celery-tasks.log'
DCMC_ITLMC='docker logs -f catalyst_itl_connector_api_1'
DCMC_SERVER_API='docker exec -i catalyst_dcmcs_1 tail -fn +1 /opt/dcmc-server/logs/api.log'
DCMC_SERVER_TASKS='docker exec -i catalyst_dcmcs_1 tail -fn +1 /opt/dcmc-server/logs/celery-tasks.log'

gnome-terminal -x bash -c "echo -ne '\033]0;[SOURCE] DCMC Master API Logs\007'; ssh catalyst.dc_old_laptop '${DCMC_API}'; read line" &
gnome-terminal -x bash -c "echo -ne '\033]0;[SOURCE] DCMC Master Task Logs\007'; ssh catalyst.dc_old_laptop '${DCMC_TASKS}'; read line" &
gnome-terminal -x bash -c "echo -ne '\033]0;[SOURCE] DCMC Lite API Logs\007'; ssh catalyst.dc_old_laptop '${DCMC_LITE_API}'; read line" &
gnome-terminal -x bash -c "echo -ne '\033]0;[SOURCE] DCMC Lite Task Logs\007'; ssh catalyst.dc_old_laptop '${DCMC_LITE_TASKS}'; read line" &
gnome-terminal -x bash -c "echo -ne '\033]0;[SOURCE] ITLMC Logs\007'; ssh catalyst.dc_old_laptop '${DCMC_ITLMC}'; read line" &
gnome-terminal --window-with-profile=MasterDest -x bash -c "echo -ne '\033]0;[DESTINATION] DCMC Master API Logs\007'; ssh catalyst.dc_old_ctrl '${DCMC_API}'; read line" &
gnome-terminal --window-with-profile=MasterDest -x bash -c "echo -ne '\033]0;[DESTINATION] DCMC Master Task Logs\007'; ssh catalyst.dc_old_ctrl '${DCMC_TASKS}'; read line" &
gnome-terminal --window-with-profile=MasterDest -x bash -c "echo -ne '\033]0;[DESTINATION] ITLMC Logs\007'; ssh catalyst.dc_old_ctrl '${DCMC_ITLMC}'; read line" &
gnome-terminal --window-with-profile=MasterDest -x bash -c "echo -ne '\033]0;[DESTINATION] DCMC Lite API Logs\007'; ssh catalyst.dc_old_ctrl '${DCMC_LITE_API}'; read line" &
gnome-terminal --window-with-profile=MasterDest -x bash -c "echo -ne '\033]0;[DESTINATION] DCMC Lite Task Logs\007'; ssh catalyst.dc_old_ctrl '${DCMC_LITE_TASKS}'; read line" &
gnome-terminal --window-with-profile=DCMCServer -x bash -c "echo -ne '\033]0;DCMC Server API Logs\007'; ssh catalyst.dcmc-server '${DCMC_SERVER_API}'; read line" &
gnome-terminal --window-with-profile=DCMCServer -x bash -c "echo -ne '\033]0;DCMC Server Task Logs\007'; ssh catalyst.dcmc-server '${DCMC_SERVER_TASKS}'; read line" &
