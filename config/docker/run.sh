#!/usr/bin/env bash

# Wait for postgres
while ! nc -z dcmcm_postgres 5432; do
  echo "Waiting for postgres server..."
  sleep 1
done

# Make migrations
cd /opt/dcmc_master_client
python3 manage.py migrate --settings=dcmc_master_client.settings.${1}

# Start supervisor
supervisord -c /etc/supervisor/supervisord.conf
update-rc.d supervisor defaults

# Start gunicorn
gunicorn dcmc_master_client.wsgi:application

echo "Initialization completed."
tail -f /dev/null  # Necessary in order for the container to not stop
