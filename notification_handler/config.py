import os

# ==================================
#          DATACENTER NAME
# ==================================
DC_NAME = os.getenv('DCMCM_OS_DC_NAME')

# ==================================
#         OS TRANSPORT URL
# ==================================
OS_TRANSPORT = {
    'USER': os.getenv('DCMCM_OS_TRANSPORT_USER'),
    'PASSWORD': os.getenv('DCMCM_OS_TRANSPORT_PASSWORD'),
    'IP': os.getenv('DCMCM_OS_TRANSPORT_IP'),
    'PORT': os.getenv('DCMCM_OS_TRANSPORT_PORT')
}
BROKER_URI = 'amqp://{USER}:{PASSWORD}@{IP}:{PORT}//'.format(**OS_TRANSPORT)

# ==================================
#      NOVA EXCHANGE SETTINGS
# ==================================
NOVA_EXCHANGE_NAME = 'nova'
NOVA_ROUTING_KEY = 'notifications.info'
NOVA_QUEUE_NAME = 'nova_dump_queue'

# ==================================
#     NEUTRON EXCHANGE SETTINGS
# ==================================
NEUTRON_EXCHANGE_NAME = 'neutron'
NEUTRON_ROUTING_KEY = 'q-l3-plugin'
NEUTRON_QUEUE_NAME = 'neutron_not_queue'
NEUTRON_METHOD_UPDATE = 'update_floatingip_statuses'
