import json
import logging

from django.core.management import BaseCommand
from kombu import BrokerConnection, Exchange, Queue
from kombu.mixins import ConsumerMixin

from communication_handler.communication_handler import get_communication_handler
from notification_handler import constants, config
from notification_handler.models import Instance, Transaction
from notification_handler.utils import get_or_create_action, update_action, create_instance_and_action

logger = logging.getLogger(constants.NOVA_LOGGER)


class NovaNotifications(ConsumerMixin):
    """Nova Notifications Handler Class.

    The purpose of this class is to receive and handle OpenStack Nova notifications
    concerning the creation, pause, suspension, reboot, shut off, live migration,
    resizing and deletion of an OpenStack VM instance.

    Attributes
    ----------
    connection : BrokerConnection
        An instance of BrokerConnection class
    vcg : VCG
        An instance of the VCG API wrapper class

    Methods
    -------
    create_instance(message)
        Handles the message concerning the instance creation
    instance_availability_change(message)
        Handles the message concerning availability changes
    live_migrate_instance(message)
        Handles messages related to live migration

    """

    def __init__(self, connection):
        """Nova Notifications Class Constructor.

        Parameters
        ----------
        connection : BrokerConnection
            An instance of the BrokerConnection Class

        """
        self.connection = connection
        self.vcg = get_communication_handler().vcg

    def get_consumers(self, consumer, channel):
        exchange = Exchange(config.NOVA_EXCHANGE_NAME, type='topic', durable=False)
        queue = Queue(config.NOVA_QUEUE_NAME, exchange, routing_key=config.NOVA_ROUTING_KEY,
                      durable=False, auto_delete=True, no_ack=True)
        return [consumer(queue, callbacks=[self.on_message])]

    @staticmethod
    def create_instance(message):
        """Handles the notifications related to creation of VMs.

        Parameters
        ----------
        message : dict
            A message received as a Nova notification

        """
        event = message['event_type']
        state = message['payload']['state']
        instance_uuid = message['payload']['instance_id']
        if event == constants.VM_CREATION_START:
            create_instance_and_action(message)
        else:
            action = get_or_create_action(instance_uuid)
            if action is not None:
                update_action(action=action, previous_event=action.state, state=event)
                instance = Instance.objects.filter(uuid=instance_uuid)
                instance.update(state=state)
            else:
                create_instance_and_action(message)
        logger.info('[CREATE][{}] {}'.format(instance_uuid, constants.MESSAGE_MAPPINGS[event]))

    def instance_availability_change(self, message):
        """Handles availability changes of an Instance except for creation and live migration.

        Parameters
        ----------
        message : dict
            A message received as a Nova notification

        """
        # Get data from received message
        event = message['event_type']
        state = message['payload']['state']
        instance_uuid = message['payload']['instance_id']
        # Get action
        action = get_or_create_action(instance_uuid)
        if event not in constants.AVAILABILITY_END_EVENTS:
            if action is not None:
                update_action(action=action, previous_event=action.state, state=event)
        else:
            instance = Instance.objects.filter(uuid=instance_uuid)
            transaction = Transaction.objects.filter(instance__uuid=instance_uuid)
            if instance.exists() and transaction.exists():
                update_action(action=action, previous_event=event, state=state)
                instance.update(state=state)
                current_status = True if event in constants.AVAILABILITY_CHANGE_TRUE else False
                self.vcg.container_availability(instance[0].dc_name, transaction[0].vc_tag, status=current_status)
        logger.info('[AVAILABILITY_CHANGE][{}] {}'.format(instance_uuid, constants.MESSAGE_MAPPINGS[event]))

    @staticmethod
    def live_migrate_instance(message):
        """Handle live migration of instance.

        Parameters
        ----------
        message : dict
            A message received as a Nova notification

        """
        # TODO: Send proper messages to VCG
        # Get necessary data
        event = message['event_type']
        host = message['publisher_id']
        instance_uuid = message['payload']['instance_id']
        # Get or create action and instance
        action = get_or_create_action(instance_uuid)
        instance = Instance.objects.filter(uuid=instance_uuid)
        if event != constants.LIVE_MIGRATION_POST_DEST_END:
            if action is not None and instance.exists():
                instance.update(state='migrating')
                update_action(action=action, previous_event=action.state, state=event)
        else:
            if action is not None and instance.exists():
                update_action(action=action, previous_event=event)
                instance.update(state='active')
        logger.info('[LIVE_MIGRATE][{}] {}.'.format(instance_uuid, constants.MESSAGE_MAPPINGS[event]))

    def on_message(self, body, message):
        """Receives message from Nova and calls appropriate handler.

        Parameters
        ----------
        body : dict
            The body of the Nova message

        """
        try:
            message = json.loads(body['oslo.message'])
            event_type = message['event_type']
            if event_type in constants.VM_CREATION_EVENTS:
                self.create_instance(message)
            elif event_type in constants.AVAILABILITY_CHANGE_EVENTS:
                self.instance_availability_change(message)
            elif event_type in constants.LIVE_MIGRATION_EVENTS:
                self.live_migrate_instance(message)
            logger.debug('Event Type: {}'.format(event_type))
            logger.debug('Body: {}'.format(body))
            logger.debug('---------------')
        except (KeyError, json.JSONDecodeError) as e:
            logger.error('An error occurred upon message reception: `{}`'.format(str(e)))


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.info('Connecting to broker {}'.format(config.BROKER_URI))
        with BrokerConnection(config.BROKER_URI, connect_timeout=45.0) as connection:
            NovaNotifications(connection).run()
