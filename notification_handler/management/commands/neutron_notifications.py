import json
import logging

from django.core.management import BaseCommand
from kombu import BrokerConnection, Exchange, Queue
from kombu.mixins import ConsumerMixin
from neutronclient.v2_0 import client
from rest_framework import status

from communication_handler.communication_handler import get_communication_handler
from datacenter_handler.datacenter_handler import get_dc_handler
from notification_handler import config, constants
from notification_handler.models import Instance, Transaction
from notification_handler.tasks import get_async_vctag

logger = logging.getLogger(constants.NEUTRON_LOGGER)


class NotificationsNeutron(ConsumerMixin):
    """Neutron Notification Handler Class."""

    def __init__(self, connection):

        self.connection = connection
        self.vcg = get_communication_handler().vcg
        self.session = get_dc_handler().session

    def get_consumers(self, consumer, channel):
        exchange = Exchange(config.NEUTRON_EXCHANGE_NAME, type='topic', durable=False)
        queue = Queue(config.NEUTRON_QUEUE_NAME, exchange, routing_key=config.NEUTRON_ROUTING_KEY, durable=False,
                      auto_delete=True, no_ack=True)
        return [consumer(queue, callbacks=[self.on_message])]

    def submit_instance_to_block(self, instance):
        """Perform a transaction to the VCG when a Floating IP is associated to the instance.

        Parameters
        ----------
        instance : Instance
            An instance object to register to the VCG

        """
        logger.info('Registering container to VCG')
        response = self.vcg.register_container(dc=config.DC_NAME, vcpu=instance.vcpu, vram=instance.vram,
                                               vdisk=instance.vdisk, uuid=instance.uuid,
                                               ip='http://{}'.format(instance.ip_address), available=True,
                                               creation_date=instance.creation_date.strftime('%Y-%m-%dT%H:%M:%SZ'))
        if response.status_code == status.HTTP_200_OK:
            logger.info('OK. Registration to VCG was successful.')
            response = response.json()
            tx_hash = response.get('tx_hash', None)
            if tx_hash is not None:
                Transaction.objects.create(instance=instance, tx_hash=tx_hash)
                get_async_vctag.delay(tx_hash)
            else:
                logger.error('No transaction hash (tx_hash) was found in the response payload.')
                logger.debug('Response payload: `{}`'.format(response.json()))
        else:
            logger.error('ERROR. Registration to VCG failed with response code [{}]'.format(response.status_code))
            logger.debug('Response payload: `{}`'.format(response.json()))

    def get_info_from_floatingip_id(self, fip_uuid):
        """Fetch information for Floating IP UUID.

        Get the VM UUID and Floating IP address from Floating IP ID if the
        object has status = ACTIVE, else if status = DOWN, the Floating ip
        is disassociating from the VM.

        Parameters
        ----------
        fip_uuid : str
            The UUID of the Floating IP to fetch details for

        Returns
        -------
        fip_details : dict, None
            A dict including the keys if status is ACTIVE and None if status is DOWN

        """
        neutron = client.Client(session=self.session)
        floating_ip = neutron.show_floatingip(fip_uuid)
        floating_ip_status = floating_ip['floatingip']['status']
        fip_details = None
        if floating_ip_status == 'ACTIVE':
            vm_uuid = neutron.show_port(floating_ip['floatingip']['port_id'])['port']['device_id']
            vm_floating_ip = floating_ip['floatingip']['floating_ip_address']
            fip_details = {'uuid': vm_uuid, 'ip': vm_floating_ip}
        return fip_details

    def handle_fip_obj(self, fip_object):
        """Handler Floating IP Object.

        Check if the Floating IP object is ACTIVE or DOWN. If the Floating IP object is used
        (ACTIVE), then we find the VM UUID and Floating IP Address by calling the FIPManager class.

        Parameters
        ----------
        fip_object : dict
            A floating IP dictionary (e.g {'13a40908-4cdb-4135-aef6-e69502e0fd0c': 'ACTIVE'})

        """
        self.session = get_dc_handler().session
        fip_id = list(fip_object.keys())[0]
        fip_status = list(fip_object.values())[0]
        if fip_status == 'ACTIVE':
            fip_details = self.get_info_from_floatingip_id(fip_id)
            if fip_details is not None:
                instance = Instance.objects.filter(uuid=fip_details['uuid'])
                if instance.exists():
                    instance.update(ip_address=fip_details['ip'], floating_ip_id=fip_id)
                    self.submit_instance_to_block(instance[0])
        else:
            instance = Instance.objects.filter(floating_ip_id=fip_id)
            if instance.exists():
                instance.update(floating_ip_id=None, ip_address=None)

    def on_message(self, body, message):
        try:
            message_body = json.loads(body['oslo.message'])
            if message_body['method'] == config.NEUTRON_METHOD_UPDATE:
                data = message_body['args']['fip_statuses']
                self.handle_fip_obj(data)
                logger.info('Received message for Floating IP update')
            logger.debug('Body: %r' % body['oslo.message'])
            logger.debug('---------------')
        except (KeyError, json.JSONDecodeError) as e:
            logger.error('A error occurred while processing Neutron message: `{}`'.format(str(e)))


class Command(BaseCommand):
    def handle(self, *args, **options):
        logger.info('Connecting to broker {}'.format(config.BROKER_URI))
        with BrokerConnection(config.BROKER_URI, connect_timeout=5.0) as connection:
            NotificationsNeutron(connection).run()
