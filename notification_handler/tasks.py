import logging
import time

from rest_framework import status

from communication_handler.communication_handler import get_communication_handler
from dcmc_master_client.celery import app
from notification_handler import constants
from notification_handler.models import Transaction

logger = logging.getLogger(constants.NEUTRON_LOGGER)


@app.task
def get_async_vctag(tx_hash):
    """Fetch VC Tag from transaction hash.

    Parameters
    ----------
    tx_hash : str
        The transaction hash generated through the VCG

    """
    task_tag = 'task:get_async_vctag'
    retries = 0
    logger.info('[{}] Getting VC Tag asynchronously.'.format(task_tag))
    while retries < 1000:
        time.sleep(2)
        response = get_communication_handler().vcg.get_transaction_vctag(tx_hash)
        if response.status_code == status.HTTP_200_OK:
            logger.info('[{}] OK. Request for VC tag was successful.'.format(task_tag))
            vc_tag = response.json()
            transaction = Transaction.objects.filter(tx_hash=tx_hash)
            transaction.update(vc_tag=vc_tag['vc_tag'],
                               block_number=vc_tag['block_number'],
                               block_hash=vc_tag['block_hash'])
            break
        else:
            logger.warning('[{}] Request failed with response code [{}]. Retrying...'
                           .format(task_tag, response.status_code))
            logger.debug('[{}] Response payload: `{}`'.format(task_tag, response.json()))
        retries += 1
