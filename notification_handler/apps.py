from django.apps import AppConfig


class NotificationHandlerConfig(AppConfig):
    name = 'notification_handler'
