# ==================================
#            VM CREATION
# ==================================
VM_CREATION_START = 'compute.instance.create.start'
VM_CREATION_END = 'compute.instance.create.end'
VM_CREATION_EVENTS = [VM_CREATION_START, VM_CREATION_END]

# ==================================
#          PAUSE INSTANCE
# ==================================
PAUSED_INSTANCE_START = 'compute.instance.pause.start'
PAUSED_INSTANCE_END = 'compute.instance.pause.end'
PAUSE_EVENTS = [PAUSED_INSTANCE_START, PAUSED_INSTANCE_END]

# ==================================
#          UNPAUSE INSTANCE
# ==================================
UNPAUSE_INSTANCE_START = 'compute.instance.unpause.start'
UNPAUSE_INSTANCE_END = 'compute.instance.unpause.end'
UNPAUSE_EVENTS = [UNPAUSE_INSTANCE_START, UNPAUSE_INSTANCE_END]

# ==================================
#          SUSPEND INSTANCE
# ==================================
SUSPENDED_INSTANCE_START = 'compute.instance.suspend.start'
SUSPENDED_INSTANCE_END = 'compute.instance.suspend.end'
SUSPENSION_EVENTS = [SUSPENDED_INSTANCE_START, SUSPENDED_INSTANCE_END]

# ==================================
#          RESUME INSTANCE
# ==================================
RESUME_INSTANCE_START = 'compute.instance.resume.start'
RESUME_INSTANCE_END = 'compute.instance.resume.end'
RESUME_EVENTS = [RESUME_INSTANCE_START, RESUME_INSTANCE_END]

# ==================================
#        REBOOT INSTANCE
# ==================================
REBOOT_INSTANCE_START = 'compute.instance.reboot.start'
REBOOT_INSTANCE_END = 'compute.instance.reboot.end'
REBOOT_EVENTS = [REBOOT_INSTANCE_START, REBOOT_INSTANCE_END]

# ==================================
#        SHUT-OFF INSTANCE
# ==================================
SHUTOFF_INSTANCE_START = 'compute.instance.power_off.start'
SHUTOFF_INSTANCE_END = 'compute.instance.power_off.end'
SHUT_OFF_EVENTS = [SHUTOFF_INSTANCE_START, SHUTOFF_INSTANCE_END]

# ==================================
#        POWER-ON INSTANCE
# ==================================
POWER_ON_INSTANCE_START = 'compute.instance.power_on.start'
POWER_ON_INSTANCE_END = 'compute.instance.power_on.end'
POWER_ON_EVENTS = [POWER_ON_INSTANCE_START, POWER_ON_INSTANCE_END]

# ==================================
#         DELETE INSTANCE
# ==================================
DELETE_START = 'compute.instance.delete.start'
SHUTDOWN_START = 'compute.instance.shutdown.start'
SHUTDOWN_END = 'compute.instance.shutdown.end'
DELETE_END = 'compute.instance.delete.end'
DELETE_EVENTS = [DELETE_START, SHUTDOWN_START, SHUTDOWN_END, DELETE_END, DELETE_END]

# ==================================
#      AVAILABILITY EVENTS
# ==================================
AVAILABILITY_CHANGE_EVENTS = [*PAUSE_EVENTS, *UNPAUSE_EVENTS, *SUSPENSION_EVENTS, *RESUME_EVENTS,
                              *SHUT_OFF_EVENTS, *POWER_ON_EVENTS, *DELETE_EVENTS, *REBOOT_EVENTS]
AVAILABILITY_END_EVENTS = [PAUSED_INSTANCE_END, UNPAUSE_INSTANCE_END, SUSPENDED_INSTANCE_END, RESUME_INSTANCE_END,
                           SHUTOFF_INSTANCE_END, POWER_ON_INSTANCE_END, SHUTDOWN_END, DELETE_END, REBOOT_INSTANCE_START]
AVAILABILITY_CHANGE_TRUE = [UNPAUSE_INSTANCE_END, RESUME_INSTANCE_END, POWER_ON_INSTANCE_END, REBOOT_INSTANCE_END]

# ==================================
#       LIVE MIGRATE INSTANCE
# ==================================
LIVE_MIGRATION_PRE_START = 'compute.instance.live_migration.pre.start'
LIVE_MIGRATION_PRE_END = 'compute.instance.live_migration.pre.end'
LIVE_MIGRATION_POST_START = 'compute.instance.live_migration._post.start'
LIVE_MIGRATION_POST_END = 'compute.instance.live_migration._post.end'
LIVE_MIGRATION_POST_DEST_START = 'compute.instance.live_migration.post.dest.start'
LIVE_MIGRATION_POST_DEST_END = 'compute.instance.live_migration.post.dest.end'
MIGRATION_ROLLBACK = 'compute.instance.live_migration._rollback.end'
LIVE_MIGRATION_EVENTS = [LIVE_MIGRATION_PRE_START, LIVE_MIGRATION_PRE_END, LIVE_MIGRATION_POST_START,
                         LIVE_MIGRATION_POST_END, LIVE_MIGRATION_POST_DEST_START, LIVE_MIGRATION_POST_DEST_END,
                         MIGRATION_ROLLBACK]

# ==================================
#       NOTIFICATION MAPPINGS
# ==================================
NOTIFICATIONS = {
    ('building', None): VM_CREATION_START,
    ('active', 'spawning'): VM_CREATION_END,
    ('active', 'pausing'): PAUSED_INSTANCE_START,
    ('paused', 'pausing'): PAUSED_INSTANCE_END,
    ('paused', 'unpausing'): UNPAUSE_INSTANCE_START,
    ('active', 'unpausing'): UNPAUSE_INSTANCE_END,
    ('active', 'suspending'): SUSPENDED_INSTANCE_START,
    ('suspended', 'suspending'): SUSPENDED_INSTANCE_END,
    ('suspended', 'resuming'): RESUME_INSTANCE_START,
    ('active', 'resuming'): RESUME_INSTANCE_END,
    ('active', 'powering-off'): SHUTOFF_INSTANCE_START,
    ('stopped', 'powering-off'): SHUTOFF_INSTANCE_END,
    ('stopped', 'powering-on'): POWER_ON_INSTANCE_START,
    ('active', 'powering-on'): POWER_ON_INSTANCE_END,
    ('active', 'deleting'): DELETE_START,
    ('deleted', 'deleting'): DELETE_END,
    ('active', 'migrating'): LIVE_MIGRATION_PRE_START,
}

# ==================================
#     LOGGING MESSAGE MAPPINGS
# ==================================
MESSAGE_MAPPINGS = {
    VM_CREATION_START: 'Creating instance ...',
    VM_CREATION_END: 'Instance has been created.',
    PAUSED_INSTANCE_START: 'Pausing instance ...',
    PAUSED_INSTANCE_END: 'Instance is has been paused.',
    UNPAUSE_INSTANCE_START: 'Unpausing instance ...',
    UNPAUSE_INSTANCE_END: 'Instance has been unpaused.',
    SUSPENDED_INSTANCE_START: 'Suspending instance ...',
    SUSPENDED_INSTANCE_END: 'Instance has been suspended.',
    RESUME_INSTANCE_START: 'Resuming instance ...',
    RESUME_INSTANCE_END: 'Instance has been resumed.',
    REBOOT_INSTANCE_START: 'Rebooting instance ...',
    REBOOT_INSTANCE_END: 'Instance has been rebooted.',
    SHUTOFF_INSTANCE_START: 'Shutting off instance ...',
    SHUTOFF_INSTANCE_END: 'Instance has been shut off.',
    POWER_ON_INSTANCE_START: 'Powering on instance ...',
    POWER_ON_INSTANCE_END: 'Instance has been powered on.',
    DELETE_START: 'Deleting instance ...',
    SHUTDOWN_START: 'Shutting down instance ...',
    SHUTDOWN_END: 'Instance has been shut down.',
    DELETE_END: 'Instance has been deleted.',
    LIVE_MIGRATION_PRE_START: 'Preparing instance for live migration ...',
    LIVE_MIGRATION_PRE_END: 'Instance prepared for live migration.',
    LIVE_MIGRATION_POST_DEST_START: 'Live migrating instance ...',
    LIVE_MIGRATION_POST_DEST_END: 'Instance has been live migrated.',
    LIVE_MIGRATION_POST_START: 'Post live migration on instance ...',
    LIVE_MIGRATION_POST_END: 'Post live migration has been completed.',
    MIGRATION_ROLLBACK: 'Live migration did not succeed. Rolled back.'
}

# ==================================
#              LOGGERS
# ==================================
NOVA_LOGGER = 'nova_logger'
NEUTRON_LOGGER = 'neutron_logger'
