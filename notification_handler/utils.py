import dateutil.parser

from notification_handler import config
from notification_handler.models import Action, Instance


def get_or_create_action(uuid):
    """Get or create Action object.

    Parameters
    ----------
    uuid : str
        The OpenStack UUID of the instance to fetch Action for.

    Returns
    -------
    action : Action
        The created or fetched Action object

    """
    try:
        action = Action.objects.get(instance__uuid=uuid)
        return action
    except Action.DoesNotExist:
        try:
            instance = Instance.objects.get(uuid=uuid)
            action = Action.objects.create(instance=instance)
            return action
        except Instance.DoesNotExist:
            return None


def update_action(action, previous_event=None, state=None):
    """ Update Action object with previous and current state.

    Parameters
    ----------
    action : Action
        The Action object to update
    previous_event : str
        The previous Action state
    state : str
        The current Action state

    """
    action.previous_event = previous_event
    action.state = state
    action.save()


def create_instance_and_action(message):
    """Create Instance and Action objects.

    Parameters
    ----------
    message : dict
        The received Nova message

    """
    data = message['payload']
    state = data['state']
    event = message['event_type']
    instance_uuid = data['instance_id']
    creation_date = dateutil.parser.parse(message['timestamp']).strftime('%Y-%m-%d %H:%M:%SZ')
    Instance.objects.create(creation_date=creation_date,
                            vcpu=data['vcpus'], vdisk=data['disk_gb'], vram=data['memory_mb'],
                            uuid=instance_uuid, host=message['publisher_id'], state=state,
                            dc_name=config.DC_NAME, project_name=message['_context_project_name'])
    action = get_or_create_action(instance_uuid)
    update_action(action=action, state=event)


def disassociate_floating_ip(uuid):
    """Disassociate IP Address and Floating IP ID from Instance.

    Parameters
    ----------
    uuid : str
        The OpenStack UUID of the instance

    """
    instance = Instance.objects.filter(uuid=uuid)
    if instance.exists():
        instance.update(ip_address=None, floating_ip_id=None)
