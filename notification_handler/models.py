from django.db import models


class Instance(models.Model):
    """Instance Model Class.

    Attributes
    ----------
    creation_date : DateTimeField
        The datetime of the instance's creation
    vcpu : IntegerField
        The number of vCPUs of the instance
    vram : IntegerField
        The amount of the instance's RAM in MB
    vdisk : IntegerField
        The amount of the instance's disk in GB
    uuid : CharField
        The OpenStack UUID of the instance
    host : CharField
        The OpenStack compute node that hosts the instance
    state : CharField
        The current state of the instance
    ip_address : CharField
        The Floating IP Address of the instance
    project_name : CharField
        The project under which the instance is created
    floating_ip_id : CharField
        The UUID of the instance's Floating IP
    dc_name : CharField
        The name of the Datacenter where this instance is deployed

    """
    creation_date = models.DateTimeField(help_text='The datetime of the instance\'s creation')
    vcpu = models.IntegerField(help_text='The number of vCPUs of the instance')
    vram = models.IntegerField(help_text='The amount of the instance\'s RAM in MB')
    vdisk = models.IntegerField(help_text='The amount of the instance\'s disk in GB')
    uuid = models.CharField(max_length=100, help_text='The OpenStack UUID of the instance')
    host = models.CharField(max_length=30, help_text='The OpenStack compute node that hosts the instance')
    state = models.CharField(max_length=15, help_text='The current state of the instance')
    ip_address = models.CharField(max_length=30, help_text='The Floating IP Address of the instance', null=True)
    project_name = models.CharField(max_length=30, help_text='The project under which the instance is created')
    floating_ip_id = models.CharField(max_length=100, help_text='The UUID of the instance\'s Floating IP', null=True)
    dc_name = models.CharField(max_length=30, help_text='The name of the Datacenter where this instance is deployed')


class Transaction(models.Model):
    """Transaction Model Class.

    Attributes
    ----------
    instance :  ForeignKey to Instance
        The Instance Object of the Transaction
    tx_hash : CharField
        The transaction hash
    vc_tag : CharField
        The Virtual Container Tag
    block_number : IntegerField
        The Block-chain's Block Number
    block_hash : CharField
        The Block-chain's Block Hash

    """
    instance = models.ForeignKey(Instance, help_text='The Instance Object of the Transaction', on_delete=models.CASCADE)
    tx_hash = models.CharField(max_length=255, help_text='The Transaction Hash', null=True)
    vc_tag = models.CharField(max_length=255, help_text='The Virtual Container Tag', null=True)
    block_number = models.IntegerField(help_text='The Block-chain\'s Block Number', null=True)
    block_hash = models.CharField(max_length=255, help_text='The Block-chain\'s BlockHash', null=True)


class Action(models.Model):
    """Action Model Class.

    Attributes
    ----------
    instance :  ForeignKey to Instance
        The Instance Object of the Action
    state : CharField
        The current state of the instance
    previous_event : CharField
        The previous event that occurred to the instance

    """
    instance = models.ForeignKey(Instance, help_text='VM instance', on_delete=models.CASCADE)
    state = models.CharField(max_length=255, help_text='Operation under Execution', null=True)
    previous_event = models.CharField(max_length=255, help_text='Previous Operation', null=True)
