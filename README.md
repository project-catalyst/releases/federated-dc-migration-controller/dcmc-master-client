# DC Migration Controller Master Client (DCMC Master)

This is the repository for the DCMC Master, implemented for the Migration Controller of the
H2020 CATALYST Project. The DCMC Master resides in every DC of the CATALYST federation and performs
preparatory tasks for the actual migration. Normally, this component is deployed in the Controller
of the OpenStack installation, but could run in other nodes as well.

## Installation Guide

In the following, the essentials for the installation of a DCMC Master instance are described, including
prerequisites for the deployment, the configuration of the services and the actual deployment.

### Prerequisites

For the deployment of the DCMC Server component the Docker engine as well as docker-compose should be installed.
These actions can be performed following the instructions provided below. Firstly, an update should be performed
and essential packages should be installed:

```bash
sudo apt-get update
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     software-properties-common
```

Secondly the key and Docker repository should be added:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
```

Then another update is performed, Docker is installed and the user is added to docker group.

```bash
sudo apt-get update
sudo apt-get install -y docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER

```

Finally, docker-compose should be installed:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Environmental Parameters

The services that are executed as containers, thus forming the DCMC Master receive their configurations
from a .env file located in the root of the repository. In the following tables, the environmental parameters
that are necessary for the configuration and deployment of the DCMC Master are recorded and described.

__Docker and Internal Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| COMPOSE_PROJECT_NAME | `docker` | The name of the project (default: catalyst) |
| PG_IMAGE_TAG | `PostgreSQL` | Docker Image Tag |
| PG_PORT | `PostgreSQL` | Port |
| PG_USER | `PostgreSQL` | User |
| PG_PASSWORD | `PostgreSQL` | DB Password |
| PG_DB | `PostgreSQL` | DB Name |
| REDIS_IMAGE_TAG | `Redis` | Docker Image Tag |
| REDIS_PORT | `Redis` | Port |
| DCMCM_ENV | `DCMC Master` | Environment (dev / prod) |
| DCMCM_API_PORT | `DCMC Master` | API Port |
| DCMCM_SUPERVISOR_PORT | `DCMC Master` | Supervisor Port |
| DCMCM_DB_HOST | `DCMC Master` | DB Host |
| DCMCM_DB_PORT | `DCMC Master` | DB Port |
| DCMCM_DB_USER | `DCMC Master` | DB User |
| DCMCM_DB_PASSWORD | `DCMC Master` | DB Password |
| DCMCM_DB_NAME | `DCMC Master` | DB Name |
| DCMCM_REDIS_HOST | `DCMC Master` | Redis Host |
| DCMCM_REDIS_PORT | `DCMC Master` | Redis Port |
| DCMCM_HOST_IP | `DCMC Master` | Host IP |
| DCMCM_HOST_PORT | `DCMC Master` | Host Port |
| DCMCM_HOST_PROTOCOL | `DCMC Master` | Host Protocol |
| DCMCM_DC_TYPE=openstack | `DCMC Master` | DC Type |


__OpenStack Services & Credentials__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| DCMCM_OS_DC_NAME | `OpenStack` | DC Name  |
| DCMCM_OS_AUTH_URL | `OpenStack` | Authorization |
| DCMCM_OS_CONTROLLER | `OpenStack` | Controller IP |
| DCMCM_OS_CONTROLLER_USER | `OpenStack` | Controller's Host Machine User |
| DCMCM_OS_CONTROLLER_PWD | `OpenStack` | Controller's Host Machine Pwd |
| DCMCM_OS_USERNAME | `OpenStack` | Username |
| DCMCM_OS_USER_PWD | `OpenStack` | Password |
| DCMCM_OS_PROJECT_NAME | `OpenStack` | Project Name |
| DCMCM_OS_PROJECT_DOMAIN_ID | `OpenStack` | Project domain (default: default) |
| DCMCM_OS_USER_DOMAIN_ID | `OpenStack` | User Domain (default: default) |
| DCMCM_OS_TRANSPORT_USER | `RabbitMQ` | Transport User |
| DCMCM_OS_TRANSPORT_PASSWORD | `RabbitMQ` | Transport Password |
| DCMCM_OS_TRANSPORT_IP | `RabbitMQ` | Transport IP |
| DCMCM_OS_TRANSPORT_PORT | `RabbitMQ` | Transport Port |
| DCMCM_OS_AUTHENTICATE_URI | `Keystone` | Service Endpoint |
| DCMCM_OS_AUTHORIZATION_URL | `Keystone` | Service Endpoint |
| DCMCM_OS_GLANCE_SERVICE | `Glance` | Service Endpoint |
| DCMCM_OS_MEMCACHED_SERVICE | `Memcached` | Service Host |
| DCMCM_OS_MEMCACHED_SECRET_KEY | `Memcached` | Secret Key |
| DCMCM_OS_NEUTRON_SERVICE | `Neutron` | Service Endpoint |
| DCMCM_OS_NEUTRON_PASS | `Neutron` | Password |
| DCMCM_OS_NOVA_PASS | `Nova` | Password |
| DCMCM_OS_PLACEMENT_SERVICE | `Placement` | Service Endpoint |
| DCMCM_OS_PLACEMENT_PASS | `Placement` | Password |
| DCMCM_OS_TRANSPORT_URL | `RabbitMQ` | Transport URL |
| DCMCM_OS_NFS_SERVER_HOST | `NFS` | Host IP |
| DCMCM_OS_NFS_SERVER_ROOT | `NFS` | Exposed Directory |
| DCMCM_OS_NOVA_UID | `Nova` | UID (nova) |
| DCMCM_OS_NOVA_GID | `Nova` | GID (nova) |
| DCMCM_OS_LIBVIRT_UID | `Libvirt` `QEMU` | UID (libvirt-qemu) |
| DCMCM_OS_LIBVIRT_GID | `Libvirt` `QEMU` | GID (kvm) |
| DCMCM_OS_LIBVIRT_GROUP | `Libvirt` | GID (libvirt) |
| DCMCM_OS_LIBVIRT_USER | `Libvirt` | User for Libvirt |
| DCMCM_OS_VCMP_IMAGE | `vCMP` | Image Name (default: vCompute-Train-v0.9.8-SNAPSHOT) |
| DCMCM_OS_VCMP_PROJECT | `vCMP` | Project Name (default: Catalyst)|
| DCMCM_OS_VCMP_KEYPAIR | `vCMP` | Keypair (default: none) |
| DCMCM_OS_VCMP_MIN_CPU | `vCMP` | Minimum numbers or CPUs (default: 1) |
| DCMCM_OS_VCMP_MIN_RAM | `vCMP` | Minimum amount of RAM (MB) (default: 2048) |
| DCMCM_OS_VCMP_MIN_DISK | `vCMP` | Minimum amount of disk (GB) (default: 20) |


__External Services__

| Environment Variable | Tag | Description |
| -------------------- | --- | ----------- |
| DCMCM_VCG_IP | `VCG` | Host IP |
| DCMCM_VCG_PORT | `VCG` | Port |
| DCMCM_VCG_PROTOCOL | `VCG` | Protocol |
| DCMCM_ITLB_IP | `IT Load Balancer` | Host IP |
| DCMCM_ITLB_PORT | `IT Load Balancer` | Port |
| DCMCM_ITLB_PROTOCOL | `IT Load Balancer` | Protocol |
| DCMCM_ITLMC_IP | `IT Load Markerplace Connector` | Host IP |
| DCMCM_ITLMC_PORT | `IT Load Markerplace Connector` | Port |
| DCMCM_ITLMC_PROTOCOL | `IT Load Markerplace Connector` | Protocol |
| DCMCM_DCMCS_IP | `DCMC Server` | Host IP |
| DCMCM_DCMCS_PORT | `DCMC Server` | Port |
| DCMCM_DCMCS_PROTOCOL | `DCMC Server` | Protocol |
| DCMCM_KEYCLOAK_URL | `Keycloak` | Host URL |
| DCMCM_KEYCLOAK_REALM | `Keycloak` | Realm |
| DCMCM_KEYCLOAK_CLIENT_ID | `Keycloak` | Client ID |
| DCMCM_KEYCLOAK_CLIENT_SECRET | `Keycloak` | Client Secret |
| DCMCM_KEYCLOAK_USERNAME | `Keycloak` | Username |
| DCMCM_KEYCLOAK_PASSWORD | `Keycloak` | Password |
| DCMCM_COVPN_HOST_URL | `Cloud OpenVPN` | Host URL |
| DCMCM_COVPN_TENANT | `Cloud OpenVPN` | Tenant |
| DCMCM_COVPN_PORT | `Cloud OpenVPN` | Connection Port |
| DCMCM_DCMCL_DEFAULT_PORT | `DCMC Lite` | Default Port (default: 60004) |


__Temporary Settings__

| Environment Variable | Description |
| -------------------- | ----------- |
| DCMCM_VC_UUID | UUID of the VM for migration testing. Temporarily static, until VCG is running as well. |
| DCMCM_TUN_DEVICE | Tun device (e.g. tun0) to be used. Temporary, as each tun device will be mapped to a transaction. |


### Deployment

The DCMC Master is deployed as collection of Docker containers, utilizing docker-compose. Having cloned the
code of this repository, and having created the .env file the following commands should be executed:

```bash
cp .env /dcmc-master-client
cd dcmc-master-client
docker-compose up --build -d
```

## API
Here, the API that DCMC Master Client offers is briefly documented. For more information you may check the Swagger
documentation available at http://<HOST_IP>:<HOST_PORT>/catalyst/dcmcm/api/docs. The prefix of the API endpoints is 
/catalyst/dcmcm/api.

### Provide migration details
| **URL**             |	/migration/details/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_M_TOKEN |
| **Request body**	  | [MigrationDetails](#migrationdetails)      |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Migration details were provided. Details object was created.<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – No load or migration objects were found by this market action<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Place a migration request
| **URL**             |	/migration/request/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_M_TOKEN          |
| **Request body**	  | [MigrationRequest](#migrationrequest)      |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Migration request was placed. Migration object was created<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Associate migration with the CATALYST Marketplace information
| **URL**             |	/migration/marketplace/update/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_M_TOKEN          |
| **Request body**	  | [MigrationUpdate](#migrationupdate)        |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – Migration was associated with the CATALYST Marketplace Information<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Retrieve migration associated with marketaction
| **URL**             |	/migration/marketaction/{marketaction}/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	GET                                       |
| **Headers**         | Authorization: Bearer DCMC_M_TOKEN          |
| **Request body**	  | N/A                        |
| **Response body**	  | [Migration](#migration)    |
| **Response codes**  | 200 – Everything went well<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – Request migration not found<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Retrieve Controller Details
| **URL**             |	/transaction/{transaction_id}/service-endpoints/  |
| ------------------- | ------------------------------------------ |
| **Method**          |	GET                                        |
| **Headers**         | Accept: application/json<br>Authorization: Bearer DCMC_M_TOKEN |
| **Request body**	  | N/A                                        |
| **Response body**	  | [ControllerDetails](#controllerdetails)    |
| **Response codes**  | 200 – Service endpoints were retrieved.<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – 	Requested resource not found<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Request Migration Rollback
| **URL**             |	/transaction/{transaction_id}/migration/rollback |
| ------------------- | ------------------------------------------ |
| **Method**          |	GET                                        |
| **Headers**         | Accept: application/json<br>Authorization: Bearer DCMC_M_TOKEN |
| **Request body**	  | N/A                                        |
| **Response body**	  | N/A                                        |
| **Response codes**  | 202 – Rollback request was accepted.<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – 	Requested resource not found<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Retrieve Service Endpoints
| **URL**             |	/transaction/{transaction_id}/service-endpoints/  |
| ------------------- | ------------------------------------------ |
| **Method**          |	GET                                        |
| **Headers**         | Accept: application/json<br>Authorization: Bearer DCMC_M_TOKEN |
| **Request body**	  | N/A                                        |
| **Response body**	  | [ServiceEndpoints](#serviceendpoints)    |
| **Response codes**  | 200 – Service endpoints were retrieved.<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – 	Requested resource not found<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Report Compute Readiness
| **URL**             |	/transaction/{transaction_id}/vcontainer/status/registered/  |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_M_TOKEN |
| **Request body**	  | [OperationalDetails](#operationaldetails)  |
| **Response body**	  | N/A                                        |
| **Response codes**  | 200 – Readiness of CMP was reported to Source DC.<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – No details were found for the provided transaction<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Request VPN creation
| **URL**             |	/transaction/{transaction_id}/vpn/create/  |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Authorization: Bearer DCMC_M_TOKEN         |
| **Request body**	  | N/A                                        |
| **Response body**	  | N/A                                        |
| **Response codes**  | 202 – Request for VPN Creation was accepted.<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>404 – No details were found for the provided transaction<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Trigger VContainer creation
| **URL**             |	/vcontainer/ |
| ------------------- | ------------------------------------------ |
| **Method**          |	POST                                       |
| **Headers**         | Content-Type: application/json<br>Authorization: Bearer DCMC_M_TOKEN |
| **Request body**	  | [VContainer](#vcontainer)                  |
| **Response body**	  | N/A                                        |
| **Response codes**  | 201 – VC Creation was triggered. VC Object was created<br>400 – Provided data is invalid or malformed<br>401 – Unauthorized users are not allowed to view this resource<br>403 – Operation forbidden for this type of user<br>405 – This type of method is not allowed for this endpoint<br>500 – Internal server error|

### Data Model

#### ControllerDetails
```json
{
   "required": ["name", "ip", "port"], 
   "type": "object", 
   "properties": {
      "name": {
         "title": "Name", 
         "description": "The name of the OpenStack hosting Keystone in source DC", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "ip": {
         "title": "Ip", 
         "description": "The IP of the OpenStack Node", 
         "type": "string", 
         "maxLength": 15, 
         "minLength": 1
      }, 
      "port": {
         "title": "Port", 
         "description": "The Port of the DCMC Master", 
         "type": "integer", 
         "default": 60001
      }
   }
}
```

#### MigrationDetails
```json
{
   "required": ["marketaction", "source", "destination", "delivery_start", "delivery_end", "transaction"], 
   "type": "object", 
   "properties": { 
      "marketaction": {
         "title": "Marketaction", 
         "description": "The ID of the market action corresponding to the migration", 
         "type": "integer", 
         "minimum": 1
      }, 
      "source": {
         "title": "Source", 
         "description": "The username of the migration's source DC as understood by the DCMC Server", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "destination": {
         "title": "Destination", 
         "description": "The username of the destination DC as understood by the DCMC Server", 
         "type": "string", 
         "maxLength": 30, 
         "minLength": 1
      }, 
      "delivery_start": {
         "title": "Delivery start", 
         "description": "The time on which the migration is planned to start", 
         "type": "string", 
         "format": "date-time"
      }, 
      "delivery_end": {
         "title": "Delivery end", 
         "description": "The time on which the remote execution is planned to end", 
         "type": "string", 
         "format": "date-time"
      }, 
      "transaction": {
         "title": "Transaction", 
         "description": "The ID of the transaction as kept in the CATALYST IT Load Marketplace", 
         "type": "integer", 
         "minimum": 1
      }
   }
}
```

#### MigrationRequest
```json 
{
   "required": ["date", "vc_tag", "starttime", "endtime", "load_values", "price", "action_type"],
   "type": "object",
   "properties": {
      "date": {
         "title": "Date",
         "description": "The time on which the request is created",
         "type": "string",
         "format": "date-time"
      },
      "vc_tag": {
         "title": "Vc tag",
         "description": "The VC Tag, as this has been generated by the VCG",
         "type": "string",
         "maxLength": 100,
         "minLength": 1
      },
      "starttime":{
         "title": "Starttime",
         "description": "The time on which the requested migration is planned to start",
         "type": "string",
         "format": "date-time"
      },
      "endtime":{
         "title": "Endtime",
         "description": "The time on which the requested migration is planned to end",
         "type": "string",
         "format": "date-time"
      },
      "load_values": {
         "description": "Information about the load characteristics",
         "type": "array",
         "items": {
            "$ref": "#/definitions/LoadValue"
         }
      },
      "price": {
         "title": "Price",
         "description": "The price suggested for the migration action",
         "type": "number"
      },
      "action_type": {
         "title": "Action type",
         "description": "The type of market action placed on the Catalyst ITLM",
         "type": "string",
         "enum": ["bid", "offer"]
      }
   }
   "definitions": {
      "LoadValue": {
         "description": "Information about the load characteristics",
         "required": ["parameter", "value", "uom"],
         "type": "object",
         "properties": {
            "parameter": {
               "title": "Parameter",
               "description": "The name of the load characteristic",
               "type": "string",
               "enum": ["cpu", "ram", "disk"]
            },
            "value": {
               "title": "Value",
               "description": "The value quantifying the load characteristic",
               "type": "number"
            },
            "uom": {
               "title": "Uom",
               "description": "The unit of measurement of the value depending on the parameter",
               "type": "string",
               "maxLength": 5,
               "minLength": 1
            }
         }
      }
   }
}
```

#### MigrationUpdate
```json
{
   "required": ["migration", "status", "marketaction"], 
   "type": "object", 
   "properties": {
      "migration": {
         "title": "Migration", 
         "description": "The ID of the migration", 
         "type": "integer",
         "minimum": 1
      }, 
      "status": {
         "title": "Status", 
         "description": "The status of the migration", 
         "type": "string", 
         "enum": ["pending", "posted", "rejected", "started", "success", "failed"]
      }, 
      "marketaction": {
         "title": "Marketaction", 
         "description": "The ID of the market action corresponding to the migration", 
         "type": "integer",
         "minimum": 1
      }
   }
}
```

#### Migration
```json
{
   "required": ["load", "status"], 
   "type": "object", 
   "properties": {
      "id": {
         "title": "ID", 
         "type": "integer", 
         "readOnly": true
      }, 
      "load": {
         "title": "Load", 
         "type": "integer"
      }, 
      "marketaction": {
         "title": "Marketaction", 
         "description": "The ID of the market action corresponding to migration", 
         "type": "integer", 
         "minimum": 1, 
         "x-nullable": true
      }, 
      "status": {
         "title": "Status", 
         "description": "The migration status", 
         "type": "string", 
         "enum": ["pending", "posted", "rejected", "started", "success", "failed"]
      }
   }
}
```

#### OperationalDetails
```json
{
   "required": ["host_name", "vpn_ip"], 
   "type": "object", 
   "properties": {
      "host_name": {
         "title": "Hostname", 
         "description": "Hostname of participating CMP node", 
         "type": "string",
         "maxLength": 30, 
         "minLength": 1
      }, 
      "vpn_ip": {
         "title": "VPN IP", 
         "description": "VPN IP of participating CMP node", 
         "type": "string", 
         "maxLength": 15, 
         "minLength": 8
      }
   }
}
```

#### ServiceEndpoints
```json
{
   "required": ["authenticate_uri", "authorization_url", "glance_service", "memcached_service", "memcached_secret_key", 
                "neutron_service", "neutron_password", "nova_password", "placement_service", "placement_password", 
                "transport_url", "nfs_server_host", "nfs_server_root", "nova_uid", "nova_gid", "libvirt_uid", 
                "libvirt_gid", "libvirt_group", "libvirt_user"], 
   "type": "object", 
   "properties": {
      "authenticate_uri": {
         "title": "Authenticate uri", 
         "description": "OS Authenticate URI", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "authorization_url": {
         "title": "Authorization url", 
         "description": "OS Authorization URL", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "glance_service": {
         "title": "Glance service", 
         "description": "OS Glance Service", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "memcached_service": {
         "title": "Memcached service", 
         "description": "OS Memcached Servers", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "memcached_secret_key": {
         "title": "Memcached secret key", 
         "description": "OS Memcached Secret Key", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "neutron_service": {
         "title": "Neutron service", 
         "description": "OS Neutron Service", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "neutron_password": {
         "title": "Neutron password", 
         "description": "OS Neutron Password", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "nova_password": {
         "title": "Nova password", 
         "description": "OS Nova Password", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "placement_service": {
         "title": "Placement service", 
         "description": "OS Placement Service", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "placement_password": {
         "title": "Placement password", 
         "description": "OS Placement Password", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "transport_url": {
         "title": "Transport url", 
         "description": "OS Transport URL", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "nfs_server_host": {
         "title": "Nfs server host", 
         "description": "NFS Server Host", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "nfs_server_root": {
         "title": "Nfs server root", 
         "description": "NFS Server Root Dir", 
         "type": "string", 
         "maxLength": 256, 
         "minLength": 1
      }, 
      "nova_uid": {
         "title": "Nova uid", 
         "description": "Nova UID", 
         "type": "string", 
         "maxLength": 8, 
         "minLength": 1
      }, 
      "nova_gid": {
         "title": "Nova gid", 
         "description": "Nova GID", 
         "type": "string", 
         "maxLength": 8, 
         "minLength": 1
      }, 
      "libvirt_uid": {
         "title": "Libvirt uid", 
         "description": "Libvirt-Qemu UID", 
         "type": "string", 
         "maxLength": 8, 
         "minLength": 1
      }, 
      "libvirt_gid": {
         "title": "Libvirt gid", 
         "description": "KVM GID", 
         "type": "string", 
         "maxLength": 8, 
         "minLength": 1
      }, 
      "libvirt_group": {
         "title": "Libvirt group", 
         "description": "Libvirt GID", 
         "type": "string", 
         "maxLength": 8, 
         "minLength": 1
      }, 
      "libvirt_user": {
         "title": "Libvirt user", 
         "description": "Libvirt User", 
         "type": "string", 
         "maxLength": 8, 
         "minLength": 1
      }
   }
}
```

#### VContainer
```json
{
   "required": ["cpu", "cpu_uom", "ram", "ram_uom", "disk", "disk_uom", "start", "end", "transaction"], 
   "type": "object", 
   "properties": {
      "id": {
         "title": "ID", 
         "type": "integer", 
         "readOnly": true
      }, 
      "cpu": {
         "title": "Cpu", 
         "description": "The number of CPU cores of the VC described", 
         "type": "integer", 
         "minimum": 1
      }, 
      "cpu_uom": {
         "title": "Cpu uom", 
         "description": "The unit of measurement for the CPU value", 
         "type": "string", 
         "maxLength": 5, 
         "minLength": 1
      }, 
      "ram": {
         "title": "Ram", 
         "description": "The amount of RAM of the VC described", 
         "type": "integer", 
         "minimum": 1
      }, 
      "ram_uom": {
         "title": "Ram uom", 
         "description": "The unit of measurement for the RAM value", 
         "type": "string", 
         "maxLength": 5, 
         "minLength": 1
      }, 
      "disk": {
         "title": "Disk", 
         "description": "The amount of disk of the VC described", 
         "type": "integer", 
         "minimum": 1
      }, 
      "disk_uom": {
         "title": "Disk uom", 
         "description": "The unit of measurement for the disk value", 
         "type": "string", 
         "maxLength": 5, 
         "minLength": 1
      }, 
      "start": {
         "title": "Start", 
         "description": "The time from which the VC is planned to be active", 
         "type": "string", 
         "format": "date-time"
      }, 
      "end": {
         "title": "End", 
         "description": "The time until which the VC is planned to be active", 
         "type": "string", 
         "format": "date-time"
      }, 
      "uuid": {
         "title": "Uuid", 
         "description": "Identification information of the issuing VM", 
         "type": "string", 
         "maxLength": 100, 
         "x-nullable": true
      }, 
      "transaction": {
         "title": "Transaction", 
         "description": "The ID of the transaction as kept in the CATALYST IT Load Marketplace", 
         "type": "integer", 
         "minimum": 1
      }, 
      "status": {
         "title": "Status", 
         "description": "The status of the VC", 
         "type": "string", 
         "enum": ["created", "registered"], 
         "x-nullable": true
      }, 
      "covpn_owner": {
         "title": "Covpn owner", 
         "description": "Owner of VPN connection for the vContainer to connect to", 
         "type": "string", 
         "maxLength": 32, 
         "x-nullable": true
      }
   }
}
```

### Parameters
| **Parameter** | **Type** | **Comments** |
| ------------- | ------------- | ------------- |
| marketaction  | Integer | The id of the market action, corresponding to the migration of interest. |
| uuid          | String | Identification information of the VM, for which the associated transaction is requested. |
| transaction_id | Integer | The id of the transaction in the CATALYST IT Load Marketplace. |