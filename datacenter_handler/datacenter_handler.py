from datacenter_handler import config
from datacenter_handler.abstract_dc_handler.basehandler import AbstractDCHandler
from datacenter_handler.openstack.openstack import OpenStackHandler
from datacenter_handler.vmware.vmware import VMWareClient

dc_handler = None


def get_dc_handler():
    """Get DC Handler Class Instance.

    Returns
    -------
    dc_handler : DCHandler
        A instance of DCHandler Class

    """
    global dc_handler
    if dc_handler is None:
        dc_handler = DCHandler(config.DC_TYPE)
    return dc_handler


class DCHandler(AbstractDCHandler):
    """DC Handler Class.

    This class serves as a wrapper for essential interactions with the Datacenters that
    take part in a migration.

    Attributes
    ----------
    session
        An authorization session

    Methods
    -------
    get_hypervisors()
        Return the hypervisor of the datacenter
    create_vm(cpu, memory, disk, name, metadata, user_data)
        Create VM and return its UUID
    delete_vm(uuid)
        Delete a VM by its UUID
    live_migrate_vm(uuid, migrate_to)
        Live migrate a VM to a given host

    """

    def __init__(self, dc_type):
        """DC Handler Class Constructor.

        Parameters
        ----------
        dc_type : str
            The type of the DC (e.g. openstack, vmware)

        """
        self.handler = None
        if dc_type == 'openstack':
            self._set_openstack_handler()
        elif dc_type == 'vmware':
            self._set_vmware_handler()

    def _set_openstack_handler(self):
        """Set OpenStack Handler. """
        self.handler = OpenStackHandler(auth_url=config.OS_AUTH_URL,
                                        username=config.OS_USERNAME,
                                        password=config.OS_USER_PWD,
                                        project_name=config.OS_PROJECT_NAME,
                                        project_domain_name=config.OS_PROJECT_DOMAIN_ID,
                                        user_domain_name=config.OS_USER_DOMAIN_ID)

    def _set_vmware_handler(self):
        """Set VMWare Handler. """
        # TODO: Complete properly
        self.handler = VMWareClient()

    @property
    def session(self):
        """An authorization session."""
        return self.handler.session

    def get_hypervisors(self):
        """Returns the hypervisors of a datacenter.

        Returns
        -------
        hypervisors : list
            A list of hypervisors

        """
        return self.handler.get_hypervisors()

    def delete_service_by_host(self, service_host):
        """Delete service by host that it lies in.

        Parameters
        ----------
        service_host : str
            The host of the service to delete

        """
        self.handler.delete_service_by_host(service_host)

    def delete_network_agent_by_host(self, agent_host):
        """Delete network agent by hostname.

        Parameters
        ----------
        agent_host : str
            The hostname of the agent to delete.

        """
        self.handler.delete_network_agent_by_host(agent_host)

    def get_hypervisor_of_server(self, uuid):
        """Retrieves the hypervisor name of a specific server.

        Parameters
        ----------
        uuid : str
            The VM UUID to retrieve hypervisor name for

        Returns
        -------
        hypervisor_name : str
            The name of the hypervisor

        """
        return self.handler.get_hypervisor_of_server(uuid)

    def create_vm(self, cpu, memory, disk, name=None, metadata=None, user_data=None):
        """Creates a VM instance.

        Parameters
        ----------
        cpu : int
            The number of CPU cores
        memory : int
            The amount of RAM in MB
        disk : int
            The amount of disk in GB
        name : str
            The name of the VM instance
        metadata : dict
            The metadata of the VM, if any
        user_data : str
            The user_data of the VM if any

        Returns
        -------
        uuid : str
            The UUID of the created VM

        """
        return self.handler.create_vm(cpu, memory, disk, name, metadata, user_data)

    def delete_vm(self, uuid):
        """Delete a VM by its UUID

        Parameters
        ----------
        uuid : str
            The UUID of the VM to delete

        """
        self.handler.delete_vm(uuid)

    def live_migrate_vm(self, uuid, migrate_to):
        """Live migrate a VM instance.

        Parameters
        ----------
        uuid : str
            The UUID of the VM to migrate
        migrate_to : str
            The name of the host to migrate to

        """
        return self.handler.live_migrate_vm(uuid, migrate_to)
