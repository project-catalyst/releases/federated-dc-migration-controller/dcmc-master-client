import os

# ==================================
#     VIRTUAL COMPUTE SETTINGS
# ==================================
OS_VCMP_IMAGE = os.getenv('DCMCM_OS_VCMP_IMAGE')
OS_VCMP_PROJECT = os.getenv('DCMCM_OS_VCMP_PROJECT')
OS_VCMP_KEYPAIR = os.getenv('DCMCM_OS_VCMP_KEYPAIR')
OS_VCMP_MIN_CPU = int(os.getenv('DCMCM_OS_VCMP_MIN_CPU'))
OS_VCMP_MIN_RAM = int(os.getenv('DCMCM_OS_VCMP_MIN_RAM'))
OS_VCMP_MIN_DISK = int(os.getenv('DCMCM_OS_VCMP_MIN_DISK'))

# ==================================
#        OPENSTACK SETTINGS
# ==================================
OS_AUTH_URL = os.getenv('DCMCM_OS_AUTH_URL')
OS_USERNAME = os.getenv('DCMCM_OS_USERNAME')
OS_USER_PWD = os.getenv('DCMCM_OS_USER_PWD')
OS_PROJECT_NAME = os.getenv('DCMCM_OS_PROJECT_NAME')
OS_PROJECT_DOMAIN_ID = os.getenv('DCMCM_OS_PROJECT_DOMAIN_ID')
OS_USER_DOMAIN_ID = os.getenv('DCMCM_OS_USER_DOMAIN_ID')

# ==================================
#           DC SETTINGS
# ==================================
DC_TYPE = os.getenv('DCMCM_DC_TYPE')
