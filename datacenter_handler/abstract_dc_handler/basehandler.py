import abc


class AbstractDCHandler(abc.ABC):
    """Abstract DC Handler Class. """

    @property
    def session(self):
        """An authorization session."""
        pass

    def get_hypervisors(self):
        """Retrieves the hypervisors of the Datacenter.

        Returns
        -------
        hypervisors : list
            A list of the available hypervisors of the DC.

        """
        pass

    def get_hypervisor_of_server(self, uuid):
        """Retrieves the hypervisor name of a specific server.

        Parameters
        ----------
        uuid : str
            The VM UUID to retrieve hypervisor name for

        Returns
        -------
        hypervisor_name : str
            The name of the hypervisor

        """
        pass

    def delete_service_by_host(self, service_host):
        """Delete service by host.

        Parameters
        ----------
        service_host : str
            The host of the service to delete

        """
        pass

    def delete_network_agent_by_host(self, agent_host):
        """Delete network agent by hostname.

        Parameters
        ----------
        agent_host : str
            The hostname of the agent to delete.

        """
        pass

    def create_vm(self, cpu, memory, disk, name=None, metadata=None, user_data=None):
        """Create a VM Instance.

        Parameters
        ----------
        cpu : int
            The number of CPU cores
        memory : int
            The required RAM in MB
        disk : int
            The required disk in GB
        name : str
            The name of the VM instance
        metadata : dict
            A dict of arbitrary key/value metadata to store for this server
        user_data : file, str
            A string or file defining an early installation script

        Returns
        -------
        uuid : str
            The UUID of the created VM

        """
        pass

    def delete_vm(self, uuid):
        """Delete a VM by its UUID.

        Parameters
        ----------
        uuid : str
            The UUID of the VM under deletion

        """
        pass

    def live_migrate_vm(self, uuid, migrate_to):
        """Live migrate instance to host.

        Parameters
        ----------
        uuid : str
            The instance's UUID
        migrate_to : str
            The name of the compute node to migrate instance to.

        Returns
        -------
        status : {'failed', 'success'}
            The status of the attempted migration

        """
        pass
