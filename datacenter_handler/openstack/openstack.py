import logging
import sys
import time

from glanceclient import client as glance
from keystoneauth1 import session as keystone_session
from keystoneauth1.identity import v3
from keystoneclient.v3 import client as keystone
from neutronclient.v2_0 import client as neutron
from novaclient import client as nova

from api.constants import SUCCESS, FAILED, OPENSTACK
from datacenter_handler import config
from datacenter_handler.abstract_dc_handler.basehandler import AbstractDCHandler

logger = logging.getLogger(OPENSTACK)


class OpenStackHandler(AbstractDCHandler):
    """OpenStack Handler Class.

    This is a class which serves as a wrapper for various OpenStack utilities.
    It includes methods for creation, deletion and live migration of a VM instance
    along with the prerequisites of these operations, such as authentication.

    Attributes
    ----------
    session : keystone_session.Session
        A Keystone Authorization Session
    glance_client : glance.Client
        An instance of the Glance Client Class
    keystone_client : keystone.Client
        An instance of the Keystone Client Class
    neutron_client : neutron.Client
        An instance of the Neutron Client Class
    nova_client : nova.Client
        An instance of the Nova Client Class

    Methods
    -------
    get_hypervisors()
        Returns a list of hypervisor objects
    get_projects()
        Returns all the projects
    get_or_create_flavor(cpu, memory, disk)
        Retrieves or creates and retrieves a flavor
    get_networks(project)
        Returns the networks of a project
    get_floating_ip()
        Creates and returns a floating IP
    servers_for_project(project)
        Returns the servers of a project
    servers_for_projects(projects)
        Returns the servers for a list of projects
    all_servers()
        Returns all servers
    get_server(uuid)
        Returns a specific server by UUID
    images_for_project(project)
        Returns the images of a project
    images_for_projects(projects)
        Returns the images for a list of projects
    get_image(project, uuid, name)
        Returns an image by project, uuid and name
    all_images()
        Returns all images
    servers_for_host(host)
        Returns all servers of a given host
    format_server(server, project)
        Returns a server formatted as a dict
    servers_of_status(projects, status)
        Returns the servers of a specific status
    get_project_name_from_id(project_id)
        Returns the name of a project by its id
    create_vm(cpu, memory, disk, name, metadata, user_data)
        Creates a new VM and returns its UUID
    delete_vm(uuid)
        Deletes a VM by UUID
    live_migrate_vm(uuid, migrate_to)
        Live migrate instance to host

    """

    def __init__(self, auth_url, username, password, project_name, project_domain_name, user_domain_name):
        """OpenStack Client Constructor Class.

        Parameters
        ----------
        auth_url : str
            The Keystone Authorization URL
        username : str
            The OpenStack username to use
        password : str
            The OpenStack user password
        project_name : str
            The OpenStack project name
        project_domain_name : str
            The OpenStack project domain name
        user_domain_name : str
            The OpenStack user domain name

        """
        self.__auth_url = auth_url
        self.__username = username
        self.__password = password
        self.__project_name = project_name
        self.__project_domain_name = project_domain_name
        self.__user_domain_name = user_domain_name

        # Session and clients
        self.__session = None
        self.__keystone_client = None
        self.__nova_client = None
        self.__glance_client = None
        self.__neutron_client = None

    @property
    def session(self):
        """Get a Keystone Authentication Client.

        Returns
        -------
        keystone_session.Session
            A Keystone Authorization Session

        """
        if self.__session is None:
            auth = v3.Password(auth_url=self.__auth_url,
                               username=self.__username,
                               password=self.__password,
                               project_name=self.__project_name,
                               project_domain_name=self.__project_domain_name,
                               user_domain_name=self.__user_domain_name)
            self.__session = keystone_session.Session(auth=auth)
        return self.__session

    @property
    def glance_client(self):
        if self.__glance_client is None:
            self.__glance_client = glance.Client('2', session=self.session)
        return self.__glance_client

    @property
    def keystone_client(self):
        if self.__keystone_client is None:
            self.__keystone_client = keystone.Client(session=self.session)
        return self.__keystone_client

    @property
    def neutron_client(self):
        if self.__neutron_client is None:
            self.__neutron_client = neutron.Client(session=self.session)
        return self.__neutron_client

    @property
    def nova_client(self):
        if self.__nova_client is None:
            self.__nova_client = nova.Client('2.1', session=self.session)
        return self.__nova_client

    def get_hypervisors(self):
        """Retrieve the hypervisor's of the OpenStack installation.

        Returns
        -------
        hypervisors : list
            A list of hypervisor objects

        """
        return self.nova_client.hypervisors.list()

    def get_hypervisor_of_server(self, uuid):
        """Retrieve the hypervisor name of a server.

        Parameters
        ----------
        uuid : str
            The VM UUID to retrieve hypervisor for

        Returns
        -------
        hypervisor_hostname : str
            The name of the VM's hypervisor

        """
        return self.nova_client.servers.get(uuid).to_dict()['OS-EXT-SRV-ATTR:host']

    def get_service_by_host(self, service_host):
        """Retrieve ID of compute service by name

        Parameters
        ----------
        service_host : str
            The name of the compute service

        Returns
        -------
        service_id : int
            The ID of the compute service

        """
        services = self.nova_client.services.list()
        for service in services:
            if service.host == service_host:
                return service.id
        return None

    def delete_service_by_host(self, service_host):
        """Delete service by its ID.

        Parameters
        ----------
        service_host : str
            The host of the service to delete

        """
        service_id = self.get_service_by_host(service_host)
        if service_id is not None:
            self.nova_client.services.delete(service_id)

    def get_network_agent_by_host(self, agent_host):
        """Retrieve network agent by hostname.

        Parameters
        ----------
        agent_host : str
            The hostname of the network agent.

        Returns
        -------
        agent_id : str
            The ID of the requested network agent.

        """
        network_agents = self.neutron_client.list_agents()['agents']
        for network_agent in network_agents:
            if network_agent['host'] == agent_host:
                return network_agent['id']
        return None

    def delete_network_agent_by_host(self, agent_host):
        """Delete network agent by hostname.

        Parameters
        ----------
        agent_host : str
            The hostname of the agent to delete.

        """
        agent_id = self.get_network_agent_by_host(agent_host)
        if agent_id is not None:
            self.neutron_client.delete_agent(agent_id)

    def get_projects(self):
        """Get all the OpenStack projects.

        Returns
        -------
        projects : list
            The list of available OpenStack projects

        """
        return self.keystone_client.projects.list()

    def get_or_create_flavor(self, cpu, memory, disk):
        """Get or create OpenStack flavor with given characteristics.

        Parameters
        ----------
        cpu : int
            The CPU of the flavor
        memory : int
            The RAM of the flavor in MB
        disk : int
            The disk of the flavor in GB

        Returns
        -------
        The flavor, either existing or created

        """
        flavor = None
        for fl in self.nova_client.flavors.list():
            if fl.ram == memory and fl.vcpus == cpu and fl.disk == disk:
                flavor = fl
                break
        return flavor if flavor is not None else \
            self.nova_client.flavors.create(name='fl-{}-{}-{}'.format(cpu, memory, disk),
                                            ram=memory, vcpus=cpu, disk=disk, is_public=True)

    def get_networks(self, project='admin'):
        """Fetch the networks of a project.

        Parameters
        ----------
        project : str
            The project to authenticate against

        Returns
        -------
        networks : list
            The identified internal and external networks

        """
        networks = {'internal': [], 'external': []}
        projects = [p.id for p in self.get_projects() if p.name == project]
        if len(projects) != 1:
            print('Could not resolve networks for project {}'.format(project))
            sys.exit(-10)
        for network in self.neutron_client.list_networks()['networks']:
            if network['project_id'] in projects:
                networks['internal'].append(network)
            elif network['router:external']:
                networks['external'].append(network)
        return networks

    def get_floating_ip(self):
        """Create and returns a new Floating IP.

        Returns
        -------
        The newly allocated Floating IP

        """
        return self.neutron_client.create_floatingip()

    def get_keypair(self, uuid=None, name=None):
        """Fetches a keypair by UUID or name.

        Parameters
        ----------
        uuid : str, optional
            The ID of the Keypair to fetch.
        name : str, optional
            The name of the keypair to fetch

        Returns
        -------
        The requested keypair if it exists else None

        """
        keypairs = self.nova_client.keypairs.list()
        if len(keypairs) == 1:
            return keypairs[0]
        for keypair in keypairs:
            if uuid == keypair.id or name == keypair.name:
                return keypair
        return None

    def servers_for_project(self, project='admin'):
        """Gets the available servers for a specific project.

        Parameters
        ----------
        project : str
            The project name

        Returns
        -------
        servers : list
            The list of Servers belonging to the project

        """
        return self.servers_for_projects([project] if type(project) is str else project)

    def servers_for_projects(self, projects=None):
        """Fetches the available servers for a list of projects.

        Parameters
        ----------
        projects : list
            A list of projects

        Returns
        -------
        servers : list
            The list of Servers belonging in the projects list

        """
        projects = [] if projects is None else projects
        servers_list = []
        for project in self.get_projects():
            if project.name in projects:
                servers_list += self.nova_client.servers.list()
        return servers_list

    def all_servers(self):
        """Gets all servers for all projects.

        Returns
        -------
        servers : list
            The list of all servers

        """
        return self.servers_for_projects([x.name for x in self.get_projects()])

    def get_server(self, uuid=None):
        """Fetches a specific server.

        Parameters
        ----------
        uuid : str
            The id of the server (VM) to fetch

        Returns
        -------
        The server instance if it exists, else None

        """
        return None if uuid is None else self.nova_client.servers.get(uuid)

    def images_for_projects(self, projects=None):
        """Gets the images for a selected list of projects.

        Parameters
        ----------
        projects : list
            The list of projects to search images for

        Returns
        -------
        images : list
            The list of all images available to the projects.

        """
        projects = [] if projects is None else projects
        project_images = []
        for project in self.get_projects():
            if project.name in projects:
                project_images.extend(self.glance_client.images.list())
        return project_images

    def images_for_project(self, project='admin'):
        """Get the images for a selected project by name.

        Parameters
        ----------
        project : str
            The project to search images for

        Returns
        -------
        images : list
            All images for project

        """
        return self.images_for_projects(projects=[project] if type(project) is str else ['admin'])

    def get_image(self, project='admin', uuid=None, name=None):
        """Gets an image out of a project, based on its UUID.

        Parameters
        ----------
        project : str
            The project to search images for
        uuid : str, optional
            The UUID of the image
        name : str, optional
            The name of the image

        Returns
        -------
        The requested image object

        """
        for image in self.images_for_project(project=project):
            if (uuid is not None and image.id == uuid) or (name is not None and image.name == name):
                return image
        return None

    def all_images(self):
        """Gets all images from all projects.

        Returns
        -------
        images : list
            All images available for all projects

        """
        images = []
        for project in self.get_projects():
            images.append(image for image in self.images_for_project(project))
        return images

    def servers_for_host(self, host=None):
        """Gets all servers for a given host name.

        Parameters
        ----------
        host : str
            The hostname to check for

        Returns
        -------
        servers : list
            The list of servers matching the host as well as the list of servers with no known host

        """
        servers = self.all_servers()
        matching_host_servers, unknown_host_servers = [], []
        for server in servers:
            if hasattr(server, 'OS-EXT-SRV-ATTR:hypervisor_hostname') and \
                            getattr(server, 'OS-EXT-SRV-ATTR:hypervisor_hostname') == host:
                matching_host_servers.append(server)
            else:
                unknown_host_servers.append(server)
        return matching_host_servers, unknown_host_servers

    def format_server(self, server, project=None):
        """Formats a server.

        Parameters
        ----------
        server : str
            The server to format
        project : str
            The server's project

        Returns
        -------
        server : dict
            A simplified server representation

        """
        if server is None:
            return None
        host, virsh_id = None, None
        if hasattr(server, 'OS-EXT-SRV-ATTR:hypervisor_hostname'):
            host = getattr(server, 'OS-EXT-SRV-ATTR:hypervisor_hostname')
        if hasattr(server, 'OS-EXT-SRV-ATTR:instance_name'):
            virsh_id = getattr(server, 'OS-EXT-SRV-ATTR:instance_name')
        if len(server.networks) < 1:
            ip = None
        else:
            ip = server.networks[list(server.networks)[0]][1] if len(
                list(server.networks[list(server.networks)[0]])) > 1 else server.networks[list(server.networks)[0]][0]
        return {
            'name': server.name,
            'uuid': server.id,
            'ip': ip,
            'project': project if project is not None else self.get_project_name_from_id(getattr(server, 'tenant_id')),
            'host': host,
            'status': server.status,
            'virsh_id': virsh_id,
            'task': getattr(server, 'OS-EXT-STS:task_state')
        }

    def servers_of_status(self, projects=None, status='ERROR'):
        """Gets all servers belonging to a set of projects having a certain status.

        Parameters
        ----------
        session : keystone_session.Session, optional
            The Keystone Session to authenticate against
        projects : list
            The list of projects to search into. Default is None (all projects)
        status : str
            The status of interest. For a complete list of statuses check https://wiki.openstack.org/wiki/VMState

        Returns
        -------
        servers : list
            The list of VMs holding this status.

        """
        projects = [] if projects is None else projects
        servers, all_relevant_servers = [], []
        all_relevant_servers = self.all_servers() if (not projects or len(projects) == 0) else \
            self.servers_for_projects(projects)
        for server in all_relevant_servers:
            if server.status == status:
                servers.append(server)
        return servers

    def get_project_name_from_id(self, project_id):
        """Get project name from ID.

        Parameters
        ----------
        project_id : str
            The ID of the project to fetch

        Returns
        -------
        name : str
            The name of the project

        """
        try:
            project = self.keystone_client.projects.get(project_id)
            return project.name
        except Exception:
            return None

    def create_vm(self, cpu, memory, disk, name=None, metadata=None, user_data=None):
        """Create a new VM in OpenStack.

        Parameters
        ----------
        cpu : int
            The number of CPU cores
        memory : int
            The required RAM in MB
        disk : int
            The required disk in GB
        name : str
            The name of the VM
        metadata : dict
            A dict of arbitrary key/value metadata to store for this server
        user_data : file, str
            A string or file defining an early installation script

        Returns
        -------
        uuid : str
            The UUID of the created VM

        """
        logger.info('Starting VM creation for Virtual Compute Node')

        # Get the necessary information. Deploy with a minimum flavor
        logger.debug('Getting proper flavor')
        flavor_cpu = config.OS_VCMP_MIN_CPU if config.OS_VCMP_MIN_CPU > cpu else cpu
        flavor_ram = config.OS_VCMP_MIN_RAM if config.OS_VCMP_MIN_RAM > memory else memory
        flavor_disk = config.OS_VCMP_MIN_DISK if config.OS_VCMP_MIN_DISK > disk else disk
        flavor = self.get_or_create_flavor(cpu=flavor_cpu, memory=flavor_ram, disk=flavor_disk)

        logger.debug('Getting proper image')
        image = self.get_image(name=config.OS_VCMP_IMAGE)

        # TODO: Remove keypair, vCMP should not have .pem
        logger.debug('Getting proper keypair')
        keypair = self.get_keypair(name=config.OS_VCMP_KEYPAIR)

        logger.debug('Setting security groups to \'catalyst-sec-group\' (make sure you have SSH on)')
        security_groups = ['catalyst-sec-group']

        # log.debug("Getting proper network configuration")
        networks = self.get_networks(project=self.__project_name)
        nics = [{'net-id': n['id'], 'v4-fixed-ip': ''} for n in networks['internal']]

        # Create it!
        logger.debug('Creating server')
        # TODO: Key-pair has been removed
        server = self.nova_client.servers.create(name=name, image=image, flavor=flavor,
                                                 security_groups=security_groups, key_name=keypair.name,
                                                 nics=nics, meta=metadata, userdata=user_data)
        uuid = server.id
        while True:
            server = self.nova_client.servers.get(uuid)
            if server.status == 'ERROR':
                logger.info('Creation of VM has failed. VM is in ERROR status.')
                return None
            elif server.status == 'ACTIVE':
                logger.info('Creation of VM was successful. The VM\'s UUID is [{}].'.format(uuid))
                return uuid
            else:
                time.sleep(10)
                logger.debug('Server not ready, yet. Status: {}'.format(server.status))
                continue

    def delete_vm(self, uuid):
        """Delete VM by its UUID.

        Parameters
        ----------
        uuid : str
            The UUID of the VM to delete

        """
        logger.info('Preparing for deletion of VM [{}].'.format(uuid))
        self.nova_client.servers.delete(uuid)
        logger.info('VM with UUID [{}] was deleted.'.format(uuid))

    def live_migrate_vm(self, uuid, migrate_to):
        """Trigger live migration from source to destination DC.

        Parameters
        ----------
        uuid : str
            The OpenStack instance's UUID
        migrate_to : str
            The name of the compute node to migrate instance to.

        Returns
        -------
        status : {'failed', 'success'}
            The status of the attempted migration

        """
        logger.info('Live migrating VM [{}].'.format(uuid))
        vm_to_migrate = self.nova_client.servers.get(uuid)
        old_vm_host = vm_to_migrate.to_dict()['OS-EXT-SRV-ATTR:host']
        vm_to_migrate.live_migrate(host=migrate_to)
        while True:
            vm_under_migration = self.nova_client.servers.get(uuid)
            if vm_under_migration.status == 'ERROR':
                logger.info('Migration of VM [{}] has failed'.format(uuid))
                return FAILED
            elif vm_under_migration.status == 'ACTIVE':
                if vm_under_migration.to_dict()['OS-EXT-SRV-ATTR:host'] != old_vm_host:
                    logger.info('Migration of VM [{}] has succeeded'.format(uuid))
                    return SUCCESS
                else:
                    logger.info('Migration of VM [{}] failed. VM is active on its original host.'.format(uuid))
                    return FAILED
            else:
                logger.debug('Migration has not been completed yet. VM Status: {}'
                             .format(vm_to_migrate.status))
                time.sleep(3)
