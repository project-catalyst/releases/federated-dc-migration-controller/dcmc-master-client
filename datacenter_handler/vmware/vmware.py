from datacenter_handler.abstract_dc_handler.basehandler import AbstractDCHandler


class VMWareClient(AbstractDCHandler):
    """VMWare Client Class.

    This class is under construction and will implement at least the operations
    of creation, deletion and live migration of a VMWare VM instance. This class
    will expose, at least, the aforementioned interfaces but will also implement
    any essential prerequisites.

    """
    def __init__(self):
        """VMWare Client Class Constructor"""
        pass

    def get_hypervisors(self):
        """Retrieve the hypervisors of the datacenter."""
        pass

    def get_hypervisor_of_server(self, uuid):
        """Retrieve the hypervisor name of a server."""
        pass

    def delete_service_by_host(self, service_host):
        """Delete service by host."""

    def delete_network_agent_by_host(self, agent_host):
        """Delete network agent by hostname."""

    def create_vm(self, cpu, memory, disk, name=None, metadata=None, user_data=None):
        """Create a VM instance. """
        pass

    def delete_vm(self, uuid):
        """Delete a VM instance by its UUID. """
        pass

    def live_migrate_vm(self, uuid, migrate_to):
        """Live migrate instance to host. """
        pass
