import os

# Keycloak
os.environ['DCMCM_KEYCLOAK_URL'] = 'http://109.232.32.223:8080/auth/'
os.environ['DCMCM_KEYCLOAK_REALM'] = 'catalyst'
os.environ['DCMCM_KEYCLOAK_CLIENT_ID'] = 'resource_server'
os.environ['DCMCM_KEYCLOAK_CLIENT_SECRET'] = '67d46722-a19e-47c3-8665-24f2f93c992c'

# vCMP Preferences
os.environ['DCMCM_OS_VCMP_IMAGE'] = 'vCompute-Train-v1.0.0-SNAPSHOT'
os.environ['DCMCM_OS_VCMP_PROJECT'] = 'Catalyst'
os.environ['DCMCM_OS_VCMP_KEYPAIR'] = 'catalyst'
os.environ['DCMCM_OS_VCMP_MIN_CPU'] = '1'
os.environ['DCMCM_OS_VCMP_MIN_RAM'] = '2048'
os.environ['DCMCM_OS_VCMP_MIN_DISK'] = '20'
