# =================================
#           DC SETTINGS
# =================================
DC_SETTINGS = {
    # DC POPs: Old Laptop
    'DC_POPS.OLD_LAPTOP_URL': 'http://192.168.1.22:60001',
    'DC_POPS.OLD_LAPTOP_USERNAME': 'dc_pops.ubuntu',
    'DC_POPS.OLD_LAPTOP_PASSWORD': 'password',
    # DC POPs: Old Ctrl
    'DC_POPS.OLD_CTRL_URL': 'http://192.168.1.251:60001',
    'DC_POPS.OLD_CTRL_USERNAME': 'dc_pops.controller-1',
    'DC_POPS.OLD_CTRL_PASSWORD': 'password',
    # DC PSNC
    'DC_PSNC_URL': 'http://10.254.254.96:60001',
    'DC_PSNC_USERNAME': 'dc_psnc.xeon-08',
    'DC_PSNC_PASSWORD': 'password',
    # DC QRN
    'DC_QRN_URL': 'http://10.254.254.99:60011',
    'DC_QRN_USERNAME': 'dc_qrn.calm-trout',
    'DC_QRN_PASSWORD': 'password'
}