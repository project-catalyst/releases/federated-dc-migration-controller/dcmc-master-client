import time
from datetime import datetime, timedelta

from django.core.management import BaseCommand

from communication_handler.api_clients.dcmcm_api import DCMCMasterClient
from demo.config import DC_SETTINGS


def send_migration_details(source, destination, marketaction_source, marketaction_dest, transaction_id):
    """Function to execute migration between two DCs"""
    print('Sending migration details ...')
    print('Source: {}'.format(source))
    print('Destination: {}'.format(destination))

    # Get time & VC Tag
    now = datetime.now()
    start = now + timedelta(hours=1)
    end = now + timedelta(hours=2)
    delivery_start = start.strftime('%Y-%m-%dT%H:%M:%SZ')
    delivery_end = end.strftime('%Y-%m-%dT%H:%M:%SZ')

    # Get DCMC Master Clients
    dcmcm_source = DCMCMasterClient(dcmcm_host_url=DC_SETTINGS['{}_URL'.format(source)],
                                    username=DC_SETTINGS['{}_USERNAME'.format(source)],
                                    password=DC_SETTINGS['{}_PASSWORD'.format(source)])
    dcmcm_dest = DCMCMasterClient(dcmcm_host_url=DC_SETTINGS['{}_URL'.format(destination)],
                                  username=DC_SETTINGS['{}_USERNAME'.format(destination)],
                                  password=DC_SETTINGS['{}_PASSWORD'.format(destination)])

    payload_source = {"marketaction": marketaction_source, "source": source, "destination": destination,
                      "delivery_start": delivery_start, "delivery_end": delivery_end, "transaction": transaction_id}
    payload_dest = {"marketaction": marketaction_dest, "source": source, "destination": destination,
                    "delivery_start": delivery_start, "delivery_end": delivery_end, "transaction": transaction_id}

    # Send migration requests
    dcmcm_source.migration_details(payload_source)
    time.sleep(5)
    dcmcm_dest.migration_details(payload_dest)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--source', required=True,
                            help='[ DC_POPS.OLD_LAPTOP | DC_POPS.OLD_CTRL | DC_QRN | DC_PSNC ]')
        parser.add_argument('-d', '--dest', required=True,
                            help='[ DC_POPS.OLD_LAPTOP | DC_POPS.OLD_CTRL | DC_QRN | DC_PSNC ]')
        parser.add_argument('-ms', '--marketaction_source', required=True, help='Marketaction Source ID')
        parser.add_argument('-md', '--marketaction_dest', required=True, help='Marketaction Dest ID')
        parser.add_argument('-t', '--transaction', required=True, help='Transaction ID')

    def handle(self, *args, **options):
        source = options['source']
        dest = options['dest']
        marketaction_source = options['marketaction_source']
        marketaction_dest = options['marketaction_dest']
        transaction = options['transaction']
        send_migration_details(source, dest, marketaction_source, marketaction_dest, transaction)
