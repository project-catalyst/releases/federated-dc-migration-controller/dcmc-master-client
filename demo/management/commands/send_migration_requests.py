from datetime import datetime, timedelta

from django.core.management import BaseCommand

from communication_handler.api_clients.dcmcm_api import DCMCMasterClient
from demo.config import DC_SETTINGS


def send_migration_requests(source, destination):
    """Function to execute migration between two DCs"""
    print('Initiating Live-migration testing ...')
    print('Source: {}'.format(source))
    print('Destination: {}'.format(destination))

    # Get time & VC Tag
    now = datetime.now()
    start = now + timedelta(hours=1)
    end = now + timedelta(hours=2)
    date = now.strftime('%Y-%m-%dT%H:%M:%SZ')
    starttime = start.strftime('%Y-%m-%dT%H:%M:%SZ')
    endtime = end.strftime('%Y-%m-%dT%H:%M:%SZ')
    vc_tag = "0x53a218879d6d4d3ddf8a7c8af9c59966e7954847ee4eefdd0207a3ead9fc46f8"

    # Get DCMC Master Clients
    dcmcm_source = DCMCMasterClient(dcmcm_host_url=DC_SETTINGS['{}_URL'.format(source)],
                                    username=DC_SETTINGS['{}_USERNAME'.format(source)],
                                    password=DC_SETTINGS['{}_PASSWORD'.format(source)])
    dcmcm_dest = DCMCMasterClient(dcmcm_host_url=DC_SETTINGS['{}_URL'.format(destination)],
                                  username=DC_SETTINGS['{}_USERNAME'.format(destination)],
                                  password=DC_SETTINGS['{}_PASSWORD'.format(destination)])

    # Migration Request
    payload_source = {"date": date, "vc_tag": vc_tag, "starttime": starttime, "endtime": endtime, "price": 0,
                      "action_type": "offer", "load_values": [{"parameter": "cpu", "value": 1, "uom": "cpu"},
                                                              {"parameter": "ram", "value": 64, "uom": "MB"},
                                                              {"parameter": "disk", "value": 1, "uom": "GB"}]}

    payload_dest = {"date": date, "vc_tag": vc_tag, "starttime": starttime, "endtime": endtime, "price": 0,
                    "action_type": "bid", "load_values": [{"parameter": "cpu", "value": 1, "uom": "cpu"},
                                                          {"parameter": "ram", "value": 64, "uom": "MB"},
                                                          {"parameter": "disk", "value": 1, "uom": "GB"}]}

    # Send migration requests
    dcmcm_source.migration_request(payload_source)
    dcmcm_dest.migration_request(payload_dest)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--source', required=True,
                            help='[ DC_POPS.OLD_LAPTOP | DC_POPS.OLD_CTRL | DC_QRN | DC_PSNC ]')
        parser.add_argument('-d', '--dest', required=True,
                            help='[ DC_POPS.OLD_LAPTOP | DC_POPS.OLD_CTRL | DC_QRN | DC_PSNC ]')

    def handle(self, *args, **options):
        source = options['source']
        dest = options['dest']
        send_migration_requests(source, dest)
