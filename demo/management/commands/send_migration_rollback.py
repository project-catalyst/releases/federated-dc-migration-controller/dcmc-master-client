from django.core.management import BaseCommand

from communication_handler.api_clients.dcmcm_api import DCMCMasterClient
from demo.config import DC_SETTINGS


def send_migration_rollback(source, transaction_id):
    """Function to execute migration between two DCs"""
    print('Sending migration rollback ...')
    print('Source: {}'.format(source))

    # Get DCMC Master Clients
    dcmcm_source = DCMCMasterClient(dcmcm_host_url=DC_SETTINGS['{}_URL'.format(source)],
                                    username=DC_SETTINGS['{}_USERNAME'.format(source)],
                                    password=DC_SETTINGS['{}_PASSWORD'.format(source)])

    # Send migration rollback
    dcmcm_source.migration_rollback(transaction_id)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--source', required=True,
                            help='[ DC_POPS.OLD_LAPTOP | DC_POPS.OLD_CTRL | DC_QRN | DC_PSNC ]')
        parser.add_argument('-t', '--transaction', required=True, help='Transaction ID')

    def handle(self, *args, **options):
        source = options['source']
        transaction = options['transaction']
        send_migration_rollback(source, transaction)
