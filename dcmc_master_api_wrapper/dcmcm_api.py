from http import HTTPStatus
from logging import getLogger
from urllib.parse import urljoin

import requests

from dcmc_master_api_wrapper import config
from dcmc_master_api_wrapper.http_client.client import Client


class DCMCMasterClient(object):
    """DCMC Master Client Class.

    This class serves as a wrapper for the API endpoints exposed by the DCMC Master (DCMCM) Client component of the
    H2020 CATALYST Migration Controller. It covers calls to the entire available API of the DCMC Master Client API.

    Methods
    -------
    controller(transaction_id, tag)
        Get the OpenStack Controller details
    migration_by_marketaction(marketaction, tag)
        Retrieve migration by the associated marketaction ID
    migration_details(payload, tag)
        Send the migration details to the DCMC Master.
    migration_request(payload, tag)
        Issue a migration request to the DCMC Master.
    migration_rollback(transaction_id, tag)
        Send request for migration rollback.
    migration_update(payload, tag)
        Update the status of a migration.
    report_cmp_readiness(transaction_id, host_name, vpn_ip)
        Inform DCMC Master of CMP readiness.
    service_endpoints(transaction_id)
        Retrieve the DC endpoints and other essential information
    vcontainer(payload, tag)
        Issue request for VC creation to the DCMC Master.
    vcontainer_by_uuid(uuid, tag)
        Retrieve a VContainer by its UUID
    vpn_creation(transaction_id, tag)
        Send request for VPN creation to source DCMC Master

    Examples
    --------
    `Initiate a DCMCMasterClient`

    >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
    >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)

    `Retrieve controller details`

    >>> dcmcm.controller(transaction_id=TRANSACTION_ID)

    `Migration by Marketaction`

    >>> dcmcm.migration_by_marketaction(marketaction=MARKETACTION_ID)

    `Migration Details`

    >>> dcmcm.migration_details(payload=PAYLOAD)

    `Migration Request`

    >>> dcmcm.migration_request(payload=PAYLOAD)

    `Migration Rollback`

    >>> dcmcm.migration_rollback(transaction_id=TRANSACTION_ID)

    `Migration Update`

    >>> dcmcm.migration_details(payload=PAYLOAD)

    `Report CMP Readiness`

    >>> dcmcm.report_cmp_readiness(transaction_id=TRANSACTION_ID)

    `Service Endpoints`

    >>> dcmcm.service_endpoints(transaction_id=TRANSACTION_ID)

    `VContainer`

    >>> dcmcm.vcontainer(payload=PAYLOAD)

    `VContainer by UUID`

    >>> dcmcm.vcontainer_by_uuid(uuid=UUID)

    `VPN Create`

    >>> dcmcm.vpn_creation(transaction_id=TRANSACTION_ID)

    """
    __API_PREFIX = 'catalyst/dcmcm/api/'
    __MIGRATION_DETAILS = 'migration/details/'
    __MIGRATION_REQUEST = 'migration/request/'
    __MIGRATION_UPDATE = 'migration/marketplace/update/'
    __MIGRATION_BY_MARKETACTION = 'migration/marketaction/{marketaction_id}/'
    __CMP_READINESS = 'transaction/{transaction_id}/vcontainer/status/registered/'
    __CONTROLLER = 'transaction/{transaction_id}/controller/'
    __MIGRATION_ROLLBACK = 'transaction/{transaction_id}/migration/rollback/'
    __SERVICE_ENDPOINTS = 'transaction/{transaction_id}/service-endpoints/'
    __VCONTAINER_BY_UUID = 'transaction/vcontainer/{uuid}/'
    __VPN_CREATION = 'transaction/{transaction_id}/vpn/create/'
    __VCONTAINER = 'vcontainer/'
    __KEYCLOAK_TOKEN = 'realms/{realm-name}/protocol/openid-connect/token'

    def __init__(self, dcmcm_host_url, username, password):
        """DCMC Master Client Class Constructor.

        Parameters
        ----------
        dcmcm_host_url : URL
            The DCMC Master's Host URL
        username : str
            The username for the DCMC Master
        password : str
            The password for the DCMC Master

        """
        self.__client = Client(verify_ssl_cert=False)
        self.__url = urljoin(dcmcm_host_url, DCMCMasterClient.__API_PREFIX)
        self.__headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        self.log = getLogger()
        self.__token = self.__get_keycloak_token(username, password)
        self.__headers['Authorization'] = 'Bearer {}'.format(self.__token)

    def __get_keycloak_token(self, username, password):
        """Get an access token from Keycloak for authorization on the DCMC Master.

        Parameters
        ----------
        username : str
            A Keycloak Username
        password : str
            A Keycloak user's password

        Returns
        -------
        token : str
            A Keycloak token if request is valid, else None

        """
        self.log.info('Requesting access token from Keycloak ...')
        payload = {
            'username': username,
            'password': password,
            'grant_type': 'password',
            'client_id': config.KEYCLOAK_CLIENT_ID,
            'client_secret': config.KEYCLOAK_CLIENT_SECRET
        }
        params = {'realm-name': config.KEYCLOAK_REALM}
        url = urljoin(config.KEYCLOAK_HOST_URL, DCMCMasterClient.__KEYCLOAK_TOKEN).format(**params)
        response = requests.post(url=url, data=payload)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('Failed to obtain access token with status code [{}]'.format(response.status_code))
            self.log.debug('Response payload: `{}`'.format(response.text))
            return None
        self.log.info('Access token was retrieved successfully')
        return response.json()['access_token']

    def controller(self, transaction_id, tag=''):
        """Fetch the Controller details.

        When the vCMP is up and running in the Destination DC of the transaction it will request the details
        of the Source Controller from the DCMC Server. The DCMC Server is not aware of these details and it
        will request them from the DCMC Master of the Source DC and respond to vCMP accordingly.

        Parameters
        ----------
        transaction_id : int
            The ID of the transaction to fetch VPN IP for
        tag : str, optional
            A logging tag

        Returns
        -------
        dict, None
            A response payload if request succeeds, else `None`

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.controller(transaction_id=TRANSACTION_ID)
        {'name': 'DC1', 'ip': '192.168.255.6', 'port': 60011}

        """
        self.log.info('{} Retrieving controller details for transaction [{}]'.format(tag, transaction_id))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__CONTROLLER).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Controller request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} Request for controller details succeeded'.format(tag))
        return response.json()

    def migration_by_marketaction(self, marketaction, tag=''):
        """Retrieve migration by marketaction ID.

        Parameters
        ----------
        marketaction : int
            The ID of the marketaction to fetch migration for.
        tag : str, optional
            A logging tag

        Returns
        -------
        dict, None
            A response payload is request succeeds, else `None`

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.migration_by_marketaction(marketaction=MARKETACTION_ID)
        {
            "id": 978,
            "load": 801,
            "marketaction": 999,
            "status": "started"
        }

        """
        self.log.info('{} Requesting migration by marketaction id [{}]'.format(tag, marketaction))
        params = {'marketaction_id': marketaction}
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_BY_MARKETACTION).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Migration request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} Migration by marketaction was retrieved'.format(tag))
        return response.json()

    def migration_details(self, payload, tag=''):
        """Send Migration Details to DCMCM.

        When the migration details, including source, destination, start time, end time etc. are received
        by the DCMC Master, the essential pre-migration procedures are executed. Upon completion of these
        procedures, a request is issued to DCMC Server notifying for the readiness of the DC.

        Parameters
        ----------
        payload : dict
            The payload to send to the migration details endpoint. Includes the following keys:
                marketaction : int
                    The ID of the marketaction (IT Load Marketplace) corresponding to the migration
                source : str
                    The username of the migration's source DC as understood by the DCMC Server
                destination : str
                    The username of the migration's destination DC as understood by the DCMC Server
                delivery_start : str
                    The datetime on which the migration is planned to start
                delivery_end : str
                    The datetime on which the migration is planned to end
                transaction : int
                    The ID of the transaction as kept in the Catalyst IT Load Marketplace
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> payload = {
        ...     "marketaction": 3232,
        ...     "source": "DC1",
        ...     "destination": "DC2",
        ...     "delivery_start": "2019-10-01T21:00:00Z",
        ...     "delivery_end": "2019-10-01T22:00:00Z",
        ...     "transaction": 2323,
        ... }
        >>> dcmcm.migration_details(payload=payload)
        True

        """
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_DETAILS)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} Migration detail request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} Migration by marketaction was retrieved'.format(tag))
        return True

    def migration_request(self, payload, tag=''):
        """Send migration request to DCMC Master.

        An initial migration request is issued by the Energy Aware IT Load Balancer to each of DCMC Masters
        that are going to take part in the inter-DC migration, including information about the load, start
        and end times, etc. When processing is completed, the IT Load Marketplace Connector is notified.

        Parameters
        ----------
        payload : dict
            The payload to send to the migration request endpoint. Includes the following parameters:
                date : datetime
                    The time on which the request is created
                vc_tag : str
                    The VC Tag as generated by the VCG
                starttime : datetime
                    The time on which the requested migration is planned to start
                endtime : datetime
                    The time on which the requested migration is planned to end
                load_values : dict
                    Information about the load characteristics. Includes the following fields:
                        parameter : str
                            The name of the load characteristic
                        value : float
                            The value quantifying the load characteristics
                        uom : str
                            The unit of measurement of the value
                price : float
                    The price suggested for the migration action
                action_type : {'bid', 'offer'}
                    The type of market action which will be placed on the IT Load Marketplace
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> payload = {
        ...     "date": "2019-09-30T16:00:00Z",
        ...     "vc_tag": "0x53a218879d6d4d3ddf8a7c8af9c59966e7954847ee4eefdd0207a3ead9fc46f8",
        ...     "starttime": "2019-10-01T21:00:00Z",
        ...     "endtime": "2019-10-01T22:00:00Z",
        ...     "price": 0,
        ...     "action_type": "offer",
        ...     "load_values": [
        ...         {"parameter": "cpu", "value": 1, "uom": "cpu"},
        ...         {"parameter": "ram", "value": 2048, "uom": "MB"},
        ...         {"parameter": "disk", "value": 20, "uom": "GB"}
        ...     ]
        ... }
        >>> dcmcm.migration_request(payload)
        True

        """
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_REQUEST)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} Migration request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Migration request has succeeded'.format(tag))
        return True

    def migration_rollback(self, transaction_id, tag=''):
        """Command DC to rollback migration by transaction ID.

        Parameters
        ----------
        transaction_id : int
            The transaction ID to rollback migration for
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.migration_rollback(transaction_id=TRANSACTION_ID)
        True

        """
        self.log.info('{} Requesting migration rollback ...'.format(tag))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_ROLLBACK).format(*params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.ACCEPTED.value:
            self.log.error('{} Migration rollback failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Migration rollback request was successful'.format(tag))
        return True

    def migration_update(self, payload, tag=''):
        """Update the marketaction and status of the migration.

        This endpoint is mostly utilized by the IT Load Marketplace Connector (ITLMC). On the first call
        to the migration update endpoint, the ITLMC sends an update including the ID of the marketaction
        as it has been assigned by the CATALYST Marketplace.

        Parameters
        ----------
        payload : dict
            The payload to send to the migration update endpoint. Includes the following fields:
                marketaction : int
                    The ID of the marketaction (IT Load Marketplace) corresponding to the migration
                migration : int
                    The ID of the migration
                status : {'pending', 'posted', 'rejected', 'started', 'success', 'failed'}
                    The status of the migration.
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> payload = {
        ...     "marketaction": 999,
        ...     "migration": 8989,
        ...     "status": "posted"
        ... }
        >>> dcmcm.migration_update(payload)
        True

        """
        self.log.info('{} Updating migration ...'.format(tag))
        url = urljoin(self.__url, DCMCMasterClient.__MIGRATION_UPDATE)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Migration update failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Migration update was successful'.format(tag))
        return True

    def report_cmp_readiness(self, transaction_id, host_name, vpn_ip, tag=''):
        """Inform DCMCM of the CMP readiness.

        The DCMC Lite Client informs the DCMC Master of the source DC that the creation of the VContainer
        has been completed and the virtual OpenStack Compute Node (vCMP) has been registered with the
        controller of the source DC.

        Parameters
        ----------
        transaction_id : int
            The ID of the current transaction
        host_name : str
            The name of the Hypervisor's Host
        vpn_ip : str
            The VPN IP of the hypervisor
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.report_cmp_readiness(transaction_id=TRANSACTION_ID, host_name=HOST_NAME, vpn_ip=VPN_IP)
        True

        """
        self.log.info('{} Reporting readiness to DCMC Master'.format(tag))
        payload = {'host_name': host_name, 'vpn_ip': vpn_ip}
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__CMP_READINESS).format(**params)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Request failed with status code [{}].'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} Compute node has reported its readiness to DCMC Master'.format(tag))
        return True

    def service_endpoints(self, transaction_id, tag=''):
        """Retrieve the service endpoints from the controller.

        When the vCMP is up and running in the Destination DC of the transaction, it will connect to VPN and
        request Controller details. Afterwards, in order for Nova and Neutron services to be configured, it
        will request the service endpoints from the Source DC.

        Parameters
        ----------
        transaction_id : int
            The transaction ID as it appears in the
        tag : str
            A logging tag

        Returns
        -------
        dict, None
            The response payload if request succeeds, else `None`

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.service_endpoints(transaction_id=TRANSACTION_ID)
        {
            'authenticate_uri': 'http://controller:80/identity',
            'authorization_url': 'http://controller:80/identity',
            'glance_service': 'http://controller:80/image',
            'memcached_service': 'controller:11211',
            'memcached_secret_key': 'noanc090a90ha0sch8hahc09has0h',
            'neutron_service': 'http://controller:9696/',
            'neutron_password': 'neutron_password',
            'nova_password': 'nova_password',
            'placement_service': 'http://controller:80/placement',
            'placement_password': 'placement_password,
            'transport_url': 'rabbit://stackrabbit:devstack@controller:5672/nova_cell1',
            'nfs_server_host': 'controller',
            'nfs_server_root': '/opt/export/instances',
            'nova_uid': 'nova_UID',
            'nova_gid': 'nova_GID',
            'libvirt_uid': 'libvirt_UID',
            'libvirt_gid': 'libvirt_GID',
            'libvirt_group': 'libvirt_group',
            'libvirt_user': 'stack'
        }

        """
        self.log.info('{} Retrieving service endpoints from DCMC Master'.format(tag))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__SERVICE_ENDPOINTS).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} Request for endpoints failed with status code [{}]'.format(tag, response.status_code))
            self.log.error('{} Error details: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} Service endpoints retrieved successfully'.format(tag))
        return response.json()

    def vcontainer(self, payload, tag=''):
        """Send request for VContainer Initiation.

        The DCMC Server issues this request to the DCMC Master of the destination DC to
        trigger the creation of the VContainer, intended to host the OpenStack virtual
        compute node (vCMP) for the IT Load of the source DC to be migrated to.

        Parameters
        ----------
        payload : dict
            The payload to send to the vcontainer endpoint. Includes the following fields:
                cpu : int
                    The number of CPUs of the described VC
                cpu_uom : str
                    The unit of measurement for the CPU
                ram : int
                    The amount of RAM of the described VC
                ram_uom : str
                    The unit of measurement for the RAM value
                disk : int
                    The amount of disk of the described VC
                disk_uom : str
                    The unit of measurement for the disk value
                start : str
                    The datetime from which the VC is planned to be active
                end : str
                    The datetime until which the VC is planned to be active
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> payload = {
        ...     "cpu": 1,
        ...     "cpu_uom": "cpu",
        ...     "ram": 2048,
        ...     "ram_uom": "MB",
        ...     "disk": 20,
        ...     "disk_uom": "GB",
        ...     "start": "2019-10-01T21:00:00Z",
        ...     "end": "2019-10-01T22:00:00Z"
        ... }
        >>> dcmcm.vcontainer(payload=payload)
        True

        """
        self.log.info('{} VContainer request is issued to DCMC Master of destination ...'.format(tag))
        url = urljoin(self.__url, DCMCMasterClient.__VCONTAINER)
        response = self.__client.post(url=url, headers=self.__headers, payload=payload)
        if response.status_code != HTTPStatus.CREATED.value:
            self.log.error('{} VContainer request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} VContainer request was successful'.format(tag))
        return True

    def vcontainer_by_uuid(self, uuid, tag=''):
        """Get a VContainer by its UUID.

        Parameters
        ----------
        uuid : str
            The UUID of the VContainer to fetch
        tag : str, optional
            A logging tag

        Returns
        -------
        dict, None
            The response payload if request succeeds, else `None`

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.vcontainer_by_uuid(vcontainer_uuid=VCONTAINER_UUID)
        {
            "id": 78787,
            "cpu": 1,
            "cpu_uom": "cpu",
            "ram": 2048,
            "ram_uom": "MB",
            "disk": 20,
            "disk_uom": "GB",
            "start": "2019-10-01T21:00:00Z",
            "end": "2019-10-01T22:00:00Z",
            "uuid": "0ddce9a8-6c32-4cdb-9b01-07a32d2e8c7f",
            "transaction": 90099,
            "status": "registered"
        }

        """
        self.log.info('{} Retrieving VContainer by UUID ...'.format(tag))
        params = {'uuid': uuid}
        url = urljoin(self.__url, DCMCMasterClient.__VCONTAINER_BY_UUID).format(**params)
        response = self.__client.get(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.OK.value:
            self.log.error('{} VContainer request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return None
        self.log.info('{} VPContainer request was successful'.format(tag))
        return response.json()

    def vpn_creation(self, transaction_id, tag=''):
        """Command Source DC to initiate VPN creation.

        Parameters
        ----------
        transaction_id : int
            The transaction ID to initiate VPN connection for
        tag : str, optional
            A logging tag

        Returns
        -------
        bool
            `True` if request succeeds, `False` otherwise

        Examples
        --------
        >>> from dcmc_master_api_wrapper.dcmcm_api import DCMCMasterClient
        >>> dcmcm = DCMCMasterClient(dcmcm_host_url=DCMCM_HOST_URL, username=DCMCM_USERNAME, password=DCMCM_PASSWORD)
        >>> dcmcm.vpn_creation(transaction_id=TRANSACTION_ID)
        True

        """
        self.log.info('Sending request for VPN creation to DCMC Master of source ...'.format(tag))
        params = {'transaction_id': transaction_id}
        url = urljoin(self.__url, DCMCMasterClient.__VPN_CREATION).format(**params)
        response = self.__client.post(url=url, headers=self.__headers)
        if response.status_code != HTTPStatus.ACCEPTED.value:
            self.log.error('{} VPN creation request failed with status code [{}]'.format(tag, response.status_code))
            self.log.debug('{} Error details: `{}`'.format(tag, response.json()['error']))
            return False
        self.log.info('{} VPN creation request was accepted'.format(tag))
        return True
