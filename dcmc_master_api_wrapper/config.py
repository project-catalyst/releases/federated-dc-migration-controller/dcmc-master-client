import os

# =================================
#         KEYCLOAK SETTINGS
# =================================
KEYCLOAK_HOST_URL = os.getenv('DCMCM_KEYCLOAK_URL')
KEYCLOAK_REALM = os.getenv('DCMCM_KEYCLOAK_REALM')
KEYCLOAK_CLIENT_ID = os.getenv('DCMCM_KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_SECRET = os.getenv('DCMCM_KEYCLOAK_CLIENT_SECRET')
